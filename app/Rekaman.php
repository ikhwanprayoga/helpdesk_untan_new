<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekaman extends Model
{
    protected $table 	= 'rekaman';
    protected $fillable	= ['permintaan_id', 'kisi', 'waktu', 'tempat', 'tujuan_wawancara'];

    public function permintaan() {
    	return $this->belongsTo('App\Permintaan');
    }
}
