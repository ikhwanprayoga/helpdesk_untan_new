<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permintaan extends Model
{
    protected $table = 'permintaan';
    protected $fillable = ['kode_permintaan', 'nama', 'email', 'telpon', 'kategori', 'kebutuhan_informasi', 'alasan', 'prioritas', 'file_identitas', 'file_tambahan', 'pj_pengelola', 'tag', 'status'];

    public function respon_permintaan()
    {
    	return $this->hasMany('\App\ResponPermintaan');
    }

    public function respon_permintaan_user()
    {
        return $this->hasMany('\App\ResponPermintaanUser');
    }

    public function chat_permintaan()
    {
        return $this->hasMany('\App\ChatPermintaan');
    }

    public function rekaman()
    {
        return $this->hasOne('\App\Rekaman');
    }

    public function unit_kerja()
    {
        return $this->belongsTo('\App\UnitKerja');
    }

    public function user()
    {
        return $this->belongsTo('\App\User', 'pj_pengelola', 'id');
    }
}
