<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomplainSiremun extends Model
{
    protected $table	= 'komplain_siremun';
    protected $fillable = ['kode_komplain', 'nip', 'nama', 'email', 'telpon', 'kategori', 'judul', 'isi', 'prioritas', 'file_identitas', 'file_tambahan', 'pj_pengelola', 'tag'];

    public function respon_komplain()
    {
    	return $this->hasMany('\App\ResponKomplainSiremun', 'komplain_id');
    }

    public function respon_komplain_user()
    {
        return $this->hasMany('\App\ResponKomplainUserSiremun', 'komplain_id');
    }

    public function chat_komplain()
    {
        return $this->hasMany('\App\ChatKomplainSiremun', 'komplain_id');
    }

    public function user()
    {
        return $this->belongsTo('\App\User', 'pj_pengelola', 'id');
    }

    public function unit_kerja()
    {
        return $this->belongsTo('\App\UnitKerja');
    }
}
