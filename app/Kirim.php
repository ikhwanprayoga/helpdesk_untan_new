<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kirim extends Model
{
    public static function sms($tujuan, $pesan)
    {
        $pesan = html_entity_decode($pesan, ENT_QUOTES, 'utf-8'); 
        $pesan = urlencode($pesan);
        
    	$curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://api.nusasms.com/api/v3/sendsms/plain',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(
                'user'      => 'smsuntan_api',
                'password'  => '5M0334s',
                'SMSText'   => $pesan,
                'GSM'       => $tujuan
            )
        ));
        $resp = curl_exec($curl);
        if (!$resp) {
            return curl_error($curl) . '" - Code: ' . curl_errno($curl);
        } else {
            header('Content-type: text/xml'); /*if you want to output to be an xml*/
            return $resp;
        }
        curl_close($curl);
    }

    public static function wa($tujuan, $pesan)
    {
        $headers = ['headers' => ['apikey' => 'untan2020']];
        $client = new \GuzzleHttp\Client();
        $request = $client->post('http://103.126.83.7:8089/api/sendText/?id_device=1&message='.$pesan.'&tujuan='.$tujuan.'@s.whatsapp.net', $headers);
        $response = $request->getBody();
    }

    public static function wa_atasan($pesan)
    {
        $headers = ['headers' => ['apikey' => 'untan2020']];
        $users = User::whereIn('email', ['imamav@untan.ac.id', 'herysj@untan.ac.id'])->get();

        foreach ($users as $key => $value) {
            $client = new \GuzzleHttp\Client();
            $request = $client->post('http://103.126.83.7:8089/api/sendText/?id_device=1&message='.$pesan.'&tujuan='.$value->nohp.'@s.whatsapp.net', $headers);
            $response = $request->getBody();
        }
    }

    // public static function sms($tujuan, $pesan)
    // {
    //     // return $tujuan.'-'.$pesan;
    //     $client = new \GuzzleHttp\Client();

    //     $response = $client->request('POST', 'https://sms.service.untan.id/api/kirim-sms', [
    //         'form_params' => [
    //             'no_hp' => $tujuan,
    //             'pesan' => $pesan,
    //         ]
    //     ]);

    // }

    public static function sms2($tujuan, $pesan)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('http://ukt.keuangan.untan.ac.id/api.sms.php?kd_mhs=54695&handphone='.$tujuan.'&isi='.$pesan);
        $response = $request->getBody();

    }

    private static function ismscURL($link)
    {
    	$http = curl_init($link);

    	curl_setopt($http, CURLOPT_RETURNTRANSFER, TRUE);
    	$http_result = curl_exec($http);
    	$http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
    	curl_close($http);

    	return $http_result;
    }
}
