<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatPermintaan extends Model
{
    protected $table     = 'chat_permintaan';
    protected $fillable  = ['permintaan_id', 'user_pengirim', 'user_tujuan', 'isi', 'status', 'file'];

    public function permintaan()
    {
        return $this->belongsTo('\App\Permintaan');
    }

    public function log_chat_permintaan()
    {
        return $this->hasMany('\App\LogChatPermintaan');
    }

    public function userpengirim()
    {
        return $this->belongsTo('\App\User', 'user_pengirim', 'id');
    }

    public function usertujuan()
    {
        return $this->belongsTo('\App\User', 'user_tujuan', 'id');
    }
}
