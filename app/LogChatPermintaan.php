<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogChatPermintaan extends Model
{
    protected $table     = 'log_chat_permintaan';
    protected $fillable  = ['chat_permintaan_id', 'status'];

    public function chat_permintaan()
    {
        return $this->belongsTo('\App\ChatPermintaan');
    }
}
