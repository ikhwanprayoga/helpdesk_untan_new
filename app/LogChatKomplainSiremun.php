<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogChatKomplainSiremun extends Model
{
    protected $table     = 'log_chat_komplain_siremun';
    protected $fillable  = ['chat_komplain_id', 'status'];

    public function chat_komplain()
    {
        return $this->belongsTo('\App\ChatKomplainSiremun');
    }
}
