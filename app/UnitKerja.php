<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    protected $table    = 'unit_kerja';
    protected $fillable = ['nama_unit_kerja', 'keterangan'];
    public $timestamps  = false;

    public function komplain()
    {
        return $this->hasOne('\App\Komplain');
    }

    public function permintaan()
    {
        return $this->hasOne('\App\Permintaan');
    }
}

