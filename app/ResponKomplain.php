<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponKomplain extends Model
{
    protected $table	 = 'respon_komplain';
    protected $fillable	 = ['komplain_id', 'status', 'note', 'file', 'users_id'];

    public function komplain()
    {
        return $this->belongsTo('\App\Komplain');
    }

    public function first_status($id)
    {
        return $this->select('status')
                    ->where('komplain_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
    }
}
