<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatKomplainSiremun extends Model
{
    protected $table     = 'chat_komplain_siremun';
    protected $fillable  = ['komplain_siremun_id', 'user_pengirim', 'user_tujuan', 'isi', 'status', 'file'];

    public function komplain()
    {
        return $this->belongsTo('\App\KomplainSiremun');
    }

    public function log_chat_komplain()
    {
        return $this->hasMany('\App\LogChatKomplainSiremun');
    }

    public function userpengirim()
    {
        return $this->belongsTo('\App\User', 'user_pengirim', 'id');
    }

    public function usertujuan()
    {
        return $this->belongsTo('\App\User', 'user_tujuan', 'id');
    }
}
