<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogChatKomplain extends Model
{
    protected $table     = 'log_chat_komplain';
    protected $fillable  = ['chat_komplain_id', 'status'];

    public function chat_komplain()
    {
        return $this->belongsTo('\App\ChatKomplain');
    }
}
