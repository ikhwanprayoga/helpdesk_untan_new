<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatKomplain extends Model
{
    protected $table     = 'chat_komplain';
    protected $fillable  = ['komplain_id', 'user_pengirim', 'user_tujuan', 'isi', 'status', 'file'];

    public function komplain()
    {
        return $this->belongsTo('\App\Komplain');
    }

    public function log_chat_komplain()
    {
        return $this->hasMany('\App\LogChatKomplain');
    }

    public function userpengirim()
    {
        return $this->belongsTo('\App\User', 'user_pengirim', 'id');
    }

    public function usertujuan()
    {
        return $this->belongsTo('\App\User', 'user_tujuan', 'id');
    }
}
