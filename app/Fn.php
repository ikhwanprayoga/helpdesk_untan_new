<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Storage;

class Fn extends Model
{
    public static function simpan_file($file, $prefix, $folder)
    {
    	$filename = $prefix.'_'.md5(date('Ymdhis')).'-'.$file->getClientOriginalName();
        Storage::putFileAs($folder.'/', $file, $filename);

		return $filename;
    }

    public static function tglinput_to_db($tanggal)
    {
    	$tgl		= explode("/", $tanggal);
    	$tgl_update	= $tgl[2]."-".$tgl[0]."-".$tgl[1];

    	return $tgl_update;
    }
}
