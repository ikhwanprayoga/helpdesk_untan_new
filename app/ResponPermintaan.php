<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponPermintaan extends Model
{
    protected $table	 = 'respon_permintaan';
    protected $fillable	 = ['permintaan_id', 'status', 'note', 'file', 'users_id'];

    public function permintaan()
    {
        return $this->belongsTo('\App\Permintaan');
    }

    public function first_status($id)
    {
        return $this->select('status')
                    ->where('permintaan_id', $id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
    }
}
