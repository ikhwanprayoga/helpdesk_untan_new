<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponKomplainUser extends Model
{
    protected $table	 = 'respon_komplain_user';
    protected $fillable	 = ['komplain_id', 'catatan', 'file'];

    public function komplain()
    {
        return $this->belongsTo('\App\Komplain');
    }
}
