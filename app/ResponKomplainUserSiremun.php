<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponKomplainUserSiremun extends Model
{
    protected $table	 = 'respon_komplain_user_siremun';
    protected $fillable	 = ['komplain_id', 'catatan', 'file'];
    
    public function komplain()
    {
        return $this->belongsTo('\App\KomplainSiremun');
    }
}
