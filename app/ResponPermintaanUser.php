<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponPermintaanUser extends Model
{
    protected $table	 = 'respon_permintaan_user';
    protected $fillable	 = ['permintaan_id', 'catatan', 'file'];

    public function permintaan()
    {
        return $this->belongsTo('\App\Permintaan');
    }
}
