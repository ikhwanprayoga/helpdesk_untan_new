<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komplain extends Model
{
    protected $table	= 'komplain';
    protected $fillable = ['kode_komplain', 'nama', 'email', 'telpon', 'kategori', 'judul', 'isi', 'prioritas', 'file_identitas', 'file_tambahan', 'pj_pengelola', 'tag'];

    public function respon_komplain()
    {
    	return $this->hasMany('\App\ResponKomplain');
    }

    public function respon_komplain_user()
    {
        return $this->hasMany('\App\ResponKomplainUser');
    }

    public function chat_komplain()
    {
        return $this->hasMany('\App\ChatKomplain');
    }

    public function user()
    {
        return $this->belongsTo('\App\User', 'pj_pengelola', 'id');
    }

    public function unit_kerja()
    {
        return $this->belongsTo('\App\UnitKerja');
    }
    
}
