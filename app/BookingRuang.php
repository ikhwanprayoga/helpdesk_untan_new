<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRuang extends Model
{
    protected $table  	=  'booking_ruang';
    protected $fillable	=  [ 'kode_booking', 'nama', 'email', 'telpon', 'kegiatan', 'file_identitas', 'data_ruang_id', 'kode_ruang', 'dari_tgl', 'sampai_tgl', 'dari_jam', 'sampai_jam', 'status'];


    public function data_ruang() {
    	return $this->belongsTo('App\DataRuang');
    }

    public function respon_booking() {
    	return $this->hasMany('App\ResponBooking');
    }

}
