<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Komplain;
use App\ChatKomplainSiremun;
use App\LogChatKomplainSiremun;
use App\ResponKomplain;
use App\Fn;
use App\User;
use App\Kirim;
use App\Link;   

use Mail;
use Auth;
use Alert;
use Notify;

class PesanKomplainSiremunController extends Controller
{
    public function pesan_masuk()
    {
        $chat = ChatKomplainSiremun::where('user_tujuan', Auth::id())->groupBy('komplain_id')->orderBy('id', 'DESC')->get();

        return view('admin.k_pesan_masuk.index', compact('chat'));
    }

    public function pesan_masuk_list($id)
    {
        // $chat = ChatKomplainSiremun::where('id', $id)->first();

        // if($chat->status == 'Belum Baca')
        // {
        //     //update status baca di chat_komplain
        //     $chatupdate = ChatKomplainSiremun::find($id);
        //     $chatupdate->status = 'Baca';
        //     $chatupdate->save();

        //     //tambah log chat
        //     $log = new LogChatKomplainSiremun;
        //     $log->chat_komplain_id = $id;
        //     $log->status           = 'Baca';
        //     $log->save();
        // }

        // return view('admin.k_pesan_masuk.list', compact('chat'));

        $chat = ChatKomplainSiremun::where('id', $id)->first();

        if($chat->status == 'Belum Baca')
        {
            //update status baca di chat_komplain
            $chatupdate = ChatKomplainSiremun::find($id);
            $chatupdate->status = 'Baca';
            $chatupdate->save();

            //tambah log chat
            $log = new LogChatKomplainSiremun;
            $log->chat_komplain_id = $id;
            $log->status           = 'Baca';
            $log->save();
        }

        $user_list_masuk = ChatKomplainSiremun::select('id', 'user_pengirim')
                                    ->where('komplain_id', $chat->komplain_id)
                                    ->where('user_tujuan', Auth::id())
                                    ->groupBy('user_pengirim')
                                    ->get();
        
        $user_list_keluar = ChatKomplainSiremun::select('id', 'user_tujuan')
                                    ->where('komplain_id', $chat->komplain_id)
                                    ->where('user_pengirim', Auth::id())
                                    ->whereRaw("user_tujuan NOT IN (SELECT user_pengirim FROM chat_komplain WHERE komplain_id=".$chat->komplain_id." AND user_tujuan=".Auth::id()." GROUP BY user_pengirim)")
                                    ->groupBy('user_tujuan')
                                    ->get();
                                    
        $respon_komplain  =	ResponKomplain::where('komplain_id', $chat->komplain_id)->get();

        return view('admin.k_pesan_masuk.list', compact('chat', 'user_list_masuk', 'user_list_keluar', 'respon_komplain'));
    }

    public function pesan_masuk_list_detail($komplain_id, $user_id)
    {

        // ->where(function ($query) {
            //     $query->where('status_verifikasi', '10')
            //         ->orWhere('status_verifikasi', '19');
            //     })
        $komplain = Komplain::where('id', $komplain_id)->first();
        // SELECT * FROM `chat_komplain`where (user_pengirim=2 or user_tujuan=2) and (user_pengirim=5 or user_tujuan=5) and komplain_id=1
        $chating = ChatKomplainSiremun::where('komplain_id', $komplain_id)
                                    ->where(function ($query) {
                                            $query->where('user_pengirim', Auth::id())
                                                ->orWhere('user_tujuan', Auth::id());
                                            })
                                    ->where(function ($query) use ($user_id) {
                                            $query->where('user_pengirim', $user_id)
                                                ->orWhere('user_tujuan', $user_id);
                                            })
                                    ->orderBy('created_at', 'ASC')->get();
        $last_chat_id = ChatKomplainSiremun::select('id')
                                    ->where('komplain_id', $komplain_id)
                                    ->where(function ($query) {
                                            $query->where('user_pengirim', Auth::id())
                                                ->orWhere('user_tujuan', Auth::id());
                                            })
                                    ->where(function ($query) use ($user_id) {
                                            $query->where('user_pengirim', $user_id)
                                                ->orWhere('user_tujuan', $user_id);
                                            })
                                    ->orderBy('created_at', 'DESC')->first();
        
                                            
        //tambah status log jadi baca
        //1.ambil dulu list id di chat komplain
        $list_chat_id = ChatKomplainSiremun::select('id')
                                    ->where('user_tujuan', Auth::id())
                                    ->where('user_pengirim', $user_id)                                                
                                    ->where('komplain_id', $komplain_id)
                                    ->where('status', 'Belum Baca')
                                    ->get();
        //2.insert status baca ke log_chat
        foreach($list_chat_id as $l_id) {
            //tambah log chat
            $log = new LogChatKomplainSiremun;
            $log->chat_komplain_id = $l_id->id;
            $log->status           = 'Baca';
            $log->save();
        }

        //update semua pesan yg belum di baca jadi sudah di baca
        $c = ChatKomplainSiremun::where('user_tujuan', Auth::id())
                            ->where('user_pengirim', $user_id)                                                
                            ->where('komplain_id', $komplain_id)
                            ->where('status', 'Belum Baca')
                            ->update(['status' => 'Baca']);

        $teman_chat = User::where('id', $user_id)->first();
        
        return view('admin.k_pesan_masuk.detail', compact('komplain', 'chating', 'last_chat_id', 'user_id', 'teman_chat'));
    }

    public function pesan_masuk_forward($id)
    {
        $chat = ChatKomplainSiremun::where('id', $id)->first();

        return view('admin.k_pesan_masuk.forward', compact('chat'));
    }

    public function pesan_masuk_forward_save(Request $request, $id)
	{
        // dd($request->all());
        //update chat sebelumnya jadi diteruskan
        $updatechat = ChatKomplainSiremun::find($id);
        //jika diteruskan lebih dari sekali dan user tujuan belum membaca, maka tidak perlu ubah status
        if($updatechat->status !== 'Belum Baca') {

            $updatechat->status = 'Diteruskan';
            $updatechat->save();

            //tambah log lama jadi di teruskan
            $log = new LogChatKomplainSiremun;
            $log->chat_komplain_id = $id;
            $log->status           = 'Diteruskan';
            $log->save();
        }   

    
        ############# --------------------------------- #############

        //tambah chat baru ke tujuan
		$chat = new ChatKomplainSiremun;
		$chat->komplain_id	 = $request->komplain_id;
		$chat->user_pengirim = Auth::id();
		$chat->user_tujuan   = $request->tujuan;
		$chat->isi			 = $request->pesan;
        $chat->status 		 = 'Belum Baca';
        
        $file = '';
		if($request->hasFile('file'))
		{
			$file 		= Fn::simpan_file($request->file('file'), 'chat', 'siremun');
			$chat->file = $file;
		}
		$chat->save();

        //tambah log baru
		$log = new LogChatKomplainSiremun;
		$log->chat_komplain_id 	= $chat->id;
		$log->status 			= 'Belum Baca';

		if($log->save()) {
            // Alert::success('Komplain berhasil diteruskan.', 'Berhasil!')->autoclose(1500);
            ############# insert link #############
				// http://localhost:8000/redirect?uid=5&token=vziv4MxseNiiQCEmZmhBNNWg6Z7J2x7jMoaSKOEYt3f4a4br5IiQ57x6y0rL&kat=kom&id=41
				$base_url 	= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
				$token		= md5(date('Ymdhis'));
				$url 		= $base_url."/redirect?uid={$request->tujuan}&t={$token}&k=komp&id={$chat->id}";
				
				$link  = new Link;
				$link->url 		= $url; 
				$link->token	= $token;
				$link->status 	= 0;
				$link->save();
            ############# ./insert link #############
            
            ############# kirim notif ke user tujuan #############				
				$user = User::where('id', $request->tujuan)->first();
				Kirim::wa($user->nohp, 'Pak '.Auth::user()->name.' meneruskan komplain. Kode komplain : '. $updatechat->komplain->kode_komplain.'. Url: '.$url);
				
				//ambil nama tujuan pesan
				$u_tujuan = User::where('id', $request->tujuan)->first();

				$judul			= 'Pesan Masuk dari '.Auth::user()->name;
				$kategori_data  = 'Komplain';
				$nama           = $u_tujuan->name;
				$email_tujuan	= $u_tujuan->email;
				$pengirim_pesan	= Auth::user()->name;
				$kode           = $updatechat->komplain->kode_komplain;
				$kategori_user  = $updatechat->komplain->tag;
				$pesan 			= $request->pesan;

				Mail::send('layouts.email.admin.pesan_masuk', [
							'nama' 				=> $nama,
							'pesan' 			=> $pesan,
							'kategori_data' 	=> $kategori_data, 
							'pengirim_pesan'	=> $pengirim_pesan,
							'kode' 				=> $kode, 
                            'kategori_user' 	=> $kategori_user,
                            'url' 				=> $url,
						], function ($message) use ($judul, $email_tujuan)
				{   
					$message->subject($judul);
					$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					$message->to($email_tujuan);
				});
			############# ./kirim notif ke user tujuan admin #############
            Notify::success('Komplain berhasil diteruskan.', 'Berhasil!');	
			return redirect()->back();
		} else {
            // Alert::danger('Komplain gagal diteruskan.', 'Gagal!')->autoclose(1500);
            Notify::success('Komplain gagal diteruskan.', 'Gagal!');	
			return redirect()->back();
		}
    }
    
}
