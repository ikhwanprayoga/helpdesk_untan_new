<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Link;
use Auth;

class RedirectWithTokenController extends Controller
{
    public function index()
    {
        $get_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

        $user = User::where('id', $_GET['uid'])->first();
        $link = Link::where('token', $_GET['t'])
                        ->where('url', $get_url)
                        ->where('status', 0)
                        ->first();

        if( $user && $link ) {

            //buat session login
            Auth::loginUsingId($user->id);

            // http://localhost:8000/redirect?uid=5&token=vziv4MxseNiiQCEmZmhBNNWg6Z7J2x7jMoaSKOEYt3f4a4br5IiQ57x6y0rL&kat=kom&id=41
            if(isset($_GET['k']) && isset($_GET['id'])) {

                //update status link jadi 1 (sudah di akses)
                $l = Link::find($link->id);
                $l->status = 1;
                $l->save();

                if($_GET['k'] == 'komp') {
                    return redirect()->route('admin.komplain.pesan.masuk.list', $_GET['id']);
                } else if($_GET['k'] == 'perm') {
                    return redirect()->route('admin.permintaan.pesan.masuk.list', $_GET['id']);
                } else {
                    return 'link tidak valid!';
                }

            } else {
                return 'link tidak valid!';
            }

        } else {
            return 'url tidak valid !';
        }
    }
}
