<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fn;
use App\BookingRuang;
use App\DataRuang;
use App\ResponBooking;
use DB;
use Auth;

class DataBookingController extends Controller
{
    public function index()
    {
    	$data['booking'] = BookingRuang::orderBy('created_at', 'DESC')->get();

    	foreach ($data['booking'] as $key => $value) {
    		$respon_booking = ResponBooking::select('status')
											   ->where('booking_ruang_id', $value->id)
											   ->orderBy('created_at', 'DESC')
											   ->first();

    		$data['booking'][$key]->status = $respon_booking;
    	}
    	return view('admin.booking.index', compact('data'));
    }

    public function detail($id)
    {
    	$detail_booking  =  BookingRuang::where('id', $id)->get();

		$respon_booking  =	ResponBooking::where('booking_ruang_id', $id)->get();

		return view('admin.booking.detail', compact('detail_booking', 'respon_booking', 'id'));
    }

    public function send_feedback(Request $request)
    {
    	// print_r($request->all());
    	$booking_ruang_id	=  $request->booking_ruang_id;
    	$status 			=  $request->status;
    	$note				=  $request->note;
        $users_id       	=  Auth::user()->id;

    	// untuk file
    	$file				=  $request->file('file');
		if($request->hasFile('file'))
		{
			$file 		= Fn::simpan_file($file, 'booking');
		}

		$save = ResponBooking::create([
					'booking_ruang_id'	=> $booking_ruang_id,
					'status'			=> $status,
					'note'				=> $note,
					'file'				=> $file,
                    'users_id'      	=> $users_id,
			]);

		if($save) {
			return redirect('databooking/detail/'.$booking_ruang_id);
			// return view('booking.alert', compact('kode_booking', 'email'))
			// 			->with('status_simpan', 'berhasil');
		} else {
			return "gagal";
			// return view('booking.alert')
			// 			->with('status_simpan', 'gagal');
		}
    }
}
