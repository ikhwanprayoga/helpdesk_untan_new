<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Fn;
use App\BookingRuang;
use App\DataRuang;
use App\ResponBooking;
use DB;

class BookingController extends Controller
{
    public function index()
    {
    	//ambil data ruangan
    	$data_ruang	=  DataRuang::all();
    	return view('booking.index', compact('data_ruang'))->with('status', 'null');
    }

    public function cekruangan(Request $request)
    {
    	$dari_tgl	=  Fn::tglinput_to_db($request->dari_tgl);
    	$sampai_tgl	=  Fn::tglinput_to_db($request->sampai_tgl);
    	$id_ruangan	=  $request->id_ruangan;

    	//ambil data ruangan
    	$data_ruang	=  DataRuang::all();

    	//cek tanggal dari - sampai, apakah sudah ada
    	$cek_ruang	=  BookingRuang::join('respon_booking', 'booking_ruang.id', 'respon_booking.booking_ruang_id')
                                     ->where('status', 'Selesai')
                                     ->where('dari_tgl', '<=', $sampai_tgl)
    								 ->where('sampai_tgl', '>=', $dari_tgl)
    								 ->where('data_ruang_id', $id_ruangan)
    								 ->count();
                                     
    	if($cek_ruang>=1) {
    		return view('booking.index', compact('data_ruang'))->with('status', 'failed');
    	} else {
    		return view('booking.booking', compact('id_ruangan', 'dari_tgl', 'sampai_tgl'));
    	}

    }

    public function booking(Request $request)
    {
    	// dd($request->all());
    	// validasi form input
    	$this->validate($request, [
				'nama'					=> 'required',
				'email'					=> 'required',
				'telpon'				=> 'required',
				'instansi'				=> 'required',
				'nama_kegiatan'			=> 'required',
				'file_identitas'		=> 'required|mimes:jpg,jpeg,png,pdf|max:1000',
    	],[
				'nama.required'				=> 'Nama harus diisi.',
				'email.required'			=> 'Email harus diisi',
				'telpon.required'			=> 'Nomor telpon harus diisi',
				'instansi.required'			=> 'Instansi harus diisi',
				'nama_kegiatan.required'	=> 'Nama kegiatan harus diisi',
                'file_identitas.required'	=> 'File identitas tidak boleh kosong',
                'file_identitas.mimes'		=> 'Hanya boleh file .jpg .jpeg .png .pdf | Max 1MB',
				'file_identitas.max'		=> 'Maksimal ukuran file 1MB',
    	]);

        $id_ruangan         =  $request->id_ruangan;
    	$nama 				=  $request->nama;
		$email				=  $request->email;
		$telpon				=  $request->telpon;
		$instansi			=  $request->instansi;
		$nama_kegiatan		=  $request->nama_kegiatan;
        $dari_tgl           =  $request->dari_tgl;
        $sampai_tgl         =  $request->sampai_tgl;

    	// cek file identitas dan tambahan
		// jika file ada maka simpan filenya
		$file_identitas     =  $request->file('file_identitas');
		$file_surat		    =  $request->file('file_surat');
		if($request->hasFile('file_identitas'))
		{
			$file_identitas	= Fn::simpan_file($file_identitas, 'booking_ruangan');
		}
				
		if($request->hasFile('file_surat'))
		{
			$file_surat	= Fn::simpan_file($file_surat, 'booking_ruangan');
		}

		// untuk pertama kali insert data komplain
        $qcek_booking      =  BookingRuang::select('kode_booking')->count();
        //ambil kode_booking terakhir
        $get_kodebooking   =  BookingRuang::select('kode_booking')
                                         ->orderBy('id', 'DESC')
                                         ->first();

        if($qcek_booking==0) {
            $kode_booking  = "BOOK1";
        } else {
            $kode_booking  =  $get_kodebooking->kode_booking;
            $exp_kode       =  explode('K', $kode_booking);
            // tambah "BOOK" (kode_booking terakhir + 1)
            $kode_booking  =  "BOOK" . ($exp_kode[1] + 1);
        }

        // insert ketable booking dan ambil insertgetid
        $id_booking  = DB::table('booking_ruang')->insertGetId([
                            'kode_booking'      =>  $kode_booking,
                            'nama'              =>  $nama,
                            'email'             =>  $email,
                            'telpon'            =>  $telpon,
                            'kegiatan'          =>  $nama_kegiatan,
                            'instansi'          =>  $instansi,
                            'file_identitas'    =>  $file_identitas,
                            'file_surat'        =>  $file_surat,
                            'data_ruang_id'     =>  $id_ruangan,
                            'dari_tgl'          =>  $dari_tgl,
                            'sampai_tgl'        =>  $sampai_tgl,
                            // 'dari_jam'          =>  '00:00:00',
                            // 'sampai_jam'        =>  '00:00:00',
                            'created_at'        =>  \Carbon\Carbon::now(),
                            'updated_at'        =>  \Carbon\Carbon::now(),
                    ]);

        // create respon_komplain
        $save       =  ResponBooking::create([
                            'booking_ruang_id'  =>  $id_booking,
                            'status'            =>  'Terkirim', 
                            'note'              =>  'Permintaan data telah terkirim.',                      
                            'created_at'        =>  \Carbon\Carbon::now(),
                            'updated_at'        =>  \Carbon\Carbon::now(),
                    ]);

        if($save) {
            return view('booking.alert', compact('kode_booking', 'email'))
                        ->with('status_simpan', 'berhasil');
        } else {
            return view('booking.alert')
                        ->with('status_simpan', 'gagal');
        }
    }

    public function ambilruang($id)
    {
        $ruang = DataRuang::find($id);
        return response()->json(array('ruang' => $ruang), 200);
    }
}
