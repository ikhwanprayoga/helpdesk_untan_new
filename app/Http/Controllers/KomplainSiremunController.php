<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Notify;
use Alert;
use Mail;
use App\User;
use App\Kirim;
use App\Komplain;
use App\Fn;
use App\KomplainSiremun;
use App\ResponKomplainSiremun;

class KomplainSiremunController extends Controller
{	
    public function index()
    {
		return view('Komplain_siremun.komplain');
    }

    public function create(Request $request)
    {
		// dd($request->all());    	
		//tesgitgit
		// validasi form input
		//jika kategoriuser != dosen, perlu validasi file identitas

		// if($request->kategori_user !== 'dosen'){
			$this->validate($request, [
					// 'file_identitas'	=> 'required|mimes:jpg,jpeg,png,pdf|max:1000',
					'file_tambahan'		=> 'mimes:jpg,jpeg,png,pdf|max:2800',
			],[
					// 'file_identitas.required'	=> 'File identitas tidak boleh kosong',
					// 'file_identitas.mimes'		=> 'Hanya boleh file .jpg .jpeg .png .pdf | Max 1MB',
					// 'file_identitas.max'		=> 'Maksimal ukuran file 1MB',

					'file_tambahan.mimes'		=> 'Hanya boleh file .jpg .jpeg .png .pdf | Max 2MB',
					'file_tambahan.max'			=> 'Maksimal ukuran file 1MB',
			]);
		// } 
		
		$this->validate($request, [
				'nama'				=> 'required',
				'email'				=> 'required',
				'telpon'			=> 'required',
				'judul_komplain'	=> 'required',
				'isi_komplain'		=> 'required',
		],[
				'nama.required'				=> 'Nama harus diisi.',
				'email.required'			=> 'Email harus diisi',
				'telpon.required'			=> 'Nomor telpon harus diisi',
				'judul_komplain.required'	=> 'Judul komplain harus diisi',
				'isi_komplain.required'		=> 'Isi komplain harus diisi',
		]);

    	$kategori_pengguna 	=  $request->kategori_pengguna;
    	$unit_kerja 		=  $request->unit_kerja;
    	$nama 				=  $request->nama;
    	$nip 				=  $request->nip;
		$email				=  $request->email;
		$telpon				=  '62'.$request->telpon;
		$kategori			=  $request->kategori;
		$judul_komplain		=  $request->judul_komplain;
		$isi_komplain		=  $request->isi_komplain;

		$file_tambahan= '';
		if( $request->hasFile('file_tambahan') ) {                         
            $file_tambahan = Fn::simpan_file($request->file('file_tambahan'), 'tambahan', 'siremun');
        }
			
		// untuk pertama kali insert data komplain
		$qcek_komplain		=  KomplainSiremun::select('kode_komplain')->count();
		//ambil kode_komplain terakhir
		$get_kodekomplain	=  KomplainSiremun::select('kode_komplain')
										->orderBy('id', 'DESC')
										->first();

		if($qcek_komplain==0) {
			$kode_komplain 	= "PLAIN1";
		} else {
			$kode_komplain 	=  $get_kodekomplain->kode_komplain;
			$exp_kode		=  explode('N', $kode_komplain);
			// tambah "PLAIN" (kode_komplain terakhir + 1)
			$kode_komplain  =  "PLAIN" . ($exp_kode[1] + 1);
		}

		// insert ketable komplain dan ambil insertgetid
		$id_komplain  = DB::table('komplain_siremun')->insertGetId([
							'nip' 			=>  $nip,
							'kode_komplain' =>  $kode_komplain,
							'unit_kerja' 	=>	$unit_kerja,
							'kategori_pengguna' =>	$kategori_pengguna,
							'nama' 			=>  $nama,
							'email'			=>  $email,
							'telpon'		=>  $telpon,
							'kategori'		=>  $kategori,
							'judul'			=>  $judul_komplain,
							'isi'			=>  $isi_komplain,
							'file_tambahan'	=>  $file_tambahan,
							'tag' 			=> 	$request->kategori_user,
							'status'		=> 'Terkirim',
							'created_at'	=>  \Carbon\Carbon::now(),
							'updated_at'	=>  \Carbon\Carbon::now(),
					]);

		// create respon_komplain
		$save 		=  ResponKomplainSiremun::create([
							'komplain_id'	=>  $id_komplain,
							'status'		=>  'Terkirim',	
							'note'			=>  'Komplain data telah terkirim.',						
							'created_at'	=>  \Carbon\Carbon::now(),
							'updated_at'	=>  \Carbon\Carbon::now(),
					]);

		if($save) {
			############# kirim notif ke service desk #############				
				$sd = User::where('email', 'zuhri@untan.ac.id')->first();	
				$pesan = 'Ada komplain siremun masuk. Kode komplain : '. $kode_komplain.'. Kategori Pertanyaan: '.$kategori.'. Pertanyaan: '.$isi_komplain;		
				Kirim::wa($sd->nohp, $pesan);
				Kirim::wa_atasan($pesan);
				// $judul			= 'Ada Komplain Baru';
				// $kategori_data  = 'Komplain';
				// $nama           = $sd->name;
				// $kode           = $kode_komplain;
				// $kategori_user  = $request->kategori_user;
				// $email_tujuan   = $sd->email;
				// $pesan 			= 'Ada komplain baru, mohon ditindak lanjuti.';
				// Mail::send('layouts.email.admin.baru', [
				// 			'nama' 				=> $nama,
				// 			'pesan' 			=> $pesan,
				// 			'kategori_data' 	=> $kategori_data, 
				// 			'kode' 				=> $kode, 
				// 			'kategori_user' 	=> $kategori_user
				// 		], function ($message) use ($judul, $email_tujuan)
				// {   
				// 	$message->subject($judul);
				// 	$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
				// 	$message->to($email_tujuan);
				// });
			############# ./kirim notif ke service desk #############
			
			############# kirim notif ke client #############
				$pesanSms = 'Terimakasih telah melakukan komplain. Untuk melakukan pelacakan komplain anda, gunakan email : '. $email. ' dan kode '.$kode_komplain;
				Kirim::wa($telpon, $pesanSms);
				// $kategori_data  = 'Komplain';
				// $judul			= $kategori_data.' terkirim';				
				// $nama           = $request->nama;
				// $kode           = $kode_komplain;
				// $email_tujuan   = $request->email;
				// $pesan 			= 'Komplain telah terkirim ke Admin HelpDesk.';
				// Mail::send('layouts.email.user.baru', [
				// 										'nama'			 => $nama, 
				// 										'pesan'			 => $pesan,
				// 										'kategori_data'  =>	$kategori_data,
				// 										'kode'			 => $kode,
				// 										'kategori_user'	 => $kategori_user, 
				// 										'email'			 => $email_tujuan,
				// 									], function ($message) use ($judul, $email_tujuan)
				// {   
				// 	$message->subject($judul);
				// 	$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
				// 	$message->to($email_tujuan);
				// });
			############# ./kirim notif ke client #############

			Alert::info('
					Data anda telah terkirim ke Administrator HelpDesk. <br>
					<h5 class="m-t-20"><b>Email anda </b> <span class="label label-primary m-l-5">'.$email.'</span></h5>
					<h5 class="m-t-20"><b>Kode Tiket Komplain </b> <span class="label label-primary m-l-5">'.$kode_komplain.'</span></h5>
					Gunakan kode tiket dan email di atas untuk melacak status komplain anda. <br>
					Untuk melacak status komplain, klik menu "Tracking".
			')->html()->persistent("Keluar");

			return redirect()->back();

			// return view('komplain.alert', compact('kode_komplain', 'email'))
			// 			->with('status_simpan', 'berhasil');
		} else {
			Notify::error('Komplain anda gagal terkirim', 'Gagal!');
			// return view('komplain.alert')
			// 			->with('status_simpan', 'gagal');
		}
	}	
	
}
