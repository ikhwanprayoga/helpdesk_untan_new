<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Alert;

class EmailController extends Controller
{
    public function kirim_email()
    {
        $nama           = 'Erwin Wahyudi';
        $judul          = 'Komplain Berhasil';
        $pesan          = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel quae dicta veritatis deserunt, itaque voluptas odio velit dignissimos. Quo, provident. Autem tenetur blanditiis quaerat impedit architecto maiores et eos repudiandae!';
        $email_tujuan   = 'erwin.wahyudi92@gmail.com';
        try{
            Mail::send('email', ['nama' => $nama, 'pesan' => $pesan], function ($message) use ($nama, $judul, $pesan, $email_tujuan)
            {   
                $message->subject($judul);
                $message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
                $message->to($email_tujuan);
            });
            Alert::success('Email terkirim.', 'Berhasil!');
            return redirect()->back();
        }
        catch (Exception $e){
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }
}
