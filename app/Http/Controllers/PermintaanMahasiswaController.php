<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Alert;

class PermintaanMahasiswaController extends Controller
{
    public function index(Request $request)
    {
        if($request->session()->has('nama') && $request->session()->has('nim')) {
            return view('permintaan_mahasiswa.index');
        } else {
            Alert::error('Tidak memiliki izin akses!!', 'Error!');
            return redirect()->route('mahasiswa.login');
        }
    }
}
