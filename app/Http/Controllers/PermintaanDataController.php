<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Permintaan;
use App\ResponPermintaan;
use App\Fn;
use App\Kirim;
use App\User;

use Mail;
use DB;
use Alert;
use Notify;

class PermintaanDataController extends Controller
{
	public function index_unitkerja()
    {
        return view('permintaan_unitkerja.index');
	}

	public function index_mahasiswa(Request $request)
    {
        if($request->session()->has('nama') && $request->session()->has('nim')) {
            return view('permintaan_mahasiswa.index');
        } else {
            Alert::error('Tidak memiliki izin akses!!', 'Error!');
            return redirect()->route('mahasiswa.login');
        }
	}

	public function index_dosen(Request $request)
    {
        if($request->session()->has('nama') && $request->session()->has('nip')) {
            return view('permintaan_dosen.index');
        } else {
            Alert::error('Tidak memiliki izin akses!!', 'Error!');
            return redirect()->route('dosen.login');
        }
	}
	
    public function index()
    {
    	return view('permintaan.index');
    }

    public function create(Request $request)
    {
    	// echo "<pre>";
    	// print_r($request->all());
    	// echo "</pre>"; die();

		// validasi form input
		if($request->kategori_user !== 'dosen'){
			$this->validate($request, [
				'file_identitas'	=> 'required|mimes:jpg,jpeg,png,pdf|max:1000',
				'file_tambahan'		=> 'mimes:jpg,jpeg,png,pdf|max:1000',
			],[
				'file_identitas.required'	=> 'File identitas tidak boleh kosong',
				'file_identitas.mimes'		=> 'Hanya boleh file .jpg .jpeg .png .pdf | Max 1MB',
				'file_identitas.max'		=> 'Maksimal ukuran file 1MB',

				'file_tambahan.mimes'		=> 'Hanya boleh file .jpg .jpeg .png .pdf | Max 1MB',
				'file_tambahan.max'			=> 'Maksimal ukuran file 1MB'
			]);
		} 

    	$this->validate($request, [
				'nama'					=> 'required',
				'email'					=> 'required',
				'telpon'				=> 'required',
				'kebutuhan_informasi'	=> 'required',
				'alasan'				=> 'required',
    	],[
				'nama.required'					=> 'Nama harus diisi.',
				'email.required'				=> 'Email harus diisi',
				'telpon.required'				=> 'Nomor telpon harus diisi',
				'kebutuhan_informasi.required'	=> 'Kebutuhan informasi harus diisi',
				'alasan.required'				=> 'Alasan permintaan harus diisi',
    	]);

    	$nama 					=  $request->nama;
		$email					=  $request->email;
		$telpon					=  '62'.$request->telpon;
		$kategori				=  $request->kategori;
		$cara_peroleh			=  $request->cara_peroleh;
		$kebutuhan_informasi	=  $request->kebutuhan_informasi;
		$alasan					=  $request->alasan;
		$prioritas				=  $request->prioritas;
		$kisi 					=  $request->kisi;

    	// cek file identitas dan tambahan
		// jika file ada maka simpan filenya
		$file_identitas = '';
		if( $request->hasFile('file_identitas') ) {                         
            $file_identitas = Fn::simpan_file($request->file('file_identitas'), 'identitas', 'permintaan');
		}
		
		$file_tambahan= '';
		if( $request->hasFile('file_tambahan') ) {                         
            $file_tambahan = Fn::simpan_file($request->file('file_tambahan'), 'tambahan', 'permintaan');
        }
		
		// untuk pertama kali insert ke permintaan
		$qcek_mintadata		=  Permintaan::select('kode_permintaan')->count();
		//ambil kode_mintadata terakhir
		$get_kodemintadata	=  Permintaan::select('kode_permintaan')
											->orderBy('id', 'DESC')
											->first();

		if($qcek_mintadata==0) {
			$kode_mintadata 	= "DAT1";
		} else {
			$kode_mintadata 	=  $get_kodemintadata->kode_permintaan;
			$exp_kode			=  explode('T', $kode_mintadata);
			// tambah "DAT" (kode_mintadata terakhir + 1)
			$kode_mintadata  	=  "DAT" . ($exp_kode[1] + 1);
		}

		//jika kategori user adalah unit kerja, maka ambil unit kerjanya
		$unitkerja = '';
		if(!empty($request->unit_kerja_id)) {
			$unitkerja = $request->unit_kerja_id;
		}

		// insert ketable permintaan dan ambil insertgetid
		$id_mintadata  = DB::table('permintaan')->insertGetId([
							'kode_permintaan'		=>  $kode_mintadata,
							'unit_kerja_id' 		=>	$unitkerja,
							'nama' 					=>  $nama,
							'email'					=>  $email,
							'telpon'				=>  $telpon,
							'kategori'				=>  $kategori,
							'cara_peroleh'			=>  $cara_peroleh,
							'kebutuhan_informasi'	=>  $kebutuhan_informasi,
							'alasan'				=>  $alasan,
							'prioritas'				=>  $prioritas,
							'tag' 					=> 	$request->kategori_user,
							'file_identitas'		=>  $file_identitas,
							'file_tambahan'			=>  $file_tambahan,
							'status'				=> 'Terkirim',
							'created_at'			=>  \Carbon\Carbon::now(),
							'updated_at'			=>  \Carbon\Carbon::now(),
					]);

		//simpan data rekaman jika user memilih rekaman
		if(!empty($kisi)){
			$save_kisi	=	DB::table('rekaman')->insert([
								'permintaan_id'		=>  $id_mintadata,
								'kisi'				=>  $kisi,
								'created_at'		=>  \Carbon\Carbon::now(),
								'updated_at'		=>  \Carbon\Carbon::now(),
				]);
		}

		// simpan status pertama ke table respon_mintadata
		$save 		=  ResponPermintaan::create([
							'permintaan_id'	=>  $id_mintadata,
							'status'		=>  'Terkirim',	
							'note'			=>  'Permintaan data telah terkirim.',						
							'created_at'	=>  \Carbon\Carbon::now(),
							'updated_at'	=>  \Carbon\Carbon::now(),
					]);

		if($save) {
			// Notify::success('Permintaan anda berhasil terkirim', 'Berhasil!');
			//kirim sms ke service desk
			############# kirim notif ke service desk #############				
				$sd = User::where('role', 'sd')->first();			
				Kirim::sms($sd->nohp, 'Ada permintaan masuk. Kode permintaan : '. $kode_mintadata);
				$judul			= 'Ada Permintaan Baru';
				$kategori_data  = 'Permintaan';
				$nama           = $sd->name;
				$kode           = $kode_mintadata;
				$kategori_user  = $request->kategori_user;
				$email_tujuan   = $sd->email;
				$pesan 			= 'Ada permintaan baru, mohon ditindak lanjuti.';
				Mail::send('layouts.email.admin.baru', [
							'nama' 				=> $nama,
							'pesan' 			=> $pesan,
							'kategori_data' 	=> $kategori_data, 
							'kode' 				=> $kode, 
							'kategori_user' 	=> $kategori_user
						], function ($message) use ($judul, $email_tujuan)
				{   
					$message->subject($judul);
					$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					$message->to($email_tujuan);
				});
			############# ./kirim notif ke service desk #############
			
			############# kirim notif ke client #############
				$pesanSms = 'Terimakasih telah melakukan permintaan. Untuk melakukan pelacakan permintaan anda, gunakan email : '.$email.' dan kode : '.$kode_mintadata;
				Kirim::sms($telpon, $pesanSms);
				$kategori_data  = 'Permintaan';
				$judul			= $kategori_data.' terkirim';				
				$nama           = $request->nama;
				$kode           = $kode_mintadata;
				$email_tujuan   = $request->email;
				$pesan 			= 'Permintaan telah terkirim ke Admin HelpDesk.';
				Mail::send('layouts.email.user.baru', [
														'nama'			 => $nama, 
														'pesan'			 => $pesan,
														'kategori_data'  =>	$kategori_data,
														'kode'			 => $kode,
														'kategori_user'	 => $kategori_user, 
														'email'			 => $email_tujuan,
													], function ($message) use ($judul, $email_tujuan)
				{   
					$message->subject($judul);
					$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					$message->to($email_tujuan);
				});
			############# ./kirim notif ke client #############

			Alert::info('
					Permintaan anda telah terkirim ke Administrator HelpDesk. <br>
					<h5 class="m-t-20"><b>Email anda </b> <span class="label label-primary m-l-5">'.$email.'</span></h5>
					<h5 class="m-t-20"><b>Kode Tiket Permintaan Data </b> <span class="label label-primary m-l-5">'.$kode_mintadata.'</span></h5>
					Gunakan kode tiket dan email di atas untuk melacak status permintaan anda. <br>
					Untuk melacak status permintaan data, klik menu "Tracking".
			')->html()->persistent("Keluar");

			return redirect()->back();

			// return view('komplain.alert', compact('kode_komplain', 'email'))
			// 			->with('status_simpan', 'berhasil');
		} else {
			Notify::error('Permintaan anda gagal terkirim', 'Gagal!');
			// return view('komplain.alert')
			// 			->with('status_simpan', 'gagal');
		}

    }
}
