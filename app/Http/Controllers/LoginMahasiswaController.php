<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Notify;
use Alert;

class LoginMahasiswaController extends Controller
{
    public function login()
    {
        return view('login_mahasiswa');
    }

    public function verifikasi(Request $request)
    {
        // dd($request->all());
        // http://203.24.50.30:7475/datasnap/rest/tservermethods1/loginmhs/{nim}/x{password}
        // {
        //     idmhs: 67570,
        //     idprogdi: 218,
        //     idprogram: 1,
        //     idperiode: 353,
        //     statakd: -1,
        //     nama: "ERWIN WAHYUDI",
        //     progdi: "Sistem Komputer",
        //     ips: 0,
        //     maxsks: 12,
        //     ipk: 0,
        //     thakad: "2017/2018",
        //     smt: 2,
        //     iden: 7881,
        //     username: "K11109008",
        //     passwd: "xERWIN"
        //     }
        //cek nim apakah valid atau tidak dengan service
        // $url    = file_get_contents('http://203.24.50.30:7475/datasnap/rest/tservermethods1/loginmhs/'.$request->nim.'/x'.$request->password);
        $url    = file_get_contents('http://service.siakad.untan.ac.id/datasnap/rest/tservermethods1/loginmhs/'.$request->nim.'/x'.$request->password);
        $dt     = json_decode($url, true);
        
        if($dt['result'][0]['idmhs'] > 0) {
            //  return $dt['result'][0];
            // return $dt['result'][0]['idmhs'];
            $request->session()->flush();
            $session     = session([
                                    'nim'   =>  $dt['result'][0]['username'],
                                    'nama'  =>  $dt['result'][0]['nama'],
                                    'prodi' =>  $dt['result'][0]['progdi'],
                                ]);
            Alert::success('Data anda benar. <br>Silahkan lengkapi data sesuai dengan form yang disediakan.', 'Berhasil!')->html()->persistent('Close');
            if($request->kategori == 'komplain') {
                return redirect()->route('mahasiswa.komplain');
            } else {
                return redirect()->route('mahasiswa.permintaan');
            }
        } else {
            Alert::error('Data tidak valid. Silahkan coba lagi.', 'Gagal!');
            return redirect()->back();
        }
    
    }
}
