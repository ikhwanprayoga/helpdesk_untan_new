<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Notify;
use Alert;

class LoginDosenController extends Controller
{
    public function login()
    {
        return view('login_dosen');
    }

    public function verifikasi(Request $request)
    {
        // $url    = file_get_contents('http://192.168.32.98:7475/datasnap/rest/tservermethods1/logindosen/X'.$request->nip.'/X'.$request->password);
        $url    = file_get_contents('http://servicedosen.siakad.untan.ac.id/datasnap/rest/tservermethods1/logindosen/X'.$request->nip.'/X'.$request->password);
        $dt     = json_decode($url, true);
        
        if($dt['result'][0]['stat'] == 'aktif') {
            $request->session()->flush();
            $session     = session([
                                    'nip'   =>  $request->nip,
                                    'nama'  =>  $dt['result'][0]['nama'],
                                ]);
            Alert::success('Data anda benar. <br>Silahkan lengkapi data sesuai dengan form yang disediakan.', 'Berhasil!')->html()->persistent('Close');
            if($request->kategori == 'komplain') {
                return redirect()->route('dosen.komplain');
            } else {
                return redirect()->route('dosen.permintaan');
            }
        } else {
            Alert::error('Data tidak valid. Silahkan coba lagi.', 'Gagal!');
            return redirect()->back();
        }
    
    }
}
