<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\KomplainSiremun;
use App\ResponKomplainSiremun;
use App\ChatKomplainSiremun;
use App\LogChatKomplainSiremun;
use App\Fn;
use App\Kirim;
use App\User;
use App\Link;

use Mail;
use DB;
use Auth;
use Alert;
use Notify;

class DataKomplainSiremunController extends Controller
{
    public function index()
    {
			$data['komplains'] = KomplainSiremun::orderBy('created_at', 'DESC')
													->get();

			foreach ($data['komplains'] as $key => $value) {
				$respon_komplain = ResponKomplainSiremun::select('status')
															->where('komplain_id', $value->id)
															->orderBy('created_at', 'DESC')
															->first();

				$data['komplains'][$key]->status = $respon_komplain;
			}

		return view('admin.siremun.index', compact('data'));
    }

    public function detail($id)
    {
		$data = KomplainSiremun::where('id', $id)->first();
		
		$user_list_keluar = ChatKomplainSiremun::select('id', 'user_tujuan')
										->where('komplain_id', $id)
										->where('user_pengirim', Auth::id())
										->groupBy('user_tujuan')
										->get();

		return view('admin.siremun.detail', compact('data', 'id', 'user_list_keluar'));
    }

    public function send_feedback(Request $request)
    {
		// dd($request->all());
		// return $request->telpon;
    	$komplain_id	=  $request->komplain_id;
    	$status 		=  $request->status;
    	$note			=  $request->note;
        $users_id       =  Auth::user()->id;

		$file = '';
    	if( $request->hasFile('file') ) {                         
            $file = Fn::simpan_file($request->file('file'), 'respon', 'siremun');
		}

		$save = ResponKomplainSiremun::create([
					'komplain_id'	=> $komplain_id,
					'status'		=> $status,
					'note'			=> $note,
					'file'			=> $file,
                    'users_id'      => $users_id,
			]);
		

		//ubah status di table komplain
		$k  = KomplainSiremun::find($komplain_id);
		$k->status = $status;
		$k->save();

		if($save) {
			// Alert::success('Feedback berhasil ditambahkan.', 'Berhasil!')->autoclose(1500);
			############# kirim notif ke client #############
				Kirim::wa($request->telpon, 'Status komplain anda : '.$status.' . Dengan catatan : '.$note.' .Lakukan tracking di web helpdesk untuk melihat detail komplain.');
				// $kategori_data  = 'Komplain';
				// $judul			= 'Pesan dari admin Helpdesk';				
				// $nama           = $k->nama;
				// $kode           = $k->kode_komplain;
				// $email_tujuan   = $k->email;
				// $pesan 			= $request->note;
				// $status			= $status;

				// Mail::send('layouts.email.user.feedback', [
				// 										'nama'			 => $nama, 
				// 										'pesan'			 => $pesan,
				// 										'kategori_data'  =>	$kategori_data,
				// 										'kode'			 => $kode,
				// 										'email'			 => $email_tujuan,
				// 										'status'		 => $status,
				// 									], function ($message) use ($judul, $email_tujuan)
				// {   
				// 	$message->subject($judul);
				// 	$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
				// 	$message->to($email_tujuan);
				// });
			############# ./kirim notif ke client #############
			
			Notify::success('Feedback berhasil ditambahkan.', 'Berhasil!');			
			return redirect()->back();
		} else {
			// Alert::danger('Feedback gagal ditambahkan.', 'Gagal!')->autoclose(1500);
			Notify::error('Feedback gagal ditambahkan.', 'Gagal!');	
			return redirect()->back();
		}
	}
	
	public function forward($id)
	{
		$data = KomplainSiremun::where('id', $id)->first();

		return view('admin.siremun.forward', compact('data'));
	}

	public function forward_save(Request $request, $id)
	{
		// dd($request->all());

		// update pengelola, SD menentukan siapa pj pengelola komplain
		$komplain = KomplainSiremun::find($id);
		$komplain->pj_pengelola = $request->tujuan;
		$komplain->save();

		// tambah chat dari SD ke tujuan
		$chat = new ChatKomplain;
		$chat->komplain_id	 = $id;
		$chat->user_pengirim = Auth::id();
		$chat->user_tujuan   = $request->tujuan;
		$chat->isi			 = $request->pesan;
		$chat->status 		 = 'Belum Baca';
		
		$file = '';
		if( $request->hasFile('file') ) {                         
			$file = Fn::simpan_file($request->file('file'), 'chat', 'komplain');
			$chat->file = $file;
		}
		$chat->save();

		//tambah log chat belum dibaca ketika pertama kali input
		$log = new LogChatKomplain;
		$log->chat_komplain_id 	= $chat->id;
		$log->status 			= 'Belum Baca';

		if($log->save()) {
			// Alert::success('Komplain berhasil diteruskan.', 'Berhasil!')->autoclose(1500);
			############# insert link #############
				// http://localhost:8000/redirect?uid=5&token=vziv4MxseNiiQCEmZmhBNNWg6Z7J2x7jMoaSKOEYt3f4a4br5IiQ57x6y0rL&kat=kom&id=41
				$base_url 	= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
				$token		= md5(date('Ymdhis'));
				$url 		= $base_url."/redirect?uid={$request->tujuan}&t={$token}&k=komp&id={$chat->id}";
				
				$link  = new Link;
				$link->url 		= $url; 
				$link->token	= $token;
				$link->status 	= 0;
				$link->save();
			############# ./insert link #############

			############# kirim notif ke user tujuan #############				
				$user = User::where('id', $request->tujuan)->first();
				Kirim::wa($user->nohp, 'Pak '.Auth::user()->name.' meneruskan komplain. Kode komplain : '. $komplain->kode_komplain.'. Url: '.$url);
				
				//ambil nama tujuan pesan
				$u_tujuan = User::where('id', $request->tujuan)->first();

				$judul			= 'Pesan Masuk dari '.Auth::user()->name;
				$kategori_data  = 'Komplain';
				$nama           = $u_tujuan->name;
				$email_tujuan	= $u_tujuan->email;
				$pengirim_pesan	= Auth::user()->name;
				$kode           = $komplain->kode_komplain;
				$kategori_user  = $komplain->tag;
				$pesan 			= $request->pesan;

				Mail::send('layouts.email.admin.pesan_masuk', [
							'nama' 				=> $nama,
							'pesan' 			=> $pesan,
							'kategori_data' 	=> $kategori_data, 
							'pengirim_pesan'	=> $pengirim_pesan,
							'kode' 				=> $kode, 
							'kategori_user' 	=> $kategori_user,
							'url' 				=> $url,
						], function ($message) use ($judul, $email_tujuan)
				{   
					$message->subject($judul);
					$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					$message->to($email_tujuan);
				});
			############# ./kirim notif ke user tujuan admin #############
			Notify::success('Komplain berhasil diteruskan.', 'Berhasil!');			
			return redirect()->back();
		} else {
			// Alert::danger('Komplain gagal diteruskan.', 'Gagal!')->autoclose(1500);
			Notify::error('Komplain gagal diteruskan.', 'Gagal!');			
			return redirect()->back();
		}
	}
}