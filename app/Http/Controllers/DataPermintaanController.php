<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permintaan;
use App\ResponPermintaan;
use App\ChatPermintaan;
use App\LogChatPermintaan;
use App\Fn;
use App\Kirim;
use App\User;
use App\Link;
use Mail;
use DB;
use Auth;
use Alert;
use Notify;

class DataPermintaanController extends Controller
{
    public function index()
    {
    	$data['permintaans'] = Permintaan::orderBy('created_at', 'DESC')
    											  	  ->get();

    	foreach ($data['permintaans'] as $key => $value) {
    		$respon_permintaan = DB::table('respon_permintaan')->select('status')
    													    ->where('permintaan_id', $value->id)
    													    ->orderBy('created_at', 'DESC')
    													    ->first();

    		$data['permintaans'][$key]->status = $respon_permintaan;
    	}
    	return view('admin.permintaan.index', compact('data'));
    }

    public function detail($id)
    {
		$data = Permintaan::where('id', $id)->first();
		
		$user_list_keluar = ChatPermintaan::select('id', 'user_tujuan')
										->where('permintaan_id', $id)
										->where('user_pengirim', Auth::id())
										->groupBy('user_tujuan')
										->get();

		return view('admin.permintaan.detail', compact('data', 'id', 'user_list_keluar'));
    }

    public function send_feedback(Request $request)
    {
		// print_r($request->all());
		// return $request->telpon;
    	$permintaan_id	=  $request->permintaan_id;
    	$status 		=  $request->status;
    	$note			=  $request->note;
        $users_id       =  Auth::user()->id;

		$file = '';
    	if( $request->hasFile('file') ) {                         
            $file = Fn::simpan_file($request->file('file'), 'respon', 'permintaan');
		}

		//untuk rekaman
        $tujuan_wawancara   =  $request->tujuan_wawancara;
        $tempat             =  $request->tempat;
        $waktu              =  $request->waktu;
		$kisi               =  $request->kisi;
		
		$save_rekaman = DB::table('rekaman')->where('permintaan_id', $permintaan_id)
                                            ->update([
                                            'tujuan_wawancara'  =>  $tujuan_wawancara,
                                            'tempat'            =>  $tempat,
                                            'waktu'             =>  $waktu,
                                            'kisi'              =>  $kisi,
                                            'updated_at'        =>  \Carbon\Carbon::now(),
									]);
		//untuk rekaman

		$save = ResponPermintaan::create([
					'permintaan_id'	=> $permintaan_id,
					'status'		=> $status,
					'note'			=> $note,
					'file'			=> $file,
                    'users_id'      => $users_id,
			]);
		

		//ubah status di table permintaan
		$p  = Permintaan::find($permintaan_id);
		$p->status = $status;
		$p->save();

		if($save) {
			// Alert::success('Feedback berhasil ditambahkan.', 'Berhasil!')->autoclose(1500);
			############# kirim notif ke client #############
				Kirim::sms($request->telpon, 'Status permintaan anda : '.$status.' . Dengan catatan : '.$note.' .Lakukan tracking di web helpdesk untuk melihat detail permintaan.');
				$kategori_data  = 'Permintaan';
				$judul			= 'Pesan dari admin Helpdesk';				
				$nama           = $p->nama;
				$kode           = $p->kode_permintaan;
				$email_tujuan   = $p->email;
				$pesan 			= $request->note;
				$status			= $status;

				Mail::send('layouts.email.user.feedback', [
														'nama'			 => $nama, 
														'pesan'			 => $pesan,
														'kategori_data'  =>	$kategori_data,
														'kode'			 => $kode,
														'email'			 => $email_tujuan,
														'status'		 => $status,
													], function ($message) use ($judul, $email_tujuan)
				{   
					$message->subject($judul);
					$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					$message->to($email_tujuan);
				});
			############# ./kirim notif ke client #############
			
			Notify::success('Feedback berhasil ditambahkan.', 'Berhasil!');			
			return redirect()->back();
		} else {
			// Alert::danger('Feedback gagal ditambahkan.', 'Gagal!')->autoclose(1500);
			Notify::error('Feedback gagal ditambahkan.', 'Gagal!');	
			return redirect()->back();
		}
	}

	public function forward($id)
	{
		$data = Permintaan::where('id', $id)->first();

		return view('admin.permintaan.forward', compact('data'));
	}

	public function forward_save(Request $request, $id)
	{
		// update pengelola, SD menentukan siapa pj pengelola permintaan
		$permintaan = Permintaan::find($id);
		$permintaan->pj_pengelola = $request->tujuan;
		$permintaan->save();

		// tambah chat dari SD ke tujuan
		$chat = new ChatPermintaan;
		$chat->permintaan_id = $id;
		$chat->user_pengirim = Auth::id();
		$chat->user_tujuan   = $request->tujuan;
		$chat->isi			 = $request->pesan;
		$chat->status 		 = 'Belum Baca';
		
		$file = '';
		if( $request->hasFile('file') ) {                         
			$file = Fn::simpan_file($request->file('file'), 'chat', 'permintaan');
			$chat->file = $file;
		}
		$chat->save();

		//tambah log chat belum dibaca ketika pertama kali input
		$log = new LogChatPermintaan;
		$log->chat_permintaan_id = $chat->id;
		$log->status 			 = 'Belum Baca';

		if($log->save()) {
			// Alert::success('permintaan berhasil diteruskan.', 'Berhasil!')->autoclose(1500);
			############# insert link #############
				// http://localhost:8000/redirect?uid=5&token=vziv4MxseNiiQCEmZmhBNNWg6Z7J2x7jMoaSKOEYt3f4a4br5IiQ57x6y0rL&kat=kom&id=41
				$base_url 	= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";
				$token		= md5(date('Ymdhis'));
				$url 		= $base_url."/redirect?uid={$request->tujuan}&t={$token}&k=perm&id={$chat->id}";
				
				$link  = new Link;
				$link->url 		= $url; 
				$link->token	= $token;
				$link->status 	= 0;
				$link->save();
			############# ./insert link #############

			############# kirim notif ke user tujuan #############				
				$user = User::where('id', $request->tujuan)->first();
				Kirim::sms($user->nohp, 'Pak '.Auth::user()->name.' meneruskan permintaan. Kode permintaan : '. $permintaan->kode_permintaan.'. Url: '.$url);
				
				//ambil nama tujuan pesan
				$u_tujuan = User::where('id', $request->tujuan)->first();

				$judul			= 'Pesan Masuk dari '.Auth::user()->name;
				$kategori_data  = 'Permintaan';
				$nama           = $u_tujuan->name;
				$email_tujuan	= $u_tujuan->email;
				$pengirim_pesan	= Auth::user()->name;
				$kode           = $permintaan->kode_permintaan;
				$kategori_user  = $permintaan->tag;
				$pesan 			= $request->pesan;

				Mail::send('layouts.email.admin.pesan_masuk', [
							'nama' 				=> $nama,
							'pesan' 			=> $pesan,
							'kategori_data' 	=> $kategori_data, 
							'pengirim_pesan'	=> $pengirim_pesan,
							'kode' 				=> $kode, 
							'kategori_user' 	=> $kategori_user,
							'url' 				=> $url,
						], function ($message) use ($judul, $email_tujuan)
				{   
					$message->subject($judul);
					$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					$message->to($email_tujuan);
				});
			############# ./kirim notif ke user tujuan admin #############
			Notify::success('Permintaan berhasil diteruskan.', 'Berhasil!');			
			return redirect()->back();
		} else {
			// Alert::danger('permintaan gagal diteruskan.', 'Gagal!')->autoclose(1500);
			Notify::error('Permintaan gagal diteruskan.', 'Gagal!');			
			return redirect()->back();
		}
	}
}
