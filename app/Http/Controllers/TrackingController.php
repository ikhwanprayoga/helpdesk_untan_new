<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komplain;
use App\Permintaan;
use App\ResponKomplainUser;
use App\ResponKomplainUserSiremun;
use App\ResponPermintaanUser;
use App\Fn;
use App\User;
use App\Kirim;
use Mail;
use DB;
use Alert;
use App\KomplainSiremun;

class TrackingController extends Controller
{
    public function index()
    {
    	return view('track.index')->with('status', 'null');
    }

    public function search(Request $request)
    {
    	//remove semua session
		$request->session()->flush();

    	$kode 		=  $request->kode;
    	$email		=  $request->email;
    	$kategori 	=  $request->kategori;

    	if($kategori == 'komplain') {

    		$cek_data	=  DB::table('komplain')->where('kode_komplain', $kode)
    											->where('email', $email)
    											->count();

    		if($cek_data>0){
				//jika data ditemukan buat session
				$session = session([
							'kat_tracking'	=> 'komplain',
							'kode_komplain'	=> $kode,
							'email'			=> $email,
				]);

    			return redirect()->route('tracking.detail');

    		} else {
    			return view('track.index')->with('status', 'failed');
			} 

    	} elseif($kategori == 'permintaan_data') {

    		$cek_data	=  DB::table('permintaan')->where('kode_permintaan', $kode)
    												->where('email', $email)
    												->count();

    		if($cek_data>0){
				$session = session([
							'kat_tracking'		=> 'permintaan',
							'kode_permintaan'	=> $kode,
							'email'				=> $email,
				]);
	    		return redirect()->route('tracking.detail');
    		} else {
    			return view('track.index')->with('status', 'failed');
			}
			
    	} elseif($kategori == 'siremun') {

    		$cek_data	=  DB::table('komplain_siremun')->where('kode_komplain', $kode)
    											->where('email', $email)
    											->count();

    		if($cek_data>0){
				//jika data ditemukan buat session
				$session = session([
							'kat_tracking'	=> 'siremun',
							'kode_komplain'	=> $kode,
							'email'			=> $email,
				]);

    			return redirect()->route('tracking.detail');

    		} else {
    			return view('track.index')->with('status', 'failed');
			}
			
    	}
        // else
        // {
        //     $cek_data   =  DB::table('booking_ruang')->where('kode_booking', $kode)
        //                                             	->where('email', $email)
        //                                             	->count();

        //     if($cek_data>0){
        //         $data['booking']   =  DB::table('booking_ruang')->where('kode_booking', $kode)
        //                                                         ->where('email', $email)
        //                                                         ->get();
        //         foreach ($data['booking'] as $key => $booking) {
        //             $data['booking'][$key]->respon  =  DB::table('respon_booking')->where('booking_ruang_id', $booking->id)
        //                                                                             ->get();
        //         }

        //         return view('track.detail_booking', compact('data'));

        //     } else {
        //         return view('track.index')->with('status', 'failed');
        //     }
        // }
	}
	
	public function detail(Request $request)
    {
    	// echo "<pre>"; print_r($request->all()); echo "</pre>";
		if( empty($request->session()->has('kat_tracking')) ) {
			Alert::error('Anda tidak memili izin akses halaman. <br>Silahkan lakukan tracking ulang.', 'Error!')->html()->persistent('Close');
			return redirect()->back();
		}

    	if(session('kat_tracking') == 'komplain') {

			$kode	= session('kode_komplain');
			$email	= session('email');
			$data	=  Komplain::where('kode_komplain', $kode)
											->where('email', $email)
											->first();

			return view('track.detail_komplain', compact('data'));

    	} elseif(session('kat_tracking') == 'permintaan') {
			
			$kode	= session('kode_permintaan');
			$email	= session('email');
			$data	=  Permintaan::where('kode_permintaan', $kode)
									->where('email', $email)
									->first();
			
			return view('track.detail_mintadata', compact('data'));

		} elseif(session('kat_tracking') == 'siremun') {

			$kode	= session('kode_komplain');
			$email	= session('email');
			$data	=  KomplainSiremun::where('kode_komplain', $kode)
											->where('email', $email)
											->first();

			return view('track.detail_siremun', compact('data'));

    	}
		
        // else
        // {
        //     $cek_data   =  DB::table('booking_ruang')->where('kode_booking', $kode)
        //                                             	->where('email', $email)
        //                                             	->count();

        //     if($cek_data>0){
        //         $data['booking']   =  DB::table('booking_ruang')->where('kode_booking', $kode)
        //                                                         ->where('email', $email)
        //                                                         ->get();
        //         foreach ($data['booking'] as $key => $booking) {
        //             $data['booking'][$key]->respon  =  DB::table('respon_booking')->where('booking_ruang_id', $booking->id)
        //                                                                             ->get();
        //         }

        //         return view('track.detail_booking', compact('data'));

        //     } else {
        //         return view('track.index')->with('status', 'failed');
        //     }
        // }
	}
	
	public function kirim_catatan(Request $request)
    {
    	// echo "<pre>"; print_r($request->all()); echo "</pre>";
		if( empty($request->session()->has('kat_tracking')) ) {
			Alert::error('Anda tidak memili izin akses halaman. <br>Silahkan lakukan tracking ulang.', 'Error!')->html()->persistent('Close');
			return redirect()->back();
		}

		return view('track.kirim_catatan');
	}

	public function kirim_catatan_save(Request $request)
	{
		// dd($request->all());
		if( empty($request->session()->has('kat_tracking')) ) {
			Alert::error('Anda tidak memili izin akses halaman. <br>Silahkan lakukan tracking ulang.', 'Error!')->html()->persistent('Close');
			return redirect()->back();
		}

		if( session('kat_tracking') == 'komplain' ) {
			//ambil komplain id
			$komplain = Komplain::where('kode_komplain', session('kode_komplain'))->first();
			
			$res = new ResponKomplainUser;
			$res->komplain_id 	= $komplain->id;
			$res->catatan		= $request->catatan;

			$file = '';
			if($request->hasFile('file'))
			{
				$file 		= Fn::simpan_file($request->file('file'), 'res_user', 'komplain');
				$res->file = $file;
			}

			if($res->save()) {
				############# kirim notif ke service desk #############				
					$sd = User::where('role', 'sd')->first();			
					Kirim::wa($sd->nohp, 'User memberikan respon atas komplain : '. session('kode_komplain'));
					
					$judul			= 'Respon Komplain kode '.$komplain->kode_komplain;
					$kategori_data  = 'Komplain';
					$nama           = $sd->name;;
					$email_tujuan   = $sd->email;
					$pengirim_pesan	= $komplain->nama.' - '.$komplain->kode_komplain;
					$kode           = $komplain->kode_komplain;
					$kategori_user  = $komplain->tag;
					$pesan 			= $request->catatan;

					Mail::send('layouts.email.admin.pesan_masuk', [
								'nama' 				=> $nama,
								'pesan' 			=> $pesan,
								'kategori_data' 	=> $kategori_data, 
								'pengirim_pesan'	=> $pengirim_pesan,
								'kode' 				=> $kode, 
								'kategori_user' 	=> $kategori_user
							], function ($message) use ($judul, $email_tujuan)
					{   
						$message->subject($judul);
						$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
						$message->to($email_tujuan);
					});
				############# ./kirim notif ke service desk #############
				
				############# kirim notif ke client #############
					Kirim::wa($komplain->telpon, 'Terimakasih telah memberikan respon atas komplain anda. Untuk melakukan pelacakan komplain, gunakan kode : '. $komplain->kode_komplain.' dan email : '.$komplain->email);
					$kategori_data  = 'Komplain';
					$judul			= 'Respon '.$kategori_data.' terkirim';				
					$nama           = $komplain->nama;
					$kode           = $komplain->kode_komplain;
					$email_tujuan   = $komplain->email;
					$pesan 			= 'Respon Komplain telah terkirim ke Admin HelpDesk.';
					Mail::send('layouts.email.user.baru', [
															'nama'			 => $nama, 
															'pesan'			 => $pesan,
															'kategori_data'  =>	$kategori_data,
															'kode'			 => $kode,
															'kategori_user'	 => $kategori_user, 
															'email'			 => $email_tujuan,
														], function ($message) use ($judul, $email_tujuan)
					{   
						$message->subject($judul);
						$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
						$message->to($email_tujuan);
					});
				############# ./kirim notif ke client #############
	
				Alert::info('
						Respon anda berhasil terkirim ke Admin HelpDesk.<br>
						Terimakasih telah memberikan respon.
				')->html()->persistent("Keluar");
	
				return redirect()->route('tracking.detail');
			} else {
				Alert::error('Terjadi kesalahan, respon gagal dikirim. Coba lagi!', 'Gagal!')->persisten('OK');
				return redirect()->back();
			}
		} elseif( session('kat_tracking') == 'permintaan' ) {
			//ambil permintaan id
			$permintaan = Permintaan::where('kode_permintaan', session('kode_permintaan'))->first();
			
			$res = new ResponPermintaanUser;
			$res->permintaan_id = $permintaan->id;
			$res->catatan		= $request->catatan;

			$file = '';
			if($request->hasFile('file'))
			{
				$file 		= Fn::simpan_file($request->file('file'), 'res_user', 'permintaan');
				$res->file = $file;
			}

			if($res->save()) {
				############# kirim notif ke service desk #############				
					$sd = User::where('role', 'sd')->first();			
					Kirim::wa($sd->nohp, 'User memberikan respon atas permintaan : '. session('kode_permintaan'));
					
					$judul			= 'Respon Permintaan kode '.$permintaan->kode_permintaan;
					$kategori_data  = 'Permintaan';
					$nama           = $sd->name;;
					$email_tujuan   = $sd->email;
					$pengirim_pesan	= $permintaan->nama.' - '.$permintaan->kode_permintaan;
					$kode           = $permintaan->kode_permintaan;
					$kategori_user  = $permintaan->tag;
					$pesan 			= $request->catatan;

					Mail::send('layouts.email.admin.pesan_masuk', [
								'nama' 				=> $nama,
								'pesan' 			=> $pesan,
								'kategori_data' 	=> $kategori_data, 
								'pengirim_pesan'	=> $pengirim_pesan,
								'kode' 				=> $kode, 
								'kategori_user' 	=> $kategori_user
							], function ($message) use ($judul, $email_tujuan)
					{   
						$message->subject($judul);
						$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
						$message->to($email_tujuan);
					});
				############# ./kirim notif ke service desk #############
				
				############# kirim notif ke client #############
					Kirim::wa($permintaan->telpon, 'Terimakasih telah memberikan respon atas permintaan anda. Untuk melakukan pelacakan permintaan, gunakan kode : '. $permintaan->kode_permintaan.' dan email : '.$permintaan->email);
					$kategori_data  = 'Permintaan';
					$judul			= 'Respon '.$kategori_data.' terkirim';				
					$nama           = $permintaan->nama;
					$kode           = $permintaan->kode_permintaan;
					$email_tujuan   = $permintaan->email;
					$pesan 			= 'Respon permintaan telah terkirim ke Admin HelpDesk.';
					Mail::send('layouts.email.user.baru', [
															'nama'			 => $nama, 
															'pesan'			 => $pesan,
															'kategori_data'  =>	$kategori_data,
															'kode'			 => $kode,
															'kategori_user'	 => $kategori_user, 
															'email'			 => $email_tujuan,
														], function ($message) use ($judul, $email_tujuan)
					{   
						$message->subject($judul);
						$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
						$message->to($email_tujuan);
					});
				############# ./kirim notif ke client #############
	
				Alert::info('
						Respon anda berhasil terkirim ke Admin HelpDesk.<br>
						Terimakasih telah memberikan respon.
				')->html()->persistent("Keluar");
	
				return redirect()->route('tracking.detail');
			} else {
				Alert::error('Terjadi kesalahan, respon gagal dikirim. Coba lagi!', 'Gagal!')->persisten('OK');
				return redirect()->back();
			}
		} elseif( session('kat_tracking') == 'siremun' ) {
			//ambil komplain id
			$komplain = KomplainSiremun::where('kode_komplain', session('kode_komplain'))->first();
			
			$res = new ResponKomplainUserSiremun;
			$res->komplain_id 	= $komplain->id;
			$res->catatan		= $request->catatan;

			$file = '';
			if($request->hasFile('file'))
			{
				$file 		= Fn::simpan_file($request->file('file'), 'res_user', 'siremun');
				$res->file = $file;
			}

			if($res->save()) {
				############# kirim notif ke service desk #############				
					// $sd = User::where('role', 'sd')->first();
					$sd = User::where('email', 'zuhri@untan.ac.id')->first();			
					Kirim::wa($sd->nohp, 'User memberikan respon atas komplain siremun : '. session('kode_komplain'));
					
					// $judul			= 'Respon Komplain kode '.$komplain->kode_komplain;
					// $kategori_data  = 'Komplain';
					// $nama           = $sd->name;;
					// $email_tujuan   = $sd->email;
					// $pengirim_pesan	= $komplain->nama.' - '.$komplain->kode_komplain;
					// $kode           = $komplain->kode_komplain;
					// $kategori_user  = $komplain->tag;
					// $pesan 			= $request->catatan;

					// Mail::send('layouts.email.admin.pesan_masuk', [
					// 			'nama' 				=> $nama,
					// 			'pesan' 			=> $pesan,
					// 			'kategori_data' 	=> $kategori_data, 
					// 			'pengirim_pesan'	=> $pengirim_pesan,
					// 			'kode' 				=> $kode, 
					// 			'kategori_user' 	=> $kategori_user,
					// 			'url' 				=> 'http://helpdesk.untan.ac.id/'
					// 		], function ($message) use ($judul, $email_tujuan)
					// {   
					// 	$message->subject($judul);
					// 	$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					// 	$message->to($email_tujuan);
					// });
				############# ./kirim notif ke service desk #############
				
				############# kirim notif ke client #############
					Kirim::wa($komplain->telpon, 'Terimakasih telah memberikan respon atas komplain anda. Untuk melakukan pelacakan komplain, gunakan kode : '. $komplain->kode_komplain.' dan email : '.$komplain->email);
					// $kategori_data  = 'Komplain';
					// $judul			= 'Respon '.$kategori_data.' terkirim';				
					// $nama           = $komplain->nama;
					// $kode           = $komplain->kode_komplain;
					// $email_tujuan   = $komplain->email;
					// $pesan 			= 'Respon Komplain telah terkirim ke Admin HelpDesk.';
					// Mail::send('layouts.email.user.baru', [
					// 										'nama'			 => $nama, 
					// 										'pesan'			 => $pesan,
					// 										'kategori_data'  =>	$kategori_data,
					// 										'kode'			 => $kode,
					// 										'kategori_user'	 => $kategori_user, 
					// 										'email'			 => $email_tujuan,
					// 									], function ($message) use ($judul, $email_tujuan)
					// {   
					// 	$message->subject($judul);
					// 	$message->from('helpdesk@untan.ac.id', 'HelpDesk Untan');
					// 	$message->to($email_tujuan);
					// });
				############# ./kirim notif ke client #############
	
				Alert::info('
						Respon anda berhasil terkirim ke Admin HelpDesk.<br>
						Terimakasih telah memberikan respon.
				')->html()->persistent("Keluar");
	
				return redirect()->route('tracking.detail');
			} else {
				Alert::error('Terjadi kesalahan, respon gagal dikirim. Coba lagi!', 'Gagal!')->persisten('OK');
				return redirect()->back();
			}
		}
	}
}
