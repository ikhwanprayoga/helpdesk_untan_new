<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use Auth;
use Alert;


class UserController extends Controller
{
    public function index()
    {
        $data = User::where('id', Auth::id())->first();

        return view('admin.profil.edit', compact('data'));
    }

    public function save(Request $request)
    {
        // dd( $request->all() );
        $user  = User::find(Auth::id());
        $user->username      = $request->username;
        $user->name          = $request->name;         
        $user->email         = $request->email;
        $user->nohp          = '62'.(int)$request->nohp; 

        // if( $request->hasFile('photo') ) {                         
        //     $file_identitas = Fn::simpan_file($request->file('file_identitas'), 'identitas', 'komplain');
		// }

        if(!empty($request->password)) {
            $user->password          = bcrypt($request->password); 
        }

        if($user->save()) {
            // return redirect()->back()
            //                  ->with('alert', 'Berhasil memperbaharui data.')
            //                  ->with('label_alert', 'success')
            //                  ->with('header_alert', 'Berhasil');
            Alert::success('Profil berhasil diubah.', 'Berhasil!');
            return redirect()->back();
        } else {
            // return redirect()->back()
            //                  ->with('alert', 'Gagal memperbaharui data anda. Silahkan coba lagi.')
            //                  ->with('label_alert', 'danger')
            //                  ->with('header_alert', 'Gagal');
            Alert::error('Profil gagal diubah. Coba lagi.', 'Gagal!');
            return redirect()->back();
        }
    }
}
