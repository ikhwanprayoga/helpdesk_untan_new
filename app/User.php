<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'role', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function respon_booking() {
        return $this->hasMany('App\ResponBooking');
    }

    public function komplain()
    {
        return $this->hasMany('\App\Komplain', 'pj_pengelola', 'id');
    }

    public function chat_komplain_kirim()
    {
        return $this->hasMany('\App\ChatKomplain', 'user_pengirim', 'id');
    }

    public function chat_komplain_tujuan()
    {
        return $this->hasMany('\App\ChatKomplain', 'user_tujuan', 'id');
    }

    ############# permintaan #############
    public function permintaan()
    {
        return $this->hasMany('\App\Permintaan', 'pj_pengelola', 'id');
    }

    public function chat_permintaan_kirim()
    {
        return $this->hasMany('\App\ChatPermintaan', 'user_pengirim', 'id');
    }

    public function chat_permintaan_tujuan()
    {
        return $this->hasMany('\App\ChatPermintaan', 'user_tujuan', 'id');
    }
    ############# ./permintaan #############
}
