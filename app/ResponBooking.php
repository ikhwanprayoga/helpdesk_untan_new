<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponBooking extends Model
{
    protected $table 	= 'respon_booking';
    protected $fillable	= ['booking_ruang_id', 'users_id', 'status', 'note', 'file'];

    public function booking_ruang() {
    	return $this->belongsTo('App\BookingRuang');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }
}
