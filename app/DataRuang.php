<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataRuang extends Model
{
    protected $table 	= 'data_ruang';
    protected $fillable	= ['kode_ruang', 'nama_ruang', 'kapasitas', 'lokasi', 'foto'];

    public function booking_ruang() {
    	return $this->hasMany('App\BookingRuang');
    }
}
