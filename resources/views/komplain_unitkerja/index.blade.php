@extends('layouts.main_template')
@section('judul_konten', 'Permohonan Komplain (Kategori Unit Kerja)')
@section('breadcrumb', 'Komplain / Umum')

@section('konten')
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Input Data Pengajuan Komplain</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Lengkapi data berikut sesuai dengan permasalahan anda :
                </p>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'komplain', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
                            <input type="hidden" name="kategori_user" value="unitkerja">
                            <div class="form-group {{ $errors->has('nama') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nama Lengkap</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="nama">                                    
                                    {!! $errors->has('nama') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nama') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('nama') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nama Unit Kerja</label>
                                <div class="col-md-10">
                                    <select class="form-control select2" name="unit_kerja_id">
                                        @foreach(\App\UnitKerja::orderBy('nama_unit_kerja', 'ASC')->get() as $uk)
                                            <option value="{{ $uk->id }}">{{ $uk->nama_unit_kerja }}</option>
                                        @endforeach
                                    </select> </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label" for="example-email">Email</label>
                                <div class="col-md-10">
                                    <input type="email" id="example-email" name="email" class="form-control">
                                    {!! $errors->has('email') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('email') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telpon') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nomor Telepon</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="+62" disabled="">
                                </div>
                                <div class="col-md-8">
                                    <input type="number" class="form-control"  id="no_hp" name="telpon">
                                    {!! $errors->has('telpon') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('telpon') }} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategori Komplain</label>
                                <div class="col-sm-5" id="kategori">
                                    <select class="form-control bootstrap-select" name="kategori">
                                        <option value="siakad" data-toggle="tooltip" data-placement="right" title="Komplain kategori SIAKAD">SIAKAD</option>
                                        <option value="edom" data-toggle="tooltip" data-placement="right" title="Komplain kategori EDOM">EDOM</option>
                                        <option value="efom" data-toggle="tooltip" data-placement="right" title="Komplain kategori EFOM">EFOM</option>
                                        <option value="elom" data-toggle="tooltip" data-placement="right" title="Komplain kategori ELOM">ELOM</option>
                                        <option value="ejournal" data-toggle="tooltip" data-placement="right" title="Komplain kategori E">E-Journal</option>
                                        <option value="elearning" data-toggle="tooltip" data-placement="right" title="Komplain kategori E">E-Learning</option>
                                        <option value="eoffice" data-toggle="tooltip" data-placement="right" title="Komplain kategori E">E-Office</option>  
                                        <option value="email" data-toggle="tooltip" data-placement="right" title="Komplain kategori Email">Email</option>
                                        <option value="repodig" data-toggle="tooltip" data-placement="right" title="Komplain kategori Repodig">Repodig</option>                                          
                                        <option value="hosting" data-toggle="tooltip" data-placement="right" title="Komplain kategori Hosting">Hosting</option>
                                        <option value="domain" data-toggle="tooltip" data-placement="right" title="Komplain kategori Domain">Domain</option>
                                        <option value="jaringan" data-toggle="tooltip" data-placement="right" title="Komplain kategori Jaringan">Jaringan</option>
                                        <option value="internet" data-toggle="tooltip" data-placement="right" title="Komplain kategori Internet">Internet</option>
                                        <option value="lainnya" data-toggle="tooltip" data-placement="right" title="Komplain kategori Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>   
                            <div class="form-group {{ $errors->has('judul_komplain') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Judul Komplain</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="judul_komplain">
                                    {!! $errors->has('judul_komplain') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('judul_komplain') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('isi_komplain') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Isi Komplain</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" name="isi_komplain"></textarea>
                                    {!! $errors->has('isi_komplain') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('isi_komplain') }} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Prioritas</label>
                                <div class="col-sm-5">
                                    <select class="select-prioritas form-control" name="prioritas">
                                        <option value="tidak mendesak">Tidak Mendesak</option>
                                        <option value="mendesak">Mendesak</option>
                                        <option value="sangat mendesak">Sangat Mendesak</option>
                                    </select>
                                </div>
                            </div>                            
                            <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Upload Identitas Anda</label>
                                <div class="col-md-5">
	                                <input type="file" class="filestyle" data-size="sm" name="file_identitas" accept=".jpg, .jpeg, .png, .pdf">
                                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('file_tambahan') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Upload File Pendukung</label>
                                <div class="col-md-5">
	                                <input type="file" class="filestyle" data-size="sm" name="file_tambahan" accept=".jpg, .jpeg, .png, .pdf">
                                    <span class="help-block"> {{ $errors->first('file_tambahan') }} </span>
                                </div>
                                <div class="col-md-5">
	                    			<p class="text-muted m-b-15 font-13">
	                                    Upload hanya jika diperlukan file pendukung
	                                </p>	                                
                                </div>
                            </div>
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Kirim Data
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
