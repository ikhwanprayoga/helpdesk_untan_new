@extends('layouts.main_template')
@section('judul_konten', 'Permintaan Data (Kategori Mahasiswa)')
@section('breadcrumb', 'Permintaan Data / Mahasiswa')

@section('konten')
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Input Permintaan Data</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Lengkapi data berikut sesuai dengan data yang ingin diminta :
                </p>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'permintaan', 'role' => 'form', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
                        <input type="hidden" name="kategori_user" value="mahasiswa">
                        <input type="hidden" name="nama" value="{{ session('nim') }} - {{ session('nama') }}">
                        <div class="form-group {{ $errors->has('nama') ? 'has-error has-feedback' : '' }}">
                            <label class="col-md-2 control-label">Nama Lengkap</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" value="{{ session('nim') }} - {{ session('nama') }}" disabled>                                    
                                {!! $errors->has('nama') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                <span class="help-block"> {{ $errors->first('nama') }} </span>
                            </div>
                        </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label" for="example-email">Email</label>
                                <div class="col-md-10">
                                    <input type="email" id="example-email" name="email" class="form-control">
                                    {!! $errors->has('email') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('email') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telpon') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nomor Telepon</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="+62" disabled="">
                                </div>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" id="no_hp" name="telpon">
                                    {!! $errors->has('telpon') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('telpon') }} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategori Permintaan</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="kategori" id="kategori">
                                        <option value="siakad" data-toggle="tooltip" data-placement="right" title="Komplain kategori SIAKAD">SIAKAD</option>
                                        <option value="edom" data-toggle="tooltip" data-placement="right" title="Komplain kategori EDOM">EDOM</option>
                                        <option value="efom" data-toggle="tooltip" data-placement="right" title="Komplain kategori EFOM">EFOM</option>
                                        <option value="elom" data-toggle="tooltip" data-placement="right" title="Komplain kategori ELOM">ELOM</option>
                                        <option value="ejournal" data-toggle="tooltip" data-placement="right" title="Komplain kategori E">E-Journal</option>
                                        <option value="elearning" data-toggle="tooltip" data-placement="right" title="Komplain kategori E">E-Learning</option>
                                        <option value="eoffice" data-toggle="tooltip" data-placement="right" title="Komplain kategori E">E-Office</option>  
                                        <option value="email" data-toggle="tooltip" data-placement="right" title="Komplain kategori Email">Email</option>
                                        <option value="repodig" data-toggle="tooltip" data-placement="right" title="Komplain kategori Repodig">Repodig</option>                                          
                                        <option value="hosting" data-toggle="tooltip" data-placement="right" title="Komplain kategori Hosting">Hosting</option>
                                        <option value="domain" data-toggle="tooltip" data-placement="right" title="Komplain kategori Domain">Domain</option>
                                        <option value="jaringan" data-toggle="tooltip" data-placement="right" title="Komplain kategori Jaringan">Jaringan</option>
                                        <option value="internet" data-toggle="tooltip" data-placement="right" title="Komplain kategori Internet">Internet</option>
                                        <option value="lainnya" data-toggle="tooltip" data-placement="right" title="Komplain kategori Lainnya">Lainnya</option>
                                    </select>
                                </div>
                            </div>     
                            <div class="form-group">
                                <label class="col-md-2 control-label">Informasi yang Dibutuhkan</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" name="kebutuhan_informasi"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Alasan Permintaan</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" name="alasan"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Prioritas</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="prioritas">
                                        <option value="tidak mendesak">Tidak Mendesak</option>
                                        <option value="mendesak">Mendesak</option>
                                        <option value="sangat mendesak">Sangat Mendesak</option>
                                    </select>
                                </div>
                            </div>        
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Cara mendapatkan informasi melalui</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="cara_peroleh" id="cara_peroleh">
                                        <option value="langsung">Langsung</option>
                                        <option value="rekaman">Rekaman</option>
                                        <option value="website">Website Helpdesk</option>
                                        <option value="email">Email</option>
                                    </select>
                                </div>
                            </div>       
                            
                            
                            {{-- rekaman --}}
                            <div id="rekaman" style="display: none;">
                                <hr>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Kisi-kisi Rekaman</label>
                                    <div class="col-md-10">
                                        <textarea class="form-control" id="kisi" rows="5" name="kisi" placeholder="Isi kisi-kisi rekaman dalam point. Contoh: - Point pertama, Point kedua"></textarea>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            {{-- ./rekaman --}}
                            

                            <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Upload Identitas Anda</label>
                                <div class="col-md-5">
                                    <input type="file" class="filestyle" data-size="sm" name="file_identitas" accept=".png, .jpg, .jpeg, .pdf">
                                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('file_tambahan') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Upload File Pendukung</label>
                                <div class="col-md-5">
                                    <input type="file" class="filestyle" data-size="sm" name="file_tambahan" accept=".png, .jpg, .jpeg, .pdf">
                                    <span class="help-block"> {{ $errors->first('file_tambahan') }} </span>
                                </div>
                                <div class="col-md-5">
                                    <p class="text-muted m-b-15 font-13">
                                        Upload hanya jika diperlukan file pendukung
                                    </p>                                    
                                </div>
                            </div>
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Kirim Data
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    


                </div>
            </div>
        </div>
    </div>
@endsection
