@extends('layouts.main_template')
@section('judul_konten', 'Forward Komplain')
@section('breadcrumb', 'Komplain / Pesan Masuk / List / Forward')

@section('konten')
    <div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Forward Komplain</b></h4><hr>
                <form action="{{ route('admin.komplain.pesan.masuk.forward.save', $chat->id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                    <input type="hidden" name="komplain_id" value="{{ $chat->komplain->id }}">
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Kode Komplain</label>
                        <div class="col-md-6">
                            <p style="padding: 6px 12px;"><code>{{ $chat->komplain->kode_komplain }}</code></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Hal</label>
                        <div class="col-md-6">
                            <p style="padding: 6px 12px;"><b class="text-success">{{ $chat->komplain->judul }}</b></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Tujuan</label>
                        <div class="col-md-6">
                            <select class="form-control select2" name="tujuan">
                                @if(Auth::user()->role == 'pg')
                                    @foreach(\App\User::where('role', '!=', 'pg')->get() as $u )
                                        <option value="{{ $u->id }}" @if(isset($_GET['uid'])) {{ ($_GET['uid'] == $u->id) ? 'selected' : '' }} @endif />{{ $u->name }}</option>
                                    @endforeach
                                @elseif(Auth::user()->role == 'tk')
                                    @foreach(\App\User::where('role', 'pg')->get() as $u )
                                        <option value="{{ $u->id }}" @if(isset($_GET['uid'])) {{ ($_GET['uid'] == $u->id) ? 'selected' : '' }} @endif />{{ $u->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="pesan" required></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file') ? 'has-error has-feedback' : '' }}">
                        <label class="col-md-2 control-label">Lampiran</label>
                        <div class="col-md-5">
                            <input type="file" class="filestyle" data-size="sm" name="file">
                            <span class="help-block"> {{ $errors->first('file') }} </span>
                        </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				</form>
			</div>
		</div>
    </div>
    

    {{-- modal ni --}}
    <!-- Bootstrap Modals -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">File</h4>
                    </div>
                    <div class="modal-body">
                            <iframe id="embed-pdf" style="width:100%;height:800px;" frameborder="0"></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    {{-- modal ni --}}


@endsection