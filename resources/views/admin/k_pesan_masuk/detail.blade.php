@extends('layouts.main_template')
@section('judul_konten', 'List Pesan Masuk')
@section('breadcrumb', 'Komplain / Pesan Masuk / List')

@section('konten')

    <div class="portlet">
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet-heading portlet-default">
					<h3 class="portlet-title text-dark">
                        <b>Detail Komplain</b>
					</h3>
					<div class="portlet-widgets">
						<a href="javascript:;" data-toggle="reload">
							<i class="ion-refresh"></i>
						</a>
						<span class="divider"></span>
						<a data-toggle="collapse" data-parent="#accordion1" href="#bg-default" class="collapsed" aria-expanded="false">
							<i class="ion-minus-round"></i>
						</a>
						<!-- <span class="divider"></span> -->
						<!-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> -->
					</div>
					<div class="clearfix"></div>
				</div>
				<div id="bg-default" class="panel-collapse collapse aria-expanded=" false "">
					<div class="portlet-body">
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Kode Komplain</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->kode_komplain }}</p> 	
                                </div>
                            </div>  
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Nama</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->nama }}</p> 		
                                </div>
                            </div>
        
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Email</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->email }}</p> 	 	
                                </div>
                            </div>  
                                            
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Telpon</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->telpon }}</p> 		
                                </div>
                            </div>  	
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Kategori User</label>
                                <div class="col-md-8">
                                    <span class="label label-inverse">{{ $komplain->tag }}</span> 	
                                </div>
                            </div>  	
        
                            @if($komplain->tag == 'unitkerja')
                            <div class="row form-group">
                                    <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Unit Kerja</label>
                                    <div class="col-md-8">
                                        <p>{{ $komplain->unit_kerja->nama_unit_kerja }}</p> 	
                                    </div>
                                </div> 
                            @endif

                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Kategori</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->kategori }}</p> 	
                                </div>
                            </div>  
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Judul</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->judul }}</p> 		
                                </div>
                            </div>  	
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Isi</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->isi}}</p> 		
                                </div>
                            </div>  
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Prioritas</label>
                                <div class="col-md-8">
                                    <p>{{ $komplain->prioritas }}</p> 	 	
                                </div>
                            </div>  	
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
                                <div class="col-md-8">
                                @if(!empty($komplain->file_identitas))
                                    @php
                                        $ext_fidentitas = pathinfo($komplain->file_identitas, PATHINFO_EXTENSION);
                                    @endphp

                                    @if($ext_fidentitas == 'pdf') 
                                        <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $komplain->file_identitas }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                    @elseif($ext_fidentitas == 'jpg' || $ext_fidentitas == 'jpeg' || $ext_fidentitas == 'png')
                                        <a target="_BLANK" href="{{ asset('storage/komplain/'.$komplain->file_identitas) }}" data-toggle="lightbox" data-title="File Identitas" data-footer="{{ $komplain->file_identitas }}" data-gallery="imagesizes">
                                            <i class="md md-file-download"></i>&nbsp;Lihat file
                                        </a>
                                    @else
                                        <a target="_BLANK" href="{{ asset('storage/komplain/'.$komplain->file_identitas) }}">
                                            <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                        </a>
                                    @endif
                                @endif
                                </div>
                            </div> 
                            <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">File Tambahan</label>
                                <div class="col-md-8">
                                @if(!empty($komplain->file_tambahan))
                                    @php
                                        $ext_ftambahan = pathinfo($komplain->file_tambahan, PATHINFO_EXTENSION);
                                    @endphp

                                    @if($ext_ftambahan == 'pdf') 
                                        <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $komplain->file_tambahan }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                    @elseif($ext_ftambahan == 'jpg' || $ext_ftambahan == 'jpeg' || $ext_ftambahan == 'png')
                                        <a href="{{ asset('storage/komplain/'.$komplain->file_tambahan) }}" data-toggle="lightbox" data-title="File Pendukung" data-footer="{{ $komplain->file_tambahan }}" data-gallery="imagesizes">
                                            <i class="md md-file-download"></i>&nbsp;Lihat file
                                        </a>								
                                    @else
                                        <a target="_BLANK" href="{{ asset('storage/komplain/'.$komplain->file_tambahan) }}">
                                            <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                        </a>
                                    @endif
                                @endif
                                </div>
                            </div> 
                    <span class="clearfix"></span>
					</div>
				</div>
			</div>
		</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><b>Percakapan</b> dengan <span class="text-primary">{{ $teman_chat->name }}</span></h4><hr>

                <div class="chat-conversation">
                    
                    <ul class="conversation-list nicescroll" tabindex="5001" style="overflow: hidden; outline: none;">
                    @foreach($chating as $chat)
                        @if($chat->user_pengirim == Auth::id())
                        <li class="clearfix odd">
                            <div class="chat-avatar">
                                <img src="{{ asset('images/'.$chat->userpengirim->photo) }}" alt="{{ $chat->userpengirim->name }}">
                                <i>{{ $chat->created_at->diffForHumans() }}</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>{{ $chat->userpengirim->name }}</i>&ensp;
                                    @if($chat->status == 'Belum Baca')
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-check-square-o" style="color: #337ab7;"></i>
                                    @endif
                                    <p>
                                        {{ $chat->isi }}
                                    </p>
                                    <p>
                                        @if(!empty($chat->file) )
                                            <a target="_BLANK" href="{{ asset('storage/komplain/'.$chat->file) }}" data-toggle="lightbox" data-title="File Lampiran" data-footer="{{ $chat->file }}"><i class="fa fa-download"></i>&ensp;Download File</a>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </li>
                        @else
                        <li class="clearfix">
                            <div class="chat-avatar">
                                    <img src="{{ asset('images/'.$chat->userpengirim->photo) }}" alt="{{ $chat->userpengirim->name }}">
                                    <i>{{ $chat->created_at->diffForHumans() }}</i>
                            </div>
                            <div class="conversation-text">
                                <div class="ctext-wrap">
                                    <i>{{ $chat->userpengirim->name }}</i>                                    
                                    <p>
                                            {{ $chat->isi }}
                                    </p>
                                    <p>
                                        @if(!empty($chat->file))
                                            <a target="_BLANK" href="{{ asset('storage/komplain/'.$chat->file) }}" data-toggle="lightbox" data-title="File Lampiran" data-footer="{{ $chat->file }}"><i class="fa fa-download"></i>&ensp;Download File</a>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </li>
                        @endif
                    @endforeach

                    <hr>
                    
                    <form action="{{ route('admin.komplain.pesan.masuk.forward.save', $last_chat_id->id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }} 
                            <input type="hidden" name="komplain_id" value="{{ $komplain->id }}">
                            <input type="hidden" name="tujuan" value="{{ $user_id }}">
                            <div class="row">
                                <div class="col-sm-9 chat-inputbar">
                                    <input type="text" class="form-control chat-input" name="pesan" placeholder="Enter your text">
                                </div>                        
                            </div><p></p>
                            <div class="row">
                                <div class="col-sm-9 chat-inputbar">
                                    <input type="file" class="filestyle" data-size="sm" name="file">
                                </div>
                                <div class="col-sm-3 chat-send">
                                    <button type="submit" class="btn btn-sm btn-info btn-block waves-effect waves-light">Send</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    {{-- modal ni --}}
    <!-- Bootstrap Modals -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">File</h4>
                    </div>
                    <div class="modal-body">
                            <iframe id="embed-pdf" style="width:100%;height:800px;" frameborder="0"></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    {{-- modal ni --}}
@endsection
