@extends('layouts.main_template')
@section('judul_konten', 'List Pesan Masuk')
@section('breadcrumb', 'Komplain / Pesan Masuk / List')

@section('konten')
<div class="portlet">
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet-heading portlet-default">
					<h3 class="portlet-title text-dark">
                        <b>Detail Komplain</b>
					</h3>
					<div class="portlet-widgets">
						<a href="javascript:;" data-toggle="reload">
							<i class="ion-refresh"></i>
						</a>
						<span class="divider"></span>
						<a data-toggle="collapse" data-parent="#accordion1" href="#bg-default" class="collapsed" aria-expanded="false">
							<i class="ion-minus-round"></i>
						</a>
						<!-- <span class="divider"></span> -->
						<!-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> -->
					</div>
					<div class="clearfix"></div>
				</div>
				<div id="bg-default" class="panel-collapse collapse aria-expanded=" false "">
					<div class="portlet-body">
                    
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Kode Komplain</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->kode_komplain }}</p> 	
                            </div>
                        </div>  
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Nama</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->nama }}</p> 		
                            </div>
                        </div>

                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Email</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->email }}</p> 	 	
                            </div>
                        </div>  
                                        
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Telpon</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->telpon }}</p> 		
                            </div>
                        </div>  	
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Kategori User</label>
                            <div class="col-md-8">
                                <span class="label label-inverse">{{ $chat->komplain->tag }}</span> 	
                            </div>
                        </div>  	
    
                        @if($chat->komplain->tag == 'unitkerja')
                        <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Unit Kerja</label>
                                <div class="col-md-8">
                                    <p>{{ $chat->komplain->unit_kerja->nama_unit_kerja }}</p> 	
                                </div>
                            </div> 
                        @endif

                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Kategori</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->kategori }}</p> 	
                            </div>
                        </div>  
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Judul</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->judul }}</p> 		
                            </div>
                        </div>  	
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Isi</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->isi}}</p> 		
                            </div>
                        </div>  
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Prioritas</label>
                            <div class="col-md-8">
                                <p>{{ $chat->komplain->prioritas }}</p> 	 	
                            </div>
                        </div>  	
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
                            <div class="col-md-8">
                            @if(!empty($chat->komplain->file_identitas))
                                @php
                                    $ext_fidentitas = pathinfo($chat->komplain->file_identitas, PATHINFO_EXTENSION);
                                @endphp

                                @if($ext_fidentitas == 'pdf') 
                                    <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $chat->komplain->file_identitas }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                @elseif($ext_fidentitas == 'jpg' || $ext_fidentitas == 'jpeg' || $ext_fidentitas == 'png')
                                    <a target="_BLANK" href="{{ asset('storage/komplain/'.$chat->komplain->file_identitas) }}" data-toggle="lightbox" data-title="File Identitas" data-footer="{{ $chat->komplain->file_identitas }}" data-gallery="imagesizes">
                                        <i class="md md-file-download"></i>&nbsp;Lihat file
                                    </a>
                                @else
                                    <a target="_BLANK" href="{{ asset('storage/komplain/'.$chat->komplain->file_identitas) }}">
                                        <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                    </a>
                                @endif
                            @endif
                            </div>
                        </div> 
                        <div class="row form-group">
                            <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">File Tambahan</label>
                            <div class="col-md-8">
                            @if(!empty($chat->komplain->file_tambahan))
                                @php
                                    $ext_ftambahan = pathinfo($chat->komplain->file_tambahan, PATHINFO_EXTENSION);
                                @endphp

                                @if($ext_ftambahan == 'pdf') 
                                    <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $chat->komplain->file_tambahan }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                @elseif($ext_ftambahan == 'jpg' || $ext_ftambahan == 'jpeg' || $ext_ftambahan == 'png')
                                    <a href="{{ asset('storage/komplain/'.$chat->komplain->file_tambahan) }}" data-toggle="lightbox" data-title="File Pendukung" data-footer="{{ $chat->komplain->file_tambahan }}" data-gallery="imagesizes">
                                        <i class="md md-file-download"></i>&nbsp;Lihat file
                                    </a>								
                                @else
                                    <a target="_BLANK" href="{{ asset('storage/komplain/'.$chat->komplain->file_tambahan) }}">
                                        <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                    </a>
                                @endif
                            @endif
                            </div>
                        </div> 		    
                        <span class="clearfix"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><b>Detail Pesan</b></h4><hr> 	
                    
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Pembuat</label>
                        <div class="col-md-8">
                            <p>{{ $chat->userpengirim->name }}</p> 	
                        </div>
                    </div>  
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Pesan</label>
                        <div class="col-md-8">
                            <p>{{ $chat->isi }}</p> 		
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">File</label>
                        <div class="col-md-8">
                            <p>
                                @if(!empty($chat->file))
                                    <a target="_BLANK" href="{{ asset('storage/komplain/'.$chat->file) }}" data-toggle="lightbox" data-title="File Lampiran" data-footer="{{ $chat->file }}">
                                        <i class="md md-file-download"></i>&nbsp;Download File
                                        {{-- <img src="{{ asset('storage/komplain/'.$chat->file) }}" class="img-fluid"> --}}
                                    </a>
                                @endif
                            </p> 		
                            
                        </div>
                    </div>  
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Tanggal</label>
                        <div class="col-md-8">
                            <p>{{ $chat->created_at }}</p> 	 	
                        </div>
                    </div>  	                     			    
                    <span class="clearfix"></span>
                    <div class="row form-group">
                        <div class="col-sm-offset-10 col-sm-9 m-t-15">
                            <a href="{{ route('admin.komplain.pesan.masuk.forward', $chat->id) }}?uid={{ $chat->user_pengirim }}">
                                <button type="button" class="btn-valid btn btn-success waves-effect waves-light"><i class="fa fa-send"></i>&nbsp;Teruskan</button>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @if(Auth::user()->role == 'pg')
    <div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Feedback Komplain</b></h4><hr>
				{!! Form::open(['url' => 'admin/feedback-komplain', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
                    <input type="hidden" name="komplain_id" value="{{ $chat->komplain->id }}">
                    <input type="hidden" name="telpon" value="{{ $chat->komplain->telpon }}">
					<div class="form-group">
			            <label class="col-sm-2 control-label">Status</label>
			            <div class="col-sm-5">
			                <select class="form-control" name="status">
			                    <option value="Proses">Proses</option>
			                    <option value="Ditolak">Ditolak</option>
			                    <option value="Selesai">Selesai</option>
			                </select>
			            </div>
			        </div>  
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="note" placeholder="catatan akan diterima oleh user yang melakukan komplain"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
			                <label class="col-md-2 control-label">Lampiran</label>
			                <div class="col-md-5">
			                    <input type="file" class="filestyle" data-size="sm" name="file">
			                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
			                </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Status Komplain</b></h4><hr>
            {{-- <p class="text-muted font-13">
                Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code>
                to make them scroll horizontally on small devices (under 768px).
            </p> --}}

            <div class="p-20">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Catatan</th>
                                <th>Lampiran</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($respon_komplain as $respon)
                            <tr>
                                <th scope="row">
                                    <?php
                                        if($respon->status == 'Terkirim'){
                                            $label = 'label-success';
                                        } elseif($respon->status == 'Ditolak') {
                                            $label = 'label-danger';
                                        } elseif($respon->status == 'Proses') {
                                            $label = 'label-warning';
                                        } else {
                                            $label = 'label-primary';
                                        }

                                    ?>	                            		
                                    <span class="label {{ $label }}">{{ $respon->status }}</span>
                                </th>
                                <td>{{ $respon->note }}</td>
                                <td>
                                    @if(!empty($respon->file))
                                        <a href="{{ asset('storage/komplain/'.$respon->file) }}">
                                            <i class="md md-file-download">&nbsp;{{ $respon->file }}</i></p>
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $respon->updated_at }}</td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>

    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="m-t-0 header-title"><b>Daftar User</b></h4>
                {{-- <p class="text-muted font-13 m-b-30">
                    Data komplain user. Untuk melihat detail komplain, klik pada kode komplain. --}}
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                    <tbody>
                        @foreach($user_list_masuk  as $u)
                            <tr>       
                                <td>
                                    {{ $u->userpengirim->name }} &nbsp; 
                                    
                                    @php 
                                        $notif = \App\ChatKomplain::where('komplain_id', $chat->komplain->id)
                                                                    ->where('user_tujuan', Auth::id())
                                                                    ->where('user_pengirim', $u->userpengirim->id)
                                                                    ->where('status', 'Belum Baca')->count() 
                                    @endphp
                                    {!! ($notif > 0) ? '<span class="badge badge-xs badge-danger">'.$notif.'</notif>' : '' !!}
                                </td>       
                                <td>
                                    <a href="{{ route('admin.komplain.pesan.masuk.list.detail', [$chat->komplain_id, $u->user_pengirim]) }}">
                                        <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class=" fa fa-commenting"></i>&nbsp;Percakapan</button>
                                    </a>
                                </td>                            
                            </tr>
                        @endforeach

                        @foreach($user_list_keluar  as $u)
                            <tr>       
                                <td>
                                    {{ $u->usertujuan->name }} &nbsp; 
                                    
                                    {{-- @php 
                                        $notif = \App\ChatKomplain::where('komplain_id', $chat->komplain->id)
                                                                    ->where('user_tujuan', $u->usertujuan->id)
                                                                    ->where('user_pengirim', Auth::id())
                                                                    ->where('status', 'Belum Baca')->count() 
                                    @endphp
                                    {!! ($notif > 0) ? '<span class="badge badge-xs badge-danger">'.$notif.'</notif>' : '' !!} --}}
                                </td>       
                                <td>
                                    <a href="{{ route('admin.komplain.pesan.masuk.list.detail', [$chat->komplain_id, $u->user_tujuan]) }}">
                                        <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class=" fa fa-commenting"></i>&nbsp;Percakapan</button>
                                    </a>
                                </td>                            
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- modal ni --}}
    <!-- Bootstrap Modals -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">File</h4>
                    </div>
                    <div class="modal-body">
                            <iframe id="embed-pdf" style="width:100%;height:800px;" frameborder="0"></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    {{-- modal ni --}}
    
@endsection
