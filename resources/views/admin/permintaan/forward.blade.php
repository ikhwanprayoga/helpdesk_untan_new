@extends('layouts.main_template')
@section('judul_konten', 'Forward Permintaan')
@section('breadcrumb', 'Data Permintaan  /  Forward Permintaan')

@section('konten')
    <div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Forward Permintaan</b></h4><hr>
                <form action="{{ route('admin.permintaan.forward.save', $data->id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                    <div class="form-group">
                        <label for="" class="col-md-2 control-label">Tujuan</label>
                        <div class="col-md-6">
                            <select class="form-control select2" name="tujuan">
                                @foreach(\App\User::where('role', 'pg')->get() as $u )
                                    <option value="{{ $u->id }}">{{ $u->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="catatan"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file') ? 'has-error has-feedback' : '' }}">
                        <label class="col-md-2 control-label">Lampiran</label>
                        <div class="col-md-5">
                            <input type="file" class="filestyle" data-size="sm" name="file">
                            <span class="help-block"> {{ $errors->first('file') }} </span>
                        </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>


@endsection