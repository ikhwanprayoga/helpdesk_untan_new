@extends('layouts.main_template')
@section('judul_konten', 'Data Permintaan Data')
@section('breadcrumb', 'Data Permintaan Data')

@section('konten')
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>Data Permintaan</b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Data permintaan data. Untuk melihat detail permintaan, klik pada kode permintaan.
                            </p>
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Kategori</th>
                                    <th>Kebutuhan Informasi</th>
                                    <th>Cara Peroleh</th>
                                    <th>Prioritas</th>
                                    <th>Status</th>
                                    {{-- <th style="min-width:270px">Tag</th> --}}
                                    <th>Tag</th>
                                    <th>Tgl Pengajuan</th>
                                </tr>
                                </thead>    

                                <tbody>
                                    @foreach($data['permintaans'] as $permintaan)
                                        <tr>
                                            <td> <a href="{{ route('admin.permintaan.detail', $permintaan->id) }}">{{ $permintaan->kode_permintaan}}</a> </td>
                                            <td> {{ $permintaan->nama }} </td>
                                            <td> {{ $permintaan->email }} </td>
                                            <td> {{ $permintaan->kategori }} </td>
                                            <td> {{ $permintaan->kebutuhan_informasi }} </td>
                                            <td> {{ $permintaan->cara_peroleh }}</td>
                                            <td> {{ $permintaan->prioritas }} </td>
                                            <td>
                                                <?php
                                                    if($permintaan->status->status == 'Terkirim'){
                                                        $label = 'label-success';
                                                    } elseif($permintaan->status->status == 'Ditolak') {
                                                        $label = 'label-danger';
                                                    } elseif($permintaan->status->status == 'Proses') {
                                                        $label = 'label-warning';
                                                    } else {
                                                        $label = 'label-primary';
                                                    }

                                                ?>

                                                <span class="label label-table {{ $label }}">{{ $permintaan->status->status }} </span>
                                            </td>
                                            <td><span class="label label-inverse">{{ $permintaan->tag }}</span> &nbsp;
                                                @php
                                                    $chat = \App\ChatPermintaan::where('permintaan_id', $permintaan->id)->where('user_pengirim', Auth::id())->orderBy('id', 'ASC')->limit(1)->first()
                                                @endphp
                                                @if(!empty($chat))
                                                    <span class="label label-inverse" style="background-color:#999;">{{ $chat['status'] }}</span> {{ $chat['usertujuan']['name'] }}
                                                @endif
                                            </td>
                                            <td> {{ $permintaan->created_at }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
@endsection
