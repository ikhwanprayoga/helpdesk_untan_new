@extends('layouts.main_template')
@section('judul_konten', 'Data Reservasi Ruangan')
@section('breadcrumb', 'Data Booking')

@section('konten')
				<div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>Data Reservasi Ruangan</b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Data reservasi ruangan. Untuk melihat detail reservasi, klik pada kode reservasi.
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                	<th>Kode</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Instansi</th>
                                    <th>Kegiatan</th>
                                    <th>Tgl Mulai</th>
                                    <th>Tgl Selesai</th>
                                    <th>Status</th>
                                    <th>Tgl Reservasi</th>
                                </tr>
                                </thead>
    

                                <tbody>
                                    @foreach($data['booking'] as $booking)
                                        <tr>
                                            <td> <a href="{{ url('/databooking/detail/'.$booking->id) }}">{{ $booking->kode_booking }}</a> </td>
                                            <td> {{ $booking->nama }} </td>
                                            <td> {{ $booking->email }} </td>
                                            <td> {{ $booking->instansi }} </td>
                                            <td> {{ $booking->kegiatan }} </td>
                                            <td> {{ $booking->dari_tgl }} </td>
                                            <td> {{ $booking->sampai_tgl }} </td>
                                            <td>
                                                <?php
                                                    if($booking->status->status == 'Terkirim'){
                                                        $label = 'label-success';
                                                    } elseif($booking->status->status == 'Ditolak') {
                                                        $label = 'label-danger';
                                                    } elseif($booking->status->status == 'Proses') {
                                                        $label = 'label-warning';
                                                    } else {
                                                        $label = 'label-primary';
                                                    }

                                                ?>

                                                <span class="label label-table {{ $label }}">{{ $booking->status->status }} </span>
                                            </td>
                                            <td> {{ $booking->created_at }} </td>
                                      
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
@endsection
