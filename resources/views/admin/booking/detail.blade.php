@extends('layouts.main_template')
@section('judul_konten', 'Detail Reservasi User')
@section('breadcrumb', 'Detail Data Booking')

@section('konten')

{{-- <div class="row"> --}}
	<div class="row">
		<div class="col-lg-12">
		    <div class="card-box">
		        <h4 class="m-t-0 m-b-20 header-title"><b>Detail Reservasi</b></h4><hr> 	
		        @foreach($detail_booking as $detail)	
			        <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kode Reservasi</label>
	                    <div class="col-md-5">
	                        <p class="text-success tran-text"><strong>{{ $detail->kode_booking }}</strong></p> 	
	                    </div>
	                </div>  	
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Nama</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->nama }}</p> 	
	                    </div>
	                </div> 	                 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Email</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->email }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Telpon</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->telpon }}</p> 	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kegiatan</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->kegiatan }}</p> 	
	                    </div>
	                </div>     	   
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Tanggal Kegiatan</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->dari_tgl }} &nbsp; sampai &nbsp; {{ $detail->sampai_tgl }}</p> 	
	                    </div>
	                </div>                 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Instansi</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->instansi }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
	                    <div class="col-md-5">
	                        <a href="{{ url('/download/booking/'.$detail->file_identitas) }}">
			        			<i class="md md-file-download"></i>&nbsp;{{ $detail->file_identitas }}
			        		</a>	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Surat</label>
	                    <div class="col-md-5">
	                     @if(!empty($detail->file_surat))
	                        <a href="{{ url('/download/booking/'.$detail->file_surat) }}">
			        			<i class="md md-file-download"></i>&nbsp;{{ $detail->file_surat }}</p>
			        		</a>	
			        	 @endif
	                    </div>
	                </div> 
				@endforeach
		        <span class="clearfix"></span>
		    </div>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Feedback Komplain</b></h4><hr>
				{!! Form::open(['url' => 'feedback-booking', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
					<input type="hidden" name="booking_ruang_id" value="{{ $id }}">
					<div class="form-group">
			            <label class="col-sm-2 control-label">Status</label>
			            <div class="col-sm-5">
			                <select class="form-control" name="status">
			                    <option value="Proses">Proses</option>
			                    <option value="Ditolak">Ditolak</option>
			                   	<option value="Selesai">Selesai/Booked</option>
			                </select>
			            </div>
			        </div>  
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="note"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
			                <label class="col-md-2 control-label">Lampiran</label>
			                <div class="col-md-5">
			                    <input type="file" class="filestyle" data-size="sm" name="file">
			                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
			                </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>


    <div class="row">
    	<div class="col-lg-12">
    	<div class="card-box">
	        <h4 class="m-t-0 header-title"><b>Status Komplain</b></h4><hr>
	        {{-- <p class="text-muted font-13">
	            Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code>
	            to make them scroll horizontally on small devices (under 768px).
	        </p> --}}

	        <div class="p-20">
	            <div class="table-responsive">
	                <table class="table m-0">
	                    <thead>
	                        <tr>
	                            <th>Status</th>
	                            <th>Catatan</th>
	                            <th>Lampiran</th>
	                            <th>Tanggal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($respon_booking as $respon)
	                        <tr>
	                            <th scope="row">
	                            	<?php
	                            		if($respon->status == 'Terkirim'){
	                            			$label = 'label-success';
	                            		} elseif($respon->status == 'Ditolak') {
	                            			$label = 'label-danger';
	                            		} elseif($respon->status == 'Proses') {
	                            			$label = 'label-warning';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $respon->status }}</span>
	                            </th>
	                            <td>{{ $respon->note }}</td>
	                            <td>
	                            	@if(!empty($respon->file))
				                        <a href="{{ url('/download/booking/'.$respon->file) }}">
						        			<i class="md md-file-download">&nbsp;{{ $respon->file }}</i></p>
						        		</a>
					        		@endif
					        	</td>
	                            <td>{{ $respon->updated_at }}</td>
	                        </tr>
	                        @endforeach
	                        
	                    </tbody>
	                </table>
	            </div>
	    	</div>
		</div>
    	</div>

	</div>

{{-- </div> --}}
@endsection
