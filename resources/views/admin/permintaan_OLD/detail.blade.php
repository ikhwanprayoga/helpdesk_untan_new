@extends('layouts.main_template')
@section('judul_konten', 'Data Komplain User')
@section('breadcrumb', 'Detail Data Komplain')

@section('konten')


	<div class="row">
		<div class="col-lg-12">
		    <div class="card-box">
		        <h4 class="m-t-0 m-b-20 header-title"><b>Detail Permintaan Data</b></h4><hr> 	
		       	@foreach($detail_permintaans as $detail)
			        <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kode Komplain</label>
	                    <div class="col-md-5">
	                        <p class="text-success tran-text"><strong>{{ $detail->kode_mintadata }}</strong></p> 	
	                    </div>
	                </div>  	
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Nama</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->nama }}</p> 	
	                    </div>
	                </div> 	                 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Email</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->email }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Telpon</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->telpon }}</p> 	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->kategori }}</p> 	
	                    </div>
	                </div>     	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Prioritas</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->prioritas}}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kebutuhan Informasi</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->kebutuhan_informasi }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Alasan Permintaan</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->alasan }}</p> 	
	                    </div>
	                </div> 	
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Cara Peroleh</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text" id="peroleh" data-caraperoleh="{{ $detail->cara_peroleh }}">{{ $detail->cara_peroleh }}</p> 	
	                    </div>
	                </div> 	        

	                {{-- rekaman --}}
	                @if($detail->cara_peroleh == 'rekaman')
	                	<hr>
	                	<div class="row form-group">
		                	<p class="text-muted font-15 col-md-2 col-xs-offset-1">
		                		<strong>Detail Rekaman</strong>
		                	</p>
		                </div>
		                <div class="row form-group">
		                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Waktu</label>
		                    <div class="col-md-5">
		                        <p class="text-default tran-text" id="waktu" data-waktu="{{ $detail->waktu }}">{{ $detail->waktu }}</p> 	
		                    </div>
		                </div> 	
		                <div class="row form-group">
		                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Tempat</label>
		                    <div class="col-md-5">
		                        <p class="text-default tran-text" id="tempat" data-tempat="{{ $detail->tempat }}">{{ $detail->tempat }}</p> 	
		                    </div>
		                </div> 	
		                <div class="row form-group">
		                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Subjek Wawancara</label>
		                    <div class="col-md-5">
		                        <p class="text-default tran-text" id="tujuan_wawancara" data-tujuan_wawancara="{{ $detail->tujuan_wawancara }}">{{ $detail->tujuan_wawancara }}</p> 	
		                    </div>
		                </div> 	
		                <div class="row form-group">
		                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kisi</label>
		                    <div class="col-md-5">
		                        <p class="text-default tran-text" id="kisi" data-kisi="{{ $detail->kisi }}">{{ $detail->kisi}}</p> 	
		                    </div>
		                </div> 
		                <hr>	
		            @endif
                	{{-- ./rekaman --}}

                	<div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
	                    <div class="col-md-5">
	                        <a href="{{ url('/download/mintadata/'.$detail->file_identitas) }}">
			        			<i class="md md-file-download"></i>&nbsp;{{ $detail->file_identitas }}
			        		</a>	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Tambahan</label>
	                    <div class="col-md-5">
	                    @if(!empty($detail->file_tambahan))
	                        <a href="{{ url('/download/mintadata/'.$detail->file_tambahan) }}">
			        			<i class="md md-file-download"></i>&nbsp;{{ $detail->file_tambahan }}</p>
			        		</a>	
			        	@endif
	                    </div>
	                </div>        
				@endforeach
		        <span class="clearfix"></span>
		    </div>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Feedback Permintaan Data</b></h4><hr>
				{!! Form::open(['url' => 'feedback-permintaan', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
					<input type="hidden" name="minta_data_id" value="{{ $id }}">

					<div class="rekaman" style="display: none;">
						<div class="form-group">
                            <label class="col-md-2 control-label">Tempat</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="data_tempat" name="tempat">                                       
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-2 control-label">Waktu</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="data_waktu" name="waktu">                                       
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-md-2 control-label">Subjek Wawancara</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" id="data_tujuan_wawancara" name="tujuan_wawancara">                                       
                            </div>
                        </div> 
						<div class="form-group">
				            <label class="col-md-2 control-label">Kisi</label>
				            <div class="col-md-10">
				                <textarea class="form-control" rows="5" name="kisi" id="data_kisi"></textarea>
				            </div>
				        </div>
					</div>
					<hr>
					<div class="form-group">
			            <label class="col-sm-2 control-label">Status</label>
			            <div class="col-sm-5">
			                <select class="form-control" name="status">
			                    <option value="Proses">Proses</option>
			                    <option value="Ditolak">Ditolak</option>
			                   	<option value="Selesai">Selesai</option>
			                </select>
			            </div>
			        </div>  
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="note"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file') ? 'has-error has-feedback' : '' }}">
			                <label class="col-md-2 control-label">Lampiran</label>
			                <div class="col-md-5">
			                    <input type="file" class="filestyle" data-size="sm" name="file">
			                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
			                </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>


    <div class="row">
    	<div class="col-lg-12">
    	<div class="card-box">
	        <h4 class="m-t-0 header-title"><b>Status Permintaan Data</b></h4><hr>
	        {{-- <p class="text-muted font-13">
	            Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code>
	            to make them scroll horizontally on small devices (under 768px).
	        </p> --}}

	        <div class="p-20">
	            <div class="table-responsive">
	                <table class="table m-0">
	                    <thead>
	                        <tr>
	                            <th>Status</th>
	                            <th>Catatan</th>
	                            <th>Lampiran</th>
	                            <th>Tanggal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($respon_mintadata as $respon)
	                        <tr>
								<th scope="row">
	                            	<?php
	                            		if($respon->status == 'Terkirim'){
	                            			$label = 'label-success';
	                            		} elseif($respon->status == 'Ditolak') {
	                            			$label = 'label-danger';
	                            		} elseif($respon->status == 'Proses') {
	                            			$label = 'label-warning';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $respon->status }}</span>
	                            </th>

	                            <td>{{ $respon->note }}</td>
	                            <td>
	                            	@if(!empty($respon->file))
				                        <a href="{{ url('/download/mintadata/'.$respon->file) }}">
						        			<i class="md md-file-download">&nbsp;{{ $respon->file }}</i></p>
						        		</a>
					        		@endif
					        	</td>
	                            <td>{{ $respon->updated_at }}</td>
	                        </tr>
	                        @endforeach
	                        
	                    </tbody>
	                </table>
	            </div>
	    	</div>
		</div>
    	</div>

	</div>

{{-- </div> --}}
@endsection
