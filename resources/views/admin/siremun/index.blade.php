@extends('layouts.main_template')
@section('judul_konten', 'Data Komplain Siremun User')
@section('breadcrumb', 'Data Komplain Siremun User')

@section('konten')
				<div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">

                            <h4 class="m-t-0 header-title"><b>Data Komplain Siremun User</b></h4>
                            <p class="text-muted font-13 m-b-30">
                                Data komplain user. Untuk melihat detail komplain, klik pada kode komplain.
                            <table id="datatable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                	<th>Kode</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Kategori</th>
                                    <th>Judul</th>
                                    <th>Status</th>
                                    <th style="min-width:270px">Tag</th>
                                    <th>Tgl Komplain</th>
                                    {{-- <th>Action</th> --}}
                                </tr>
                                </thead>
    

                                <tbody>
                                    @foreach($data['komplains'] as $komplain)
                                        <tr>
                                            <td> <a href="{{ route('admin.siremun.komplain.detail', $komplain->id) }}">{{ $komplain->kode_komplain }}</a> </td>
                                            <td> {{ $komplain->nama }} </td>
                                            <td> {{ $komplain->email }} </td>
                                            <td> {{ $komplain->kategori }} </td>
                                            <td> {{ $komplain->judul }} </td>
                                            <td>
                                                <?php
                                                    if($komplain->status->status == 'Terkirim'){
                                                        $label = 'label-success';
                                                    } elseif($komplain->status->status == 'Ditolak') {
                                                        $label = 'label-danger';
                                                    } elseif($komplain->status->status == 'Proses') {
                                                        $label = 'label-warning';
                                                    } else {
                                                        $label = 'label-primary';
                                                    }

                                                ?>

                                                <span class="label label-table {{ $label }}">{{ $komplain->status->status }} </span>
                                            </td>
                                            <td><span class="label label-inverse">{{ $komplain->tag }}</span> &nbsp;
                                                @php
                                                    $chat = \App\ChatKomplain::where('komplain_id', $komplain->id)->where('user_pengirim', Auth::id())->orderBy('id', 'ASC')->limit(1)->first()
                                                @endphp
                                                @if(!empty($chat))
                                                    <span class="label label-inverse" style="background-color:#999;">{{ $chat['status'] }}</span> {{ $chat['usertujuan']['name'] }}
                                                @endif
                                            </td>
                                            <td> {{ $komplain->created_at }} </td>
                                            {{-- <td>
                                                <a href="{{ route('admin.siremun.komplain.forward', $komplain->id) }}">
                                                    <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class="fa fa-send"></i>&nbsp;Teruskan</button>
                                                </a>
                                            </td> --}}
                                
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
@endsection
