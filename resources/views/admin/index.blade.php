@extends('layouts.main_template')
@section('judul_konten', 'Selamat Datang di halaman Administrator Website Helpdesk Universitas Tanjungpura')
@section('breadcrumb', 'Selamat Datang')

@section('konten')
	{{-- <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="card-box widget-box-1 bg-white">
                <i class="fa fa-info-circle text-muted pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                <h4 class="text-dark">Total Komplain</h4>
                <h2 class="text-primary text-center"><span data-plugin="counterup">5623</span></h2>
                <p class="text-muted">Januari 2016 - Sekarang <span class="pull-right"></span></p>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="card-box widget-box-1 bg-white">
                <i class="fa fa-info-circle text-muted pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                <h4 class="text-dark">Total Permintaan Data</h4>
                <h2 class="text-pink text-center"><span data-plugin="counterup">185</span></h2>
                <p class="text-muted">Januari 2016 - Sekarang <span class="pull-right"></span></p>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="card-box widget-box-1 bg-white">
                <i class="fa fa-info-circle text-muted pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                <h4 class="text-dark">Jumlah Komplain Selesai</h4>
                <h2 class="text-success text-center"><span data-plugin="counterup">9562</span></h2>
                <p class="text-muted">Januari 2016 - Sekarang <span class="pull-right"></span></p>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-lg-3">
            <div class="card-box widget-box-1 bg-white">
                <i class="fa fa-info-circle text-muted pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Last 24 Hours"></i>
                <h4 class="text-dark">Jumlah Data Selesai</h4>
                <h2 class="text-warning text-center"><span data-plugin="counterup">874</span></h2>
                <p class="text-muted" class="text-center">Januari 2016 - Sekarang <span class="pull-right"></span></p>
            </div>
        </div>
    </div> --}}

    <!-- Row start -->
    <div class="row">
        <div class="col-md-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Info</b></h4>
            <p class="text-muted m-b-15 font-13">
                Selamat datang dalam layanan Helpdesk Universitas Tanjungpura. Anda dapat menyampaikan keluhan, pengaduan, atau informasi lain yang berhubungan dengan kegiatan dan layanan di Universitas Tanjungpura. 
                Semua komplain atau permintaan yang masuk akan kami layani pada hari dan jam kerja. <code> Senin - Jum'at : 08:00 - 16:00 WIB </code>
                .
            </p>

            <address>
                <strong>Alamat</strong> <br>
                Jl. Prof. Dr. H. Hadari Nawawi, Bansir Laut, <br>
                Pontianak Tenggara, Kota Pontianak, <br>
                Kalimantan Barat 78124 <br>
                <abbr title="Phone">Telepon:</abbr> (0561) 739630
            </address>

            <address>
                <strong>Email</strong>
                <br>
                <a href="mailto:#">helpdesk@untan.ac.id</a>
            </address>
        </div>
        </div>
    </div>

@stop
