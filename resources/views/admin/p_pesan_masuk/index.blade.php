@extends('layouts.main_template')
@section('judul_konten', 'Permintaan Masuk')
@section('breadcrumb', 'Permintaan / Permintaan Masuk')

@section('konten')
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="m-t-0 header-title"><b>Data Permintaan User</b></h4>
                {{-- <p class="text-muted font-13 m-b-30">
                    Data permintaan user. Untuk melihat detail permintaan, klik pada kode permintaan. --}}
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>No</th>
                        <th>Kode</th>
                        <th>Pengirim</th>
                        <th>Kebutuhan Informasi</th>
                        <th>Cara Peroleh</th>
                        <th>Status Permintaan</th>
                        <th>Status Pesan</th>
                        <th>Tanggal</th>
                        <th style="min-width:65px;">Action</th>
                    </tr>
                    </thead>


                    <tbody>
                        @foreach($chat as $c)
                            <tr>       
                                <td>{{ $loop->iteration }}</td>                         
                                <td>{{ $c->permintaan->kode_permintaan }}</td>
                                <td>{{ $c->userpengirim->name }}</td>
                                <td>{{ $c->permintaan->kebutuhan_informasi }}</td>
                                <td>{{ $c->permintaan->cara_peroleh }}</td>
                                <td>
                                    <?php
	                            		if($c->permintaan->status == 'Terkirim'){
	                            			$label = 'label-success';
	                            		} elseif($c->permintaan->status == 'Ditolak') {
	                            			$label = 'label-danger';
	                            		} elseif($c->permintaan->status == 'Proses') {
	                            			$label = 'label-warning';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $c->permintaan->status }}</span>
                                </td>
                                <td>
                                    <?php
	                            		if($c->status == 'Baca'){
	                            			$label = 'label-success';
	                            		} elseif($c->status == 'Belum Baca') {
	                            			$label = 'label-warning';
	                            		} elseif($c->status == 'Diteruskan') {
	                            			$label = 'label-primary';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $c->status }}</span>
                                </td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($c->created_at)) }} </td>
                                <td>
                                    <a href="{{ route('admin.permintaan.pesan.masuk.list', $c->id) }}">
                                        <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class="fa fa-list"></i>&nbsp;Detail</button>
                                    </a>
                                    @php
                                        $jlh_chat_belum_baca = \App\ChatPermintaan::where('user_tujuan', Auth::id())
                                                                                    ->where('status', 'Belum Baca')
                                                                                    ->where('permintaan_id', $c->permintaan_id)
                                                                                    ->count() 
                                    @endphp
                                    
                                    @if($jlh_chat_belum_baca > 0)
                                        <span class="badge badge-sm badge-danger">
                                            {{ $jlh_chat_belum_baca }}
                                        </span>
                                    @endif
                                </td>
                            
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
