@extends('layouts.main_template')
@section('judul_konten', 'List Permintaan Masuk')
@section('breadcrumb', 'Permintaan / Permintaan Masuk / List')

@section('konten')
<div class="portlet">
		<div class="row">
			<div class="col-lg-12">
				<div class="portlet-heading portlet-default">
					<h3 class="portlet-title text-dark">
                        <b>Detail Permintaan</b>
					</h3>
					<div class="portlet-widgets">
						<a href="javascript:;" data-toggle="reload">
							<i class="ion-refresh"></i>
						</a>
						<span class="divider"></span>
						<a data-toggle="collapse" data-parent="#accordion1" href="#bg-default" class="" aria-expanded="true">
							<i class="ion-minus-round"></i>
						</a>
						<!-- <span class="divider"></span> -->
						<!-- <a href="#" data-toggle="remove"><i class="ion-close-round"></i></a> -->
					</div>
					<div class="clearfix"></div>
				</div>
				<div id="bg-default" class="panel-collapse aria-expanded=collapse in" true "">
					<div class="portlet-body">
                    
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kode Permintaan</label>
                            <div class="col-md-5">
                                <p class="text-success tran-text"><strong>{{ $chat->permintaan->kode_permintaan }}</strong></p> 	
                            </div>
                        </div>  	
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Nama</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->nama }}</p> 	
                            </div>
                        </div> 	                 
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Email</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->email }}</p> 	
                            </div>
                        </div> 	                
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Telpon</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->telpon }}</p> 	
                            </div>
                        </div> 
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori User</label>
                            <div class="col-md-8">
                                <span class="label label-inverse">{{ $chat->permintaan->tag }}</span> 	
                            </div>
                        </div>  	
    
                        @if($chat->permintaan->tag == 'unitkerja')
                        <div class="row form-group">
                                <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Unit Kerja</label>
                                <div class="col-md-8">
                                    <p>{{ $chat->permintaan->unit_kerja->nama_unit_kerja }}</p> 	
                                </div>
                            </div> 
                        @endif

                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->kategori }}</p> 	
                            </div>
                        </div>     	                
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Prioritas</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->prioritas}}</p> 	
                            </div>
                        </div> 	                
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kebutuhan Informasi</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->kebutuhan_informasi }}</p> 	
                            </div>
                        </div> 	                
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Alasan Permintaan</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text">{{ $chat->permintaan->alasan }}</p> 	
                            </div>
                        </div> 	
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Cara Peroleh</label>
                            <div class="col-md-5">
                                <p class="text-default tran-text" id="peroleh" data-caraperoleh="{{ $chat->permintaan->cara_peroleh }}">{{ $chat->permintaan->cara_peroleh }}</p> 	
                            </div>
                        </div> 	        

                        {{-- rekaman --}}
                        @if($chat->permintaan->cara_peroleh == 'rekaman')
                            <hr>
                            <div class="row form-group">
                                <p class="text-muted font-15 col-md-2 col-xs-offset-1">
                                    <strong>Detail Rekaman</strong>
                                </p>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Waktu</label>
                                <div class="col-md-5">
                                    <p class="text-default tran-text" id="waktu" data-waktu="{{ $chat->permintaan->rekaman->waktu }}">{{ $chat->permintaan->rekaman->waktu }}</p> 	
                                </div>
                            </div> 	
                            <div class="row form-group">
                                <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Tempat</label>
                                <div class="col-md-5">
                                    <p class="text-default tran-text" id="tempat" data-tempat="{{ $chat->permintaan->rekaman->tempat }}">{{ $chat->permintaan->rekaman->tempat }}</p> 	
                                </div>
                            </div> 	
                            <div class="row form-group">
                                <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Subjek Wawancara</label>
                                <div class="col-md-5">
                                    <p class="text-default tran-text" id="tujuan_wawancara" data-tujuan_wawancara="{{ $chat->permintaan->rekaman->tujuan_wawancara }}">{{ $chat->permintaan->rekaman->tujuan_wawancara }}</p> 	
                                </div>
                            </div> 	
                            <div class="row form-group">
                                <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kisi</label>
                                <div class="col-md-5">
                                    <p class="text-default tran-text" id="kisi" data-kisi="{{ $chat->permintaan->rekaman->kisi }}">{{ $chat->permintaan->rekaman->kisi}}</p> 	
                                </div>
                            </div> 
                            <hr>	
                        @endif
                        {{-- ./rekaman --}}

                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
                            <div class="col-md-5">
                            @if(!empty($chat->permintaan->file_identitas))
                                @php    
                                    $ext_fidentitas = pathinfo($chat->permintaan->file_identitas, PATHINFO_EXTENSION);
                                @endphp

                                @if($ext_fidentitas == 'pdf') 
                                    <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="permintaan" data-file="{{ $chat->permintaan->file_identitas }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                @elseif($ext_fidentitas == 'jpg' || $ext_fidentitas == 'jpeg' || $ext_fidentitas == 'png')
                                    <a target="_BLANK" href="{{ asset('storage/permintaan/'.$chat->permintaan->file_identitas) }}" data-toggle="lightbox" data-title="File Identitas" data-footer="{{ $chat->permintaan->file_identitas }}" data-gallery="imagesizes">
                                        <i class="md md-file-download"></i>&nbsp;Lihat file
                                    </a>
                                @else
                                    <a target="_BLANK" href="{{ asset('storage/permintaan/'.$chat->permintaan->file_identitas) }}">
                                        <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                    </a>
                                @endif	
                            @endif	
                            </div>
                        </div> 
                        <div class="row form-group">
                            <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Tambahan</label>
                            <div class="col-md-5">
                            @if(!empty($chat->permintaan->file_tambahan))
                                @php
                                    $ext_ftambahan = pathinfo($chat->permintaan->file_tambahan, PATHINFO_EXTENSION);
                                @endphp

                                @if($ext_ftambahan == 'pdf') 
                                    <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="permintaan" data-file="{{ $chat->permintaan->file_tambahan }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                @elseif($ext_ftambahan == 'jpg' || $ext_ftambahan == 'jpeg' || $ext_ftambahan == 'png')
                                    <a href="{{ asset('storage/permintaan/'.$chat->permintaan->file_tambahan) }}" data-toggle="lightbox" data-title="File Pendukung" data-footer="{{ $chat->permintaan->file_tambahan }}" data-gallery="imagesizes">
                                        <i class="md md-file-download"></i>&nbsp;Lihat file
                                    </a>								
                                @else
                                    <a target="_BLANK" href="{{ asset('storage/permintaan/'.$chat->permintaan->file_tambahan) }}">
                                        <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                    </a>
                                @endif
                            @endif
                            </div>
                        </div>    

                        <span class="clearfix"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><b>Detail Pesan</b></h4><hr> 	
                    
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Pembuat</label>
                        <div class="col-md-8">
                            <p>{{ $chat->userpengirim->name }}</p> 	
                        </div>
                    </div>  
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Pesan</label>
                        <div class="col-md-8">
                            <p>{{ $chat->isi }}</p> 		
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">File</label>
                        <div class="col-md-8">
                            <p>
                                @if(!empty($chat->file))
                                    <a target="_BLANK" href="{{ asset('storage/permintaan/'.$chat->file) }}" data-toggle="lightbox" data-title="File Lampiran" data-footer="{{ $chat->file }}">
                                        <i class="md md-file-download"></i>&nbsp;Download File
                                        {{-- <img src="{{ asset('storage/permintaan/'.$chat->file) }}" class="img-fluid"> --}}
                                    </a>
                                @endif
                            </p> 		
                            
                        </div>
                    </div>  
                    <div class="row form-group">
                        <label class="col-md-3 col-xs-offset-1 control-label" for="example-email">Tanggal</label>
                        <div class="col-md-8">
                            <p>{{ $chat->created_at }}</p> 	 	
                        </div>
                    </div>  	                     			    
                    <span class="clearfix"></span>
                    <div class="row form-group">
                        <div class="col-sm-offset-10 col-sm-9 m-t-15">
                            <a href="{{ route('admin.permintaan.pesan.masuk.forward', $chat->id) }}?uid={{ $chat->user_pengirim }}">
                                <button type="button" class="btn-valid btn btn-success waves-effect waves-light"><i class="fa fa-send"></i>&nbsp;Teruskan</button>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    @if(Auth::user()->role == 'pg')
    <div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Feedback Permintaan</b></h4><hr>
				{!! Form::open(['url' => 'admin/feedback-permintaan', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
                    <input type="hidden" name="permintaan_id" value="{{ $chat->permintaan->id }}">
                    <input type="hidden" name="telpon" value="{{ $chat->permintaan->telpon }}">
					<div class="form-group">
			            <label class="col-sm-2 control-label">Status</label>
			            <div class="col-sm-5">
			                <select class="form-control" name="status">
			                    <option value="Proses">Proses</option>
			                    <option value="Ditolak">Ditolak</option>
			                    <option value="Selesai">Selesai</option>
			                </select>
			            </div>
			        </div>  
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="note" placeholder="catatan akan diterima oleh user yang melakukan permintaan"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
			                <label class="col-md-2 control-label">Lampiran</label>
			                <div class="col-md-5">
			                    <input type="file" class="filestyle" data-size="sm" name="file">
			                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
			                </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
    </div>

    <div class="row">
        <div class="col-lg-12">
        <div class="card-box">
            <h4 class="m-t-0 header-title"><b>Status Permintaan</b></h4><hr>
            {{-- <p class="text-muted font-13">
                Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code>
                to make them scroll horizontally on small devices (under 768px).
            </p> --}}

            <div class="p-20">
                <div class="table-responsive">
                    <table class="table m-0">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Catatan</th>
                                <th>Lampiran</th>
                                <th>Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($respon_permintaan as $respon)
                            <tr>
                                <th scope="row">
                                    <?php
                                        if($respon->status == 'Terkirim'){
                                            $label = 'label-success';
                                        } elseif($respon->status == 'Ditolak') {
                                            $label = 'label-danger';
                                        } elseif($respon->status == 'Proses') {
                                            $label = 'label-warning';
                                        } else {
                                            $label = 'label-primary';
                                        }

                                    ?>	                            		
                                    <span class="label {{ $label }}">{{ $respon->status }}</span>
                                </th>
                                <td>{{ $respon->note }}</td>
                                <td>
                                    @if(!empty($respon->file))
                                        <a href="{{ asset('storage/permintaan/'.$respon->file) }}">
                                            <i class="md md-file-download">&nbsp;{{ $respon->file }}</i></p>
                                        </a>
                                    @endif
                                </td>
                                <td>{{ $respon->updated_at }}</td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>

    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="m-t-0 header-title"><b>Daftar User</b></h4>
                {{-- <p class="text-muted font-13 m-b-30">
                    Data permintaan user. Untuk melihat detail permintaan, klik pada kode permintaan. --}}
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                    <tbody>
                        @foreach($user_list_masuk  as $u)
                            <tr>       
                                <td>
                                    {{ $u->userpengirim->name }} &nbsp; 
                                    
                                    @php 
                                        $notif = \App\ChatPermintaan::where('permintaan_id', $chat->permintaan->id)
                                                                    ->where('user_tujuan', Auth::id())
                                                                    ->where('user_pengirim', $u->userpengirim->id)
                                                                    ->where('status', 'Belum Baca')->count() 
                                    @endphp
                                    {!! ($notif > 0) ? '<span class="badge badge-xs badge-danger">'.$notif.'</notif>' : '' !!}
                                </td>       
                                <td>
                                    <a href="{{ route('admin.permintaan.pesan.masuk.list.detail', [$chat->permintaan_id, $u->user_pengirim]) }}">
                                        <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class=" fa fa-commenting"></i>&nbsp;Percakapan</button>
                                    </a>
                                </td>                            
                            </tr>
                        @endforeach

                        @foreach($user_list_keluar  as $u)
                            <tr>       
                                <td>
                                    {{ $u->usertujuan->name }} &nbsp; 
                                    
                                    {{-- @php 
                                        $notif = \App\ChatPermintaan::where('permintaan_id', $chat->permintaan->id)
                                                                    ->where('user_tujuan', $u->usertujuan->id)
                                                                    ->where('user_pengirim', Auth::id())
                                                                    ->where('status', 'Belum Baca')->count() 
                                    @endphp
                                    {!! ($notif > 0) ? '<span class="badge badge-xs badge-danger">'.$notif.'</notif>' : '' !!} --}}
                                </td>       
                                <td>
                                    <a href="{{ route('admin.permintaan.pesan.masuk.list.detail', [$chat->permintaan_id, $u->user_tujuan]) }}">
                                        <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class=" fa fa-commenting"></i>&nbsp;Percakapan</button>
                                    </a>
                                </td>                            
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- modal ni --}}
    <!-- Bootstrap Modals -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">File</h4>
                    </div>
                    <div class="modal-body">
                            <iframe id="embed-pdf" style="width:100%;height:800px;" frameborder="0"></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    {{-- modal ni --}}
    
@endsection
