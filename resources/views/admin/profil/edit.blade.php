@extends('layouts.main_template')
@section('judul_konten', 'Edit Data Profil')
@section('breadcrumb', 'Edit Data Profil')

@section('konten')
    
     
    <br>

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

	

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Edit Data Profil</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Input data baru untuk mengubah data anda.
                </p>

                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('admin.profil.save') }}" method="POST" role="form" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-3 control-label">Username</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" name="username" value="{{ $data->username }}">                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama</label>
                                <div class="col-md-7">
                                    <input type="text" class="form-control" name="name" value="{{ $data->name }}">                                    
                                </div>
                            </div>      
                            <div class="form-group">
                                <label class="col-md-3 control-label">No Hp</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="+62" disabled="">
                                </div>
                                <div class="col-md-5">
                                    <input type="number" class="form-control" name="nohp" id="no_hp" value="{{ substr($data->nohp,2) }}">                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Email</label>
                                <div class="col-md-7">
                                    <input type="email" class="form-control" name="email" value="{{ $data->email }}">                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Password</label>
                                <div class="col-md-3">
                                    <input type="password" class="form-control" name="password" id="inp_password"> 
                                    <br><p class="text-danger">* Password akan berubah jika anda mengisi input password.</p>
                                </div>
                                <div class="col-md-4">
                                    <span id="show_password"></span>
                                </div>
                            </div>                           
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script>
    $(document).ready(function() {
        
        $('#inp_password').keyup(function(){
            $('#show_password').text( 'Password anda adalah : |  ' + $(this).val() + '  |' );
        });
        $('#no_hp').keypress(function(){
            a = $(this).val( parseInt( $(this).val(), 10 ) );
            // console.log( a );
        });
    });
</script>
