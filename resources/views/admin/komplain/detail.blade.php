@extends('layouts.main_template')
@section('judul_konten', 'Data Komplain User')
@section('breadcrumb', 'Detail Data Komplain')

@section('konten')

{{-- <div class="row"> --}}
	<div class="row">
		<div class="col-lg-12">
		    <div class="card-box">
		        <h4 class="m-t-0 m-b-20 header-title"><b>Detail Komplain</b></h4><hr> 	
		        	<div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kode Komplain</label>
	                    <div class="col-md-5">
	                        <p class="text-success tran-text"><strong>{{ $data->kode_komplain }}</strong></p> 	
	                    </div>
	                </div>  	
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Nama</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->nama }}</p> 	
	                    </div>
	                </div> 	                 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Email</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->email }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Telpon</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->telpon }}</p> 	
	                    </div>
					</div> 
					<div class="row form-group">
						<label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori User</label>
						<div class="col-md-5">
							<span class="label label-inverse">{{ $data->tag }}</span> 	
						</div>
					</div>  	

					@if($data->tag == 'unitkerja')
					<div class="row form-group">
							<label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Unit Kerja</label>
							<div class="col-md-5">
								<p class="text-default tran-text">{{ $data->unit_kerja->nama_unit_kerja }}</p> 	
							</div>
						</div> 
					@endif

	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->kategori }}</p> 	
	                    </div>
	                </div>     	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Prioritas</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->prioritas}}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Judul Komplain</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->judul }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Isi Komplain</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->isi }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
	                    <div class="col-md-5">
							@if(!empty($data->file_identitas))
	                       		@php
                                    $ext_fidentitas = pathinfo($data->file_identitas, PATHINFO_EXTENSION);
                                @endphp

                                @if($ext_fidentitas == 'pdf') 
                                    <button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $data->file_identitas }}" data-target=".bs-example-modal-lg">Lihat file</button>
                                @elseif($ext_fidentitas == 'jpg' || $ext_fidentitas == 'jpeg' || $ext_fidentitas == 'png')
                                    <a target="_BLANK" href="{{ asset('storage/komplain/'.$data->file_identitas) }}" data-toggle="lightbox" data-title="File Identitas" data-footer="{{ $data->file_identitas }}" data-gallery="imagesizes">
                                        <i class="md md-file-download"></i>&nbsp;Lihat file
                                    </a>
                                @else
                                    <a target="_BLANK" href="{{ asset('storage/komplain/'.$data->file_identitas) }}">
                                        <button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
                                    </a>
								@endif
							@endif
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Tambahan</label>
	                    <div class="col-md-5">
	                    @if(!empty($data->file_tambahan))
							@php
								$ext_ftambahan = pathinfo($data->file_tambahan, PATHINFO_EXTENSION);
							@endphp

							@if($ext_ftambahan == 'pdf') 
								<button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $data->file_tambahan }}" data-target=".bs-example-modal-lg">Lihat file</button>
							@elseif($ext_ftambahan == 'jpg' || $ext_ftambahan == 'jpeg' || $ext_ftambahan == 'png')
								<a href="{{ asset('storage/komplain/'.$data->file_tambahan) }}" data-toggle="lightbox" data-title="File Pendukung" data-footer="{{ $data->file_tambahan }}" data-gallery="imagesizes">
									<i class="md md-file-download"></i>&nbsp;Lihat file
								</a>								
							@else
								<a target="_BLANK" href="{{ asset('storage/komplain/'.$data->file_tambahan) }}">
									<button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
								</a>
							@endif
			        	@endif
	                    </div>
	                </div> 
		        <span class="clearfix"></span>
		    </div>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Feedback Komplain</b></h4><hr>
				{!! Form::open(['url' => 'admin/feedback-komplain', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
					<input type="hidden" name="komplain_id" value="{{ $id }}">
                    <input type="hidden" name="telpon" value="{{ $data->telpon }}">
					<div class="form-group">
			            <label class="col-sm-2 control-label">Status</label>
			            <div class="col-sm-5">
			                <select class="form-control" name="status">
			                    <option value="Proses">Proses</option>
			                    <option value="Ditolak">Ditolak</option>
			                	<option value="Selesai">Selesai</option>
			                </select>
			            </div>
			        </div>  
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="note" placeholder="catatan akan diterima oleh user yang melakukan komplain"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
			                <label class="col-md-2 control-label">Lampiran</label>
			                <div class="col-md-5">
			                    <input type="file" class="filestyle" data-size="sm" name="file">
			                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
			                </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="card-box">
				<h4 class="m-t-0 header-title"><b>Forward Komplain</b></h4><hr>
				<form action="{{ route('admin.komplain.forward.save', $id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{ csrf_field() }} 
					<div class="form-group">
						<label for="" class="col-md-2 control-label">Tujuan</label>
						<div class="col-md-6">
							<select class="form-control select2" name="tujuan">
								@foreach(\App\User::where('role', 'pg')->get() as $u )
									<option value="{{ $u->id }}">{{ $u->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 control-label">Pesan</label>
						<div class="col-md-10">
							<textarea class="form-control" rows="5" name="pesan" placeholder="Pesan akan diterima dan dibaca oleh user tujuan"></textarea>
						</div>
					</div>
					<div class="form-group {{ $errors->has('file') ? 'has-error has-feedback' : '' }}">
						<label class="col-md-2 control-label">Lampiran</label>
						<div class="col-md-5">
							<input type="file" class="filestyle" data-size="sm" name="file">
							<span class="help-block"> {{ $errors->first('file') }} </span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-9 m-t-15">
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
							<button type="reset" class="btn btn-default m-l-5">
								Reset
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">

                <h4 class="m-t-0 header-title"><b>Daftar Percakapan User</b></h4>
                {{-- <p class="text-muted font-13 m-b-30">
                    Data komplain user. Untuk melihat detail komplain, klik pada kode komplain. --}}
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                    <tbody>
                    	@foreach($user_list_keluar  as $u)
                            <tr>       
                                <td>
                                    {{ $u->usertujuan->name }} &nbsp; 
                                </td>       
                                <td>
                                    <a href="{{ route('admin.komplain.pesan.masuk.list.detail', [$data->id, $u->user_tujuan]) }}">
                                        <button type="button" class="btn btn-xs btn-default waves-effect waves-light"><i class=" fa fa-commenting"></i>&nbsp;Percakapan</button>
                                    </a>
                                </td>                            
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>



    <div class="row">
    	<div class="col-lg-12">
    	<div class="card-box">
	        <h4 class="m-t-0 header-title"><b>Status Komplain</b></h4><hr>
	        {{-- <p class="text-muted font-13">
	            Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code>
	            to make them scroll horizontally on small devices (under 768px).
	        </p> --}}

	        <div class="p-20">
	            <div class="table-responsive">
	                <table class="table m-0">
	                    <thead>
	                        <tr>
	                            <th>Status</th>
	                            <th>Catatan</th>
	                            <th>Lampiran</th>
	                            <th>Tanggal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($data->respon_komplain as $respon)
	                        <tr>
	                            <th scope="row">
	                            	<?php
	                            		if($respon->status == 'Terkirim'){
	                            			$label = 'label-success';
	                            		} elseif($respon->status == 'Ditolak') {
	                            			$label = 'label-danger';
	                            		} elseif($respon->status == 'Proses') {
	                            			$label = 'label-warning';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $respon->status }}</span>
	                            </th>
	                            <td>{{ $respon->note }}</td>
	                            <td>
	                            	@if(!empty($respon->file))
				                        <a href="{{ asset('storage/komplain/'.$respon->file) }}">
						        			<i class="md md-file-download">&nbsp;{{ $respon->file }}</i></p>
						        		</a>
					        		@endif
					        	</td>
	                            <td>{{ ( date('d-m-Y H:i:s', strtotime($respon->created_at)) ) }}</td>
	                        </tr>
	                        @endforeach
	                        
	                    </tbody>
	                </table>
	            </div>
	    	</div>
		</div>
    	</div>

	</div>

	{{-- respon user --}}
	<div class="row">
		<div class="col-lg-12">
		<div class="card-box">
			<h4 class="m-t-0 header-title"><b>RESPON USER</b></h4><hr>
			<div class="p-20">
				<div class="table-responsive">
					<table class="table m-0">
						<thead>
							<tr>
								<th>No</th>
								<th>Catatan</th>
								<th>Lampiran</th>
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data->respon_komplain_user as $res)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $res->catatan }}</td>
								<td>
									@if(!empty($res->file))
										<a target="_BLANK" href="{{ asset('storage/komplain/'.$res->file) }}">
											<i class="md md-file-download">&nbsp;Download File</i></p>
										</a>
									@endif
								</td> 
								<td>{{ ( date('d-m-Y H:i:s', strtotime($respon->created_at)) ) }}</td>
							</tr>	    
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
	</div>
	{{-- respon user --}}

	{{-- modal ni --}}
    <!-- Bootstrap Modals -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="myLargeModalLabel">File</h4>
                    </div>
                    <div class="modal-body">
                            <iframe id="embed-pdf" style="width:100%;height:800px;" frameborder="0"></iframe>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    {{-- modal ni --}}

{{-- </div> --}}
@endsection
