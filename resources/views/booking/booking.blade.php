@extends('layouts.main_template')
@section('judul_konten', 'Form Booking')
@section('breadcrumb', 'Booking Ruangan / Form Booking')

@section('konten')

    <div class="row">
        <div class="col-md-12">
        @component('layouts.com_alert')
            @slot('status')
                success
            @endslot

            @slot('judul')
               <i class="md md-check"></i> Ruangan Tersedia
            @endslot
                Ruangan dari tanggal <b> {{ $dari_tgl }} </b> sampai <b> {{ $sampai_tgl }} </b> tersedia, silahkan lengkapi data untuk melakukan reservasi.
        @endcomponent
        </div>
    </div>
    <br>        
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Input Data Reservasi</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Lengkapi data berikut sesuai dengan data reservasi anda :
                </p>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'bookingruang', 'class' => 'form-horizontal', 'role' => 'form' , 'enctype' => 'multipart/form-data' ]) !!}
                            <input type="hidden" name="id_ruangan" value="{{ $id_ruangan }}">
                            <input type="hidden" name="dari_tgl" value="{{ $dari_tgl }}">
                            <input type="hidden" name="sampai_tgl" value="{{ $sampai_tgl }}">
                            <div class="form-group {{ $errors->has('nama') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nama Lengkap</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="nama">                                    
                                    {!! $errors->has('nama') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nama') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label" for="example-email">Email</label>
                                <div class="col-md-10">
                                    <input type="email" id="example-email" name="email" class="form-control">
                                    {!! $errors->has('email') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('email') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telpon') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nomor Telepon</label>
                                <div class="col-md-10">
                                    <input type="number" class="form-control" name="telpon">
                                    {!! $errors->has('telpon') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('telpon') }} </span>
                                </div>
                            </div> 
                            <div class="form-group {{ $errors->has('instansi') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Instansi</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="instansi" placeholder="ex: Pegawai Universitas Tanjungpura / Mahasiswa Untan / Lainnya (isi sesuai instansi/perusahaan anda)">
                                    {!! $errors->has('instansi') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('instansi') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('nama_kegiatan') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nama Kegiatan</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="nama_kegiatan">
                                    {!! $errors->has('nama_kegiatan') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nama_kegiatan') }} </span>
                                </div>
                            </div>                  
                            <div class="form-group {{ $errors->has('file_identitas') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Upload Identitas Anda</label>
                                <div class="col-md-5">
	                                <input type="file" class="filestyle" data-size="sm" name="file_identitas" accept=".jpg, .jpeg, .png, .pdf">
                                    <span class="help-block"> {{ $errors->first('file_identitas') }} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Upload Surat Peminjaman</label>
                                <div class="col-md-5">
	                                <input type="file" class="filestyle" data-size="sm" name="file_surat" accept=".jpg, .jpeg, .png, .pdf">
                                </div>
                                <div class="col-md-5">
	                    			<p class="text-muted m-b-15 font-13">
	                                    Upload hanya jika diperlukan file pendukung
	                                </p>	                                
                                </div>
                            </div>
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Kirim Data
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
