@extends('layouts.main_template')
@section('judul_konten', 'Reservasi Ruangan UPT.TIK')
@section('breadcrumb', 'Booking Ruangan')

@section('konten')
    
    @if($status == 'failed')
        <div class="row">
            <div class="col-md-12">
            @component('layouts.com_alert')
                @slot('status')
                    danger
                @endslot

                @slot('judul')
                   <i class="md md-error"></i> Ruangan Sedang Dipakai !
                @endslot
                    Ruang sedang dipakai ditanggal tersebut. Silahkan coba tanggal lain.
            @endcomponent
            </div>
        </div>
    @endif
    <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Cek Ruangan</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Lakukan pengecekkan ruangan dahulu sebelum reservasi.
                </p>
                <br><br>
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'cekruang', 'class' => 'form-horizontal', 'role' => 'form' , ]) !!}
                            <div class="form-group">
                                <label class="control-label col-md-2">Rentang Tanggal</label>
                                <div class="col-sm-4">
                                    <div class="input-daterange input-group" id="date-range">
                                        <input type="text" class="form-control" name="dari_tgl" required placeholder="Tanggal Mulai" />
                                        <span class="input-group-addon bg-custom b-0 text-white">sampai</span>
                                        <input type="text" class="form-control" name="sampai_tgl" required placeholder="Tanggal Selesai" />
                                    </div>
                                </div>
                            </div>    

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nama Ruangan</label>
                                <div class="col-sm-4">
                                    <select class="form-control" name="id_ruangan" id="pilih_ruangan">
                                    <option value="99"> Pilih Ruangan </option>
                                        @foreach($data_ruang as $ruang)
                                            <option value="{{ $ruang->id }}"> {{ $ruang->nama_ruang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>   
                            
                            <div class="form-group">
                                <hr>
                                <div class="col-sm-offset-2 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Cek Ruangan
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" id="detail_ruang" style="display: none;">
        <div class="col-sm-6">
            <div class="card-box">
                <div class="row form-group">
                    <label class="col-sm-5 control-label">Nama Ruangan</label>
                    <div class="col-sm-4">
                        <div id="nama_ruang"></div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-sm-5 control-label">Lokasi</label>
                    <div class="col-sm-4">
                        <div id="lokasi"></div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-sm-5 control-label">Kapasitas Ruangan</label>
                    <div class="col-sm-4">
                        <div id="kapasitas"></div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="col-sm-6">  
                <img src="https://c1.staticflickr.com/8/7244/7212832500_e5e3c52ee4.jpg" class="img-thumb" height="162px">          
                {{-- <div class="row form-group">
                    <label class="col-sm-3 control-label">Foto Ruangan</label>
                    <div class="col-sm-4">
                        <div id="foto"></div>
                    </div>
                </div> --}}
        </div>
    </div>

    
@endsection
