@extends('layouts.main_template')
@section('judul_konten', 'Form Booking')
@section('breadcrumb', 'Booking Ruangan / Form Booking')

@section('konten')
    <div class="row">
        <div class="col-sm-12">
                @if($status_simpan == 'berhasil')
                        @component('layouts.com_alert')
                            @slot('status')
                                success
                            @endslot

                            @slot('judul')
                               <b><i class="md md-check"></i> Data berhasil tersimpan</b>
                            @endslot

                            Data anda telah terkirim ke Administrator HelpDesk. <br>
                            <h5 class="m-t-20"><b>Email anda </b> <span class="label label-primary m-l-5">{{ $email }}</span></h5>
                            <h5 class="m-t-20"><b>Kode Reservasi </b> <span class="label label-primary m-l-5">{{ $kode_booking }}</span></h5>
                            Gunakan kode reservasi dan email di atas untuk melacak status reservasi anda. <br>
                            Untuk melacak status permintaan data, klik button "Tracking".
                            <p>
                                <a href="{{ url('/tracking') }}" class="btn btn-default waves-effect waves-light">
                                    Tracking
                                </a>
                            </p>
                        @endcomponent
                @else
                        @component('layouts.com_alert')
                            @slot('status')
                                danger
                            @endslot

                            @slot('judul')
                               <i class="md md-check"></i> Data gagal tersimpan
                            @endslot

                            Terjadi kesalahan. Data yang anda input tidak berhasil tersimpan.
                            Silahkan input kembali data anda.
                        @endcomponent
                @endif
        </div>
    </div>
@endsection
