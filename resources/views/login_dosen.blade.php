@extends('layouts.main_template')
@section('judul_konten', 'Login Dosen')
@section('breadcrumb', 'Komplain / Dosen / Login')

@section('konten')
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Login Form</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Pastikan data login anda benar, untuk bisa melanjutkan ke halaman komplain atau permintaan data :
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('dosen.login.verifikasi') }}" method="POST" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group {{ $errors->has('nip') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">NIP</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="nip" placeholder="masukkan nip anda" required>                                    
                                    {!! $errors->has('nip') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nip') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label" for="example-password">Password</label>
                                <div class="col-md-10">
                                    <input type="password" id="example-password" name="password" class="form-control" placeholder="masukkan password anda" required>
                                    {!! $errors->has('password') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('password') }} </span>
                                </div>
                            </div>             
                            <div class="form-group">
                                    <label class="col-sm-2 control-label">Kategori</label>
                                    <div class="col-sm-5" id="kategori">
                                        <select class="form-control" name="kategori">
                                            <option value="komplain" @if(isset($_GET['kat'])) {{(($_GET['kat']) == 'komplain') ? 'selected' : ''}} @endif >Komplain</option>
                                            <option value="minta_data" @if(isset($_GET['kat'])) {{(($_GET['kat']) == 'permintaan') ? 'selected' : ''}} @endif>Permintaan Data</option>                                            
                                        </select>
                                    </div>
                                </div>                  
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Proses
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
