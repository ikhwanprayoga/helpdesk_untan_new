@extends('layouts.main_template')
@section('judul_konten', 'Pusat Bantuan Siremun')
@section('breadcrumb', 'Siremun')

@section('konten')
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Input Data Pengajuan Komplain Siremun</b></h4>
                <p class="text-muted m-b-30 font-13">
                    Lengkapi data berikut sesuai dengan permasalahan anda :
                </p>
                <div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('siremun.komplain.save') }}" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{-- <input type="hidden" name="kategori_user" value="dosen"> --}}
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategori Pengguna</label>
                                <div class="col-sm-5" id="kategori-penggunra">
                                    <select class="form-control select2" name="kategori_pengguna" required>
                                        <option value="" data-toggle="tooltip" data-placement="right">Pilih Kategori Pengguna</option>
                                        <option value="dosen" data-toggle="tooltip" data-placement="right">Dosen</option>
                                        <option value="tendik" data-toggle="tooltip" data-placement="right">Tendik</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Unit Kerja</label>
                                <div class="col-sm-5" id="unit-kerja">
                                    <select class="form-control select2" name="unit_kerja" required>
                                        <option value="" data-toggle="tooltip" data-placement="right">Pilih Unit Kerja</option>
                                        <option value="Fakultas Hukum" data-toggle="tooltip" data-placement="right">Fakultas Hukum</option>
                                        <option value="FEB" data-toggle="tooltip" data-placement="right">Fakultas Ekonomi dan Bisnis</option>
                                        <option value="Fakultas Pertanian" data-toggle="tooltip" data-placement="right">Fakultas Pertanian</option>
                                        <option value="Fakultas Teknik" data-toggle="tooltip" data-placement="right">Fakultas Teknik</option>
                                        <option value="FISIP" data-toggle="tooltip" data-placement="right">Fakultas Ilmu Sosial dan Ilmu Politik</option>
                                        <option value="FKIP" data-toggle="tooltip" data-placement="right">Fakultas Keguruan dan Ilmu Pendidikan</option>
                                        <option value="Fakultas Kehutanan" data-toggle="tooltip" data-placement="right">Fakultas Kehutanan</option>
                                        <option value="FMIPA" data-toggle="tooltip" data-placement="right">Fakultas MIPA</option>
                                        <option value="Fakultas Kedokteran" data-toggle="tooltip" data-placement="right">Fakultas Kedokteran</option>
                                    </select>
                                </div>
                            </div> 
                            <div class="form-group {{ $errors->has('nama') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nama Lengkap</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="" name="nama" required>                                    
                                    {!! $errors->has('nama') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nama') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('nip') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">NIP</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" value="" name="nip" required>                                    
                                    {!! $errors->has('nip') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nip') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label" for="example-email">Email</label>
                                <div class="col-md-10">
                                    <input type="email" id="example-email" name="email" class="form-control" required>
                                    {!! $errors->has('email') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('email') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('telpon') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Nomor Telepon</label>
                                <div class="col-md-1">
                                    <input type="text" class="form-control" value="+62" disabled="">
                                </div>
                                <div class="col-md-8">
                                    <input type="number" class="form-control" id="no_hp" name="telpon" required>
                                    {!! $errors->has('telpon') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('telpon') }} </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategori Komplain</label>
                                <div class="col-sm-5" id="kategori">
                                    <select class="form-control select2" name="kategori" required>
                                        <option value="" data-toggle="tooltip" data-placement="right">Pilih Kategori Komplain</option>
                                        <option value="rubrik" data-toggle="tooltip" data-placement="right">Rubrik</option>
                                        <option value="perhitungan-point" data-toggle="tooltip" data-placement="right">Perhitungan Point</option>
                                        <option value="sistem" data-toggle="tooltip" data-placement="right">Sistem</option>
                                    </select>
                                </div>
                            </div>   
                            <div class="form-group {{ $errors->has('judul_komplain') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Judul Komplain</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="judul_komplain" required>
                                    {!! $errors->has('judul_komplain') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('judul_komplain') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('isi_komplain') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Isi Komplain</label>
                                <div class="col-md-10">
                                    <textarea class="form-control" rows="5" name="isi_komplain" required></textarea>
                                    {!! $errors->has('isi_komplain') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('isi_komplain') }} </span>
                                </div>
                            </div>      
                            <div class="form-group {{ $errors->has('file_tambahan') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Upload File Pendukung</label>
                                <div class="col-md-5">
	                                <input type="file" class="filestyle" data-size="sm" name="file_tambahan" accept=".jpg, .jpeg, .png, .pdf">
                                    <span class="help-block"> {{ $errors->first('file_tambahan') }} </span>
                                </div>
                                <div class="col-md-5">
	                    			<p class="text-muted m-b-15 font-13">
	                                    Upload hanya jika diperlukan file pendukung
	                                </p>	                                
                                </div>
                            </div>
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Kirim Data
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
