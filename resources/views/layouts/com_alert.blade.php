<div class="alert alert-{{ $status }} fade in m-b-0">
    <h4>{{ $judul }}</h4>
    <p>
        {{ $slot }}
    </p>    
</div>
