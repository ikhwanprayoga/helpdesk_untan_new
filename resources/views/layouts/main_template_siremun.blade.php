<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">
        <!-- App Favicon icon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">
        <!-- App Title -->
        <title>HelpDesk - UNTAN</title>

        <!-- DataTables -->
        <link href="{{ asset('plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/dataTables.colVis.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
        <link href="{{ asset('plugins/datatables/fixedColumns.dataTables.min.css') }}" rel="stylesheet" type="text/css"/>
    
        <!-- Plugins css -->
        <link href="{{ asset('plugins/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/clockpicker/css/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href="{{ asset('plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />


        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/core.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/components.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/icons.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/pages.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/menu.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />

        <!-- swet alert -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">


        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="{{ asset('js/modernizr.min.js') }}"></script>
        <style>
                #user {
                    font-weight: 600;
                    letter-spacing: 0em;
                    /* padding: 0em; */
                    font-size: 55%;
                }
                .ekko-lightbox .modal-dialog {
                    max-width: 600px !important;
                }
                .ekko-lightbox .modal-dialog .modal-content {
                    padding: 0px;
                }
        </style>
        <!--Start of Zendesk Chat Script-->
    {{-- <script type="text/javascript">
        window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
        d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
        _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
        $.src="https://v2.zopim.com/?5un8by5DQ7xg3QtS1FODPTKMor6VyTK2";z.t=+new Date;$.
        type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script> --}}
    <!--End of Zendesk Chat Script-->
    </head>


    <body>        

            <!-- Top Bar Start -->
            @include('layouts.header')
            <!-- Top Bar End -->

            {{-- notifikasi pnotify --}}
                {!! Notify::render() !!}
            {{-- ./notifikasi pnotify --}}



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="wrapper">
                <div class="container">

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="page-title"> @yield('judul_konten') </h4>
                                <ol class="breadcrumb">
									<li>
										<a href="#">Home</a>
									</li>
									<li class="active">
										@yield('breadcrumb')
									</li>
								</ol>
                            </div>
                        </div>
                        <!-- ./  Page-Title -->
                        
                        <!-- ini untuk notif pesan bro -->
                        <div id="pesan" data-pesan="info"></div>
                        <!-- ./ini untuk notif pesan bro -->
                    

                        <!-- konten utama disini ya bro -->
                        <div class="row">  
                            {{-- <div class="card-box"> --}}
                                @yield('konten')
                            {{-- </div> --}}
                        </div>
                        <!-- ./konten utama disini ya bro -->
                               
                
                <!-- footer -->
                @include('layouts.footer')
                <!--./footer -->          


                </div> <!-- content -->
            </div>
            
            
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


    
        <!-- jQuery  -->
        {{-- <script src="{{ asset('js/jquery.min.js') }}"></script> --}}
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/detect.js') }}"></script>
        <script src="{{ asset('js/fastclick.js') }}"></script>
        <script src="{{ asset('js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('js/waves.js') }}"></script>
        <script src="{{ asset('js/wow.min.js') }}"></script>
        <script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
        
        <!-- buatan erwin js -->
        <script src="{{ asset('js/my.js') }}" type="text/javascript"></script>
        
        <!-- plugins js -->
            <script src="{{ asset('plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>

            
            <!-- plugins js - datable -->
            <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.bootstrap.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.buttons.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/buttons.bootstrap.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/jszip.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/pdfmake.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/vfs_fonts.js') }}"></script>
            <script src="{{ asset('plugins/datatables/buttons.html5.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/buttons.print.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.keyTable.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.responsive.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/responsive.bootstrap.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.scroller.min.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.colVis.js') }}"></script>
            <script src="{{ asset('plugins/datatables/dataTables.fixedColumns.min.js') }}"></script>
        
            <!-- plugins js - datepicker -->
            <script src="{{ asset('plugins/moment/moment.js') }}"></script>
            <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
            <script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
            <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
            <script src="{{ asset('plugins/clockpicker/js/bootstrap-clockpicker.min.js') }}"></script>
            <script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        
        <!-- page js -->
        <script src="{{ asset('pages/datatables.init.js') }}"></script>
        <script src="{{ asset('pages/jquery.form-pickers.init.js') }}"></script>


        <!-- App core js -->
        <script src="{{ asset('js/jquery.core.js') }}"></script>
        <script src="{{ asset('js/jquery.app.js') }}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

        <!-- Include this after the sweet alert js file -->
        @include('sweet::alert')

        

        <script type="text/javascript">
            $( document ).ready(function() {
                $('.datatable').dataTable({ "ordering": false });
                $('#datatable').dataTable({ "ordering": false });
                $('#datatable-keytable').DataTable({keys: true});
                $('#datatable-responsive').DataTable();
                $('#datatable-colvid').DataTable({
                    "dom": 'C<"clear">lfrtip',
                    "colVis": {
                        "buttonText": "Change columns"
                    }
                });
                $('#datatable-scroller').DataTable({
                    ajax: "assets/plugins/datatables/json/scroller-demo.json",
                    deferRender: true,
                    scrollY: 380,
                    scrollCollapse: true,
                    scroller: true
                });
                var table = $('#datatable-fixed-header').DataTable({
                    // "ordering":false,
                    fixedHeader: true,
                });
                var table = $('#datatable-fixed-col').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    fixedColumns: {
                        leftColumns: 1,
                        rightColumns: 1
                    }
                });
            });
            TableManageButtons.init();
                // var pesan = $("#pesan").data('pesan');
                // console.log(pesan);
                // if(pesan == 'info') {
                //     $.Notification.confirm('success','top right', 'Are you nuts?!');
                //     $.Notification.autoHideNotify('info', 'top right', 'Info', 'Pesan informasi');
                // } else if(pesan == 'success') {
                //     $.Notification.autoHideNotify('success', 'top right', 'Success', 'Pesan sukses');
                // } else if(pesan == 'warning') {
                //     $.Notification.autoHideNotify('warning', 'top right', 'Warning', 'Pesan warning');
                // } else {
                //     $.Notification.autoHideNotify('error', 'top right', 'Error', 'Pesan error');
                // }
            
            $('.select2').select2();
            $('#kategori [data-toggle="tooltip"]').tooltip();
            $('a[data-toggle="tooltip"]').tooltip({
                animated: 'fade',
                placement: 'bottom',
                html: true
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function ($) {

                // delegate calls to data-toggle="lightbox"
                $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function (event) {
                    event.preventDefault();
                    return $(this).ekkoLightbox({
                        onShown: function () {
                            if (window.console) {
                                return console.log('onShown event fired');
                            }
                        },
                        onContentLoaded: function () {
                            if (window.console) {
                                return console.log('onContentLoaded event fired');
                            }
                        },
                        onNavigate: function (direction, itemIndex) {
                            if (window.console) {
                                return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
                            }
                        }
                    });
                });

                // //Programatically call
                // $('#open-image').click(function (e) {
                //     e.preventDefault();
                //     $(this).ekkoLightbox();
                // });
                // $('#open-youtube').click(function (e) {
                //     e.preventDefault();
                //     $(this).ekkoLightbox();
                // });

                // $(document).delegate('*[data-gallery="navigateTo"]', 'click', function (event) {
                //     event.preventDefault();
                //     return $(this).ekkoLightbox({
                //         onShown: function () {
                //             var lb = this;
                //             $(lb.modal_content).on('click', '.modal-footer a#jumpit', function (e) {
                //                 e.preventDefault();
                //                 lb.navigateTo(2);
                //             });
                //             $(lb.modal_content).on('click', '.modal-footer a#closeit', function (e) {
                //                 e.preventDefault();
                //                 lb.close();
                //             });
                //         }
                //     });
                // });

            });

        </script>

        <script>
            $(document).ready(function() {
                // embed pdf ketika klik file
                $('.btn-file-pdf').on('click', function(){
                    var file_pdf 	= $(this).data('file');
                    var dt_kategori	= $(this).data('kategori');
                    var url 		= 'https://docs.google.com/viewer?url=';

                    $('#embed-pdf').attr("src", url + "{{ asset('storage/') }}" + "/" + dt_kategori + "/" + file_pdf + '&embedded=true');

                });
                // embed pdf ketika klik file
            });		
        </script>

    
    </body>
</html>
