<!-- Footer -->
<footer class="footer text-right">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                © UPT.TIK Universitas Tanjungpura 2017. All rights reserved.
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->
