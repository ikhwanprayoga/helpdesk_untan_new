        <!-- Navigation Bar-->
        <header id="topnav">
            <div class="topbar-main">
            {{-- @if(!Auth::check()) --}}

                <div class="container">

                    <!-- Logo container-->
                    <div class="logo">
                        <a href="#" class="logo">
                            <span>H<i class="md md-perm-phone-msg"></i>LPDESK
                            </span>&nbsp; UNTAN
                        </a>
                    </div>
                    <!-- End Logo container-->      

                    <div class="menu-extras">
                        <div class="menu-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                            @if(Auth::check())
                                <ul class="nav navbar-nav navbar-right pull-right">
                                    <li class="navbar-c-items">
                                        <form role="search" class="navbar-left app-search pull-left">
                                            <ul class="visible-xs text-primary">

                                                <h5 style="color: #9E9E9E;font-size:1.15em;">
                                                    <span id="user" class="label label-inverse" style="border-radius:0px">{{ Auth::user()->name }} </span>
                                                </h5>
                                            </ul>
                                            <ul class="hidden-xs text-primary">                                            
                                                <h4 style="color: #9E9E9E;">&ensp;Selamat datang &nbsp;
                                                    <span class="label label-inverse" style="border-radius:0px">
                                                        {{ Auth::user()->name }}</span>
                                                </h4>
                                            </ul>
                                        </form>
                                    </li>
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>         
            
            
            <div class="navbar-custom">
                <div class="container">
                    <div id="navigation">
                        <!-- Navigation Menu-->
                        {{-- halaman untuk user --}}
                        @if(Auth::guest())         
                            <ul class="navigation-menu">
                                <li class="has-submenu"> 
                                    <a href="{{ url('/') }}"><i class="md md-home"></i>Home</a>
                                </li>
                                <li class="has-submenu">
                                    <a href="#"><i class="md md-speaker-notes"></i>Komplain</a>
                                    <ul class="submenu">
                                        <li>
                                            <a href="{{ url('/komplain') }}" data-toggle="tooltip" data-placement="top" title="Komplain sebagai 'Umum', jika bukan pegawai atau mahasiswa di Universitas Tanjungpura">Umum</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('unitkerja.komplain') }}" data-toggle="tooltip" data-placement="top" title="Komplain sebagai 'Unit Kerja', jika anda adalah Pegawai disalah satu unit kerja Universitas Tanjungpura">Unit Kerja (Untan)</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('dosen.login') }}?kat=komplain" data-toggle="tooltip" data-placement="top" title="Komplain sebagai 'Dosen', jika anda dosen di Universitas Tanjungpura">Dosen</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('mahasiswa.login') }}?kat=komplain" data-toggle="tooltip" data-placement="top" title="Komplain sebagai 'Mahasiswa', jika anda mahasiswa Universitas Tanjungpura">Mahasiswa</a>
                                        </li>
                                    </ul>
                                </li>    
                                <li class="has-submenu">
                                    <a href="#"><i class="md md-folder"></i>Permintaan Data</a>
                                    <ul class="submenu">
                                        <li>
                                            <a href="{{ route('permintaan.index') }}" data-toggle="tooltip" data-placement="top" title="Ajukan permintaan data sebagai 'Umum', jika bukan pegawai atau mahasiswa di Universitas Tanjungpura">Umum</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('unitkerja.permintaan') }}" data-toggle="tooltip" data-placement="top" title="Ajukan permintaan data sebagai 'Unit Kerja', jika anda adalah Pegawai disalah satu unit kerja Universitas Tanjungpura">Unit Kerja (Untan)</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('dosen.login') }}?kat=permintaan" data-toggle="tooltip" data-placement="top" title="Ajukan permintaan data sebagai 'Dosen', jika anda dosen di Universitas Tanjungpura">Dosen</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('mahasiswa.login') }}?kat=permintaan" data-toggle="tooltip" data-placement="top" title="Ajukan permintaan data sebagai 'Mahasiswa', jika anda mahasiswa Universitas Tanjungpura">Mahasiswa</a>
                                        </li>
                                    </ul>
                                </li>
                                {{-- <li class="has-submenu">
                                    <a href="{{ url('/cekruang') }}"><i class="fa fa-ticket"></i>Booking Ruangan</a>
                                </li> --}}
                                <li class="has-submenu">
                                    <a href="{{ url('/siremun') }}"><i class="md md-payment"></i>Komplain Siremun</a>
                                </li>
                                <li class="has-submenu">
                                    <a href="{{ url('/tracking') }}"><i class="md md-search"></i>Tracking</a>
                                </li>
                            </ul>
                        @else
                            <ul class="navigation-menu">
                                <li class="has-submenu"> 
                                    <a href="{{ url('/') }}"><i class="md md-home"></i>Home</a>
                                </li>
                                <li class="has-submenu">
                                    <a href="#"><i class="md md-speaker-notes"></i>Komplain</a>
                                    @if(Auth::user()->role == 'sd')
                                        @php 
                                            $notif_komplain_sd = \App\Komplain::where('status', 'Terkirim')->count();
                                        @endphp
                                        @if($notif_komplain_sd > 0)
                                            <span class="badge badge-sm badge-danger">
                                                {{ $notif_komplain_sd }}
                                            </span>
                                        @endif
                                    @endif
                                    @if(Auth::user()->role == 'pg' || Auth::user()->role == 'tk')
                                        @php 
                                            $notif_komplain_pgtk = \App\ChatKomplain::where('user_tujuan', Auth::id())->where('status', 'Belum Baca')->count();
                                        @endphp

                                        @if($notif_komplain_pgtk > 0)
                                            <span class="badge badge-sm badge-danger">
                                                {{ $notif_komplain_pgtk }}
                                            </span>
                                        @endif
                                    @endif
                                    <ul class="submenu">
                                        @if(Auth::user()->role == 'sd')
                                            <li>
                                                <a href="{{ route('admin.komplain') }}">Data Komplain</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('admin.komplain.pesan.masuk') }}">Percakapan </a>
                                            </li>
                                        @else
                                        <li>
                                            <a href="{{ route('admin.komplain.pesan.masuk') }}">Komplain Masuk</a>
                                        </li>
                                        @endif
                                        {{-- <li>
                                            <a href="{{ route('admin.komplain.pesan.keluar') }}">Pesan Keluar</a>
                                        </li> --}}
                                    </ul>
                                </li>
                                <li class="has-submenu">
                                    <a href="#"><i class="md md-speaker-notes"></i>Permintaan</a>
                                    @if(Auth::user()->role == 'sd')
                                        @php 
                                            $notif_permintaan_sd = \App\Permintaan::where('status', 'Terkirim')->count();
                                        @endphp

                                        @if($notif_permintaan_sd > 0)
                                            <span class="badge badge-sm badge-danger">
                                                {{ $notif_permintaan_sd  }}
                                            </span>
                                        @endif
                                    @endif
                                    @if(Auth::user()->role == 'pg' || Auth::user()->role == 'tk')
                                        @php 
                                            $notif_permintaan_pgtk = \App\ChatPermintaan::where('user_tujuan', Auth::id())->where('status', 'Belum Baca')->count();
                                        @endphp

                                        @if($notif_permintaan_pgtk > 0)
                                            <span class="badge badge-sm badge-danger">
                                                {{ $notif_permintaan_pgtk  }}
                                            </span>
                                        @endif
                                    @endif
                                    <ul class="submenu">
                                        @if(Auth::user()->role == 'sd')
                                            <li>
                                                <a href="{{ route('admin.permintaan') }}">Data Permintaan</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('admin.permintaan.pesan.masuk') }}">Percakapan </a>
                                            </li>
                                        @else
                                        <li>
                                            <a href="{{ route('admin.permintaan.pesan.masuk') }}">Permintaan Masuk</a>
                                        </li>
                                        @endif
                                        {{-- <li>
                                            <a href="{{ route('admin.komplain.pesan.keluar') }}">Pesan Keluar</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                    <a href="#"><i class="md md-payment"></i>Komplain Siremun</a>
                                    @if(Auth::user()->role == 'sd')
                                        @php 
                                            $notif_komplain_siremun_sd = \App\KomplainSiremun::where('status', 'Terkirim')->count();
                                        @endphp

                                        @if($notif_komplain_siremun_sd > 0)
                                            <span class="badge badge-sm badge-danger">
                                                {{ $notif_komplain_siremun_sd  }}
                                            </span>
                                        @endif
                                    @endif
                                    <ul class="submenu">
                                        @if(Auth::user()->role == 'sd')
                                            <li>
                                                <a href="{{ route('admin.siremun.komplain') }}">Data Komplain Siremun</a>
                                            </li>
                                            {{-- <li>
                                                <a href="{{ route('admin.siremun.komplain.pesan.masuk') }}">Percakapan </a>
                                            </li> --}}
                                        @endif
                                        {{-- <li>
                                            <a href="{{ route('admin.komplain.pesan.keluar') }}">Pesan Keluar</a>
                                        </li> --}}
                                    </ul>
                                </li>

                                <li class="has-submenu">
                                        <a href="{{ route('admin.profil.edit') }}"><i class="md md-account-box"></i>Edit Profil</a>
                                    </li>
                                <li class="has-submenu">
                                    <a href="{{ url('/logout') }}"><i class="md md-settings-power"></i>Logout</a>
                                </li>

                            </ul>     
                        @endif
                        <!-- End navigation menu        -->
                    </div>
                </div> <!-- end container -->
            </div> <!-- end navbar-custom -->
        </header>
        <!-- End Navigation Bar-->
