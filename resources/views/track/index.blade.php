@extends('layouts.main_template')
@section('judul_konten', 'Tracking')
@section('breadcrumb', 'Tracking')

@section('konten')
    
     @if($status == 'failed')
        <div class="row">
            <div class="col-md-12">
            @component('layouts.com_alert')
                @slot('status')
                    danger
                @endslot

                @slot('judul')
                   <i class="md md-error"></i> Data tidak ditemukan !
                @endslot
                    Data anda tidak ditemukan, silahkan coba lagi.
            @endcomponent
            </div>
        </div>
        @endif
    <br>
	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Tracking Data</b></h4>
                {{-- <p class="text-muted m-b-30 font-13">
                    Lengkapi data berikut sesuai dengan permasalahan anda :
                </p> --}}

                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['url' => 'tracking', 'class' => 'form-horizontal', 'role' => 'form' , ]) !!}
                            <div class="form-group {{ $errors->has('nama') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label">Kode</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="kode">                                    
                                    {!! $errors->has('nama') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('nama') }} </span>
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? 'has-error has-feedback' : '' }}">
                                <label class="col-md-2 control-label" for="example-email">Email</label>
                                <div class="col-md-5">
                                    <input type="email" id="example-email" name="email" class="form-control">
                                    {!! $errors->has('email') ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : '' !!}
                                    <span class="help-block"> {{ $errors->first('email') }} </span>
                                </div>
                            </div>                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kategori</label>
                                <div class="col-sm-5">
                                    <select class="form-control" name="kategori">
                                        <option value="siremun">Siremun</option>
                                        <option value="komplain">Komplain</option>
                                        <option value="permintaan_data">Permintaan Data</option>
                                        {{-- <option value="reservasi">Reservasi Ruangan</option> --}}
                                    </select>
                                </div>
                            </div>                            
                            
                            <div class="form-group">
                            	<hr>
                            	<div class="col-sm-offset-4 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        Kirim Data
                                    </button>
                                    <button type="reset" class="btn btn-default m-l-5">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
