@extends('layouts.main_template')
@section('judul_konten', 'Kirim Catatan')
@section('breadcrumb', 'Detail Tracking Komplain / Kirim Catatan')

@section('konten')
    <div class="row">
		<div class="col-lg-12">
	    	<div class="card-box">
		        <h4 class="m-t-0 header-title"><b>Kirim Catatan (Kategori Komplain)</b></h4><hr>
                <form action="{{ route('tracking.kirim_catatan.save') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    {{ csrf_field() }} 
                    <input type="hidden" name="kategori" value="{{ session('kat_tracking') }}">
					<div class="form-group">
			            <label class="col-md-2 control-label">Catatan</label>
			            <div class="col-md-10">
			                <textarea class="form-control" rows="5" name="catatan"></textarea>
			            </div>
			        </div>
			        <div class="form-group {{ $errors->has('file') ? 'has-error has-feedback' : '' }}">
                        <label class="col-md-2 control-label">Lampiran</label>
                        <div class="col-md-5">
                            <input type="file" class="filestyle" data-size="sm" name="file">
                            <span class="help-block"> {{ $errors->first('file') }} </span>
                        </div>
                        <div class="col-md-5">
                            <p class="text-muted m-b-15 font-13">
                                Upload hanya jika diperlukan file pendukung
                            </p>	                                
                        </div>
					</div>
					<div class="form-group">
                    	<div class="col-sm-offset-2 col-sm-9 m-t-15">
                            <button type="submit" class="btn btn-primary">
                                Submit
                            </button>
                            <button type="reset" class="btn btn-default m-l-5">
                                Reset
                            </button>
                        </div>
                    </div>
				</form>
			</div>
		</div>
	</div>


@endsection