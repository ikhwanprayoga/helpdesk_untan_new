@extends('layouts.main_template')
@section('judul_konten', 'Tracking Komplain')
@section('breadcrumb', 'Detail Tracking Komplain')

@section('konten')
	{{-- detail komplain  --}}
	<div class="row">
		<div class="col-lg-12">
		    <div class="card-box">
		        <h4 class="m-t-0 m-b-20 header-title"><b>Detail Komplain</b></h4><hr> 
		   	        <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 " for="example-email">Kode Komplain</label>
	                    <div class="col-md-5">
	                        <p class="text-success tran-text"><strong>{{ $data->kode_komplain }}</strong></p> 	
	                    </div>
	                </div>  	
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Nama</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->nama }}</p> 	
	                    </div>
	                </div> 	                 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Email</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->email }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Telpon</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->telpon }}</p> 	
	                    </div>
					</div> 
					<div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori User</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->tag }}</p> 	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kategori</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->kategori }}</p> 	
	                    </div>
	                </div>     	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Prioritas</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $data->prioritas}}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Judul Komplain</label>
	                    <div class="col-md-8">
	                        <p class="text-default tran-text">{{ $data->judul }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Isi Komplain</label>
	                    <div class="col-md-8">
	                        <p class="text-default tran-text">{{ $data->isi }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
	                    <div class="col-md-5">
						@if(!empty($data->file_identitas))
							@php
								$ext_fidentitas = pathinfo($data->file_identitas, PATHINFO_EXTENSION);
							@endphp

							@if($ext_fidentitas == 'pdf') 
								<button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $data->file_identitas }}" data-target=".bs-example-modal-lg">Lihat file</button>
							@elseif($ext_fidentitas == 'jpg' || $ext_fidentitas == 'jpeg' || $ext_fidentitas == 'png')
								<a target="_BLANK" href="{{ asset('storage/komplain/'.$data->file_identitas) }}" data-toggle="lightbox" data-title="File Identitas" data-footer="{{ $data->file_identitas }}" data-gallery="imagesizes">
									<i class="md md-file-download"></i>&nbsp;Lihat file
								</a>
							@else
								<a target="_BLANK" href="{{ asset('storage/komplain/'.$data->file_identitas) }}">
									<button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
								</a>
							@endif	
						@endif
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Tambahan</label>
	                    <div class="col-md-5">
	                    @if(!empty($data->file_tambahan))
							@php
								$ext_ftambahan = pathinfo($data->file_tambahan, PATHINFO_EXTENSION);
							@endphp

							@if($ext_ftambahan == 'pdf') 
								<button class="btn btn-primary btn-xs waves-effect waves-light btn-file-pdf" data-toggle="modal" data-kategori="komplain" data-file="{{ $data->file_tambahan }}" data-target=".bs-example-modal-lg">Lihat file</button>
							@elseif($ext_ftambahan == 'jpg' || $ext_ftambahan == 'jpeg' || $ext_ftambahan == 'png')
								<a href="{{ asset('storage/komplain/'.$data->file_tambahan) }}" data-toggle="lightbox" data-title="File Pendukung" data-footer="{{ $data->file_tambahan }}" data-gallery="imagesizes">
									<i class="md md-file-download"></i>&nbsp;Lihat file
								</a>								
							@else
								<a target="_BLANK" href="{{ asset('storage/komplain/'.$data->file_tambahan) }}">
									<button class="btn btn-primary btn-xs waves-effect waves-light">Lihat file</button>
								</a>
							@endif	
			        	@endif
	                    </div>
	                </div> 
		    		<span class="clearfix"></span>
		    </div>
		</div>
	</div>
	{{-- ./detail komplain  --}}

	{{-- status komplain  --}}
    <div class="row">
    	<div class="col-lg-12">
    	<div class="card-box">
	        <h4 class="m-t-0 header-title"><b>RESPON ADMIN</b></h4><hr>
			<div class="row">
				<a href="{{ route('tracking.kirim_catatan') }}" >
					<button type="button" class="btn btn-default pull-right" data-toggle="tooltip" data-placement="top" title="Gunakan fitur ini jika admin meminta respon dari anda untuk melengkapi data komplain berupa catatan dan atau file. Atau jika anda ingin menambahkan catatan dikomplain yang anda buat kepada Admin Helpdesk." style="margin-right:25px"><i class="fa fa-send"></i>&ensp;Kirim Catatan ke Admin</button>
				</a>
			</div>
	        <div class="p-20">
				<div class="table-responsive">
					<table class="table m-0">
	                    <thead>
	                        <tr>
	                            <th>Status</th>
	                            <th>Catatan</th>
	                            <th>Lampiran</th>
	                            <th>Tanggal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($data->respon_komplain as $respon)
	                        <tr>
	                            <th scope="row">
	                            	<?php
	                            		if($respon->status == 'Terkirim'){
	                            			$label = 'label-success';
	                            		} elseif($respon->status == 'Ditolak') {
	                            			$label = 'label-danger';
	                            		} elseif($respon->status == 'Proses') {
	                            			$label = 'label-warning';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $respon->status }}</span>
	                            </th>
	                            <td>{{ $respon->note }}</td>
	                            <td>
	                            	@if(!empty($respon->file))
				                        <a target="_BLANK" href="{{ asset('storage/komplain/'.$respon->file) }}">
						        			<i class="md md-file-download">&nbsp;Download File</i></p>
										</a>
					        		@endif
								</td> 
	                            <td>{{ ( date('d-m-Y H:i:s', strtotime($respon->created_at)) ) }}</td>
	                        </tr>
	                        @endforeach	                        
	                    </tbody>
	                </table>
	            </div>
	    	</div>
		</div>
    	</div>
	</div>
	{{-- ./status komplain  --}}

	<div class="row">
    	<div class="col-lg-12">
    	<div class="card-box">
	        <h4 class="m-t-0 header-title"><b>RESPON USER</b></h4><hr>
	        <p class="text-muted font-13">
				Daftar respon yang anda buat dan terkirim ke Admin HelpDesk. Untuk memberikan respon atas komplain anda, silahkan klik tombol &ensp;
				<a href="{{ route('tracking.kirim_catatan') }}" >
						<button type="button" class="btn btn-xs btn-default" data-toggle="tooltip" data-placement="top" title="Gunakan fitur ini jika admin meminta respon dari anda untuk melengkapi data komplain berupa catatan dan atau file. Atau jika anda ingin menambahkan catatan dikomplain yang anda buat kepada Admin Helpdesk." style="margin-right:25px"><i class="fa fa-send"></i>&ensp;Kirim Catatan ke Admin</button>
				</a>
	        </p>

	        <div class="p-20">
	            <div class="table-responsive">
	                <table class="table m-0">
	                    <thead>
	                        <tr>
	                            <th>No</th>
	                            <th>Catatan</th>
	                            <th>Lampiran</th>
	                            <th>Tanggal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
							@foreach($data->respon_komplain_user as $res)
	                    	<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $res->catatan }}</td>
								<td>
									@if(!empty($res->file))
										<a target="_BLANK" href="{{ asset('storage/komplain/'.$res->file) }}">
											<i class="md md-file-download">&nbsp;Download File</i></p>
										</a>
									@endif
								</td> 
								<td>{{ ( date('d-m-Y H:i:s', strtotime($respon->created_at)) ) }}</td>
							</tr>	    
							@endforeach
	                    </tbody>
	                </table>
	            </div>
	    	</div>
		</div>
    	</div>
	</div>
	{{-- ./status komplain  --}}


	{{-- modal ni --}}
    <!-- Bootstrap Modals -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myLargeModalLabel">File</h4>
				</div>
				<div class="modal-body">
						<iframe id="embed-pdf" style="width:100%;height:800px;" frameborder="0"></iframe>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	{{-- modal ni --}}

@endsection
