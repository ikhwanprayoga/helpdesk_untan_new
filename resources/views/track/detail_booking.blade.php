@extends('layouts.main_template')
@section('judul_konten', 'Tracking Data Reservasi')
@section('breadcrumb', 'Detail Tracking Reservasi')

@section('konten')
	{{-- detail booking  --}}
	<div class="row">
		<div class="col-lg-12">
		    <div class="card-box">
		        <h4 class="m-t-0 m-b-20 header-title"><b>Detail Reservasi</b></h4><hr> 
		    @foreach($data['booking'] as $detail)	
			         <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kode Reservasi</label>
	                    <div class="col-md-5">
	                        <p class="text-success tran-text"><strong>{{ $detail->kode_booking }}</strong></p> 	
	                    </div>
	                </div>  	
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Nama</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->nama }}</p> 	
	                    </div>
	                </div> 	                 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Email</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->email }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Telpon</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->telpon }}</p> 	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Kegiatan</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->kegiatan }}</p> 	
	                    </div>
	                </div>     	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">Instansi</label>
	                    <div class="col-md-5">
	                        <p class="text-default tran-text">{{ $detail->instansi }}</p> 	
	                    </div>
	                </div> 	                
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Identitas</label>
	                    <div class="col-md-5">
	                        <a href="{{ url('/download/booking/'.$detail->file_identitas) }}">
			        			<i class="md md-file-download"></i>&nbsp;{{ $detail->file_identitas }}
			        		</a>	
	                    </div>
	                </div> 
	                <div class="row form-group">
	                    <label class="col-md-2 col-xs-offset-1 control-label" for="example-email">File Surat</label>
	                    <div class="col-md-5">
	                     @if(!empty($detail->file_surat))
	                        <a href="{{ url('/download/booking/'.$detail->file_surat) }}">
			        			<i class="md md-file-download"></i>&nbsp;{{ $detail->file_surat }}</p>
			        		</a>	
			        	 @endif
	                    </div>
	                </div> 
	                <span class="clearfix"></span>
		    </div>
		</div>
	</div>
	{{-- ./detail booking  --}}

	{{-- status booking  --}}
    <div class="row">
    	<div class="col-lg-12">
    	<div class="card-box">
	        <h4 class="m-t-0 header-title"><b>Status Reservasi</b></h4><hr>
	        {{-- <p class="text-muted font-13">
	            Create responsive tables by wrapping any <code>.table</code> in <code>.table-responsive</code>
	            to make them scroll horizontally on small devices (under 768px).
	        </p> --}}

	        <div class="p-20">
	            <div class="table-responsive">
	                <table class="table m-0">
	                    <thead>
	                        <tr>
	                            <th>Status</th>
	                            <th>Catatan</th>
	                            <th>Lampiran</th>
	                            <th>Tanggal</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@foreach($detail->respon as $respon)
	                        <tr>
	                            <th scope="row">
	                            	<?php
	                            		if($respon->status == 'Terkirim'){
	                            			$label = 'label-success';
	                            		} elseif($respon->status == 'Ditolak') {
	                            			$label = 'label-danger';
	                            		} elseif($respon->status == 'Proses') {
	                            			$label = 'label-warning';
	                            		} else {
	                            			$label = 'label-primary';
	                            		}

	                            	?>	                            		
	                            	<span class="label {{ $label }}">{{ $respon->status }}</span>
	                            </th>
	                            <td>{{ $respon->note }}</td>
	                            <td>
	                            	@if(!empty($respon->file))
				                        <a href="{{ url('/download/booking/'.$respon->file) }}">
						        			<i class="md md-file-download">&nbsp;{{ $respon->file }}</i></p>
						        		</a>
					        		@endif
					        	</td>
	                            <td>{{ $respon->updated_at }}</td>
	                        </tr>
	                        @endforeach
	        @endforeach
	                        
	                    </tbody>
	                </table>
	            </div>
	    	</div>
		</div>
    	</div>
	</div>
	{{-- ./status booking  --}}

@endsection
