<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMintadataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_permintaan');
            $table->integer('unit_kerja_id')->nullable();
            $table->string('kategori');
            $table->string('email');
            $table->string('telpon');            
            $table->string('nama');
            $table->string('prioritas');
            $table->text('kebutuhan_informasi');
            $table->text('alasan');
            $table->string('cara_peroleh')->nullable();
            $table->string('format_bahan')->nullable();
            $table->string('file_identitas')->nullable();
            $table->string('file_tambahan')->nullable();;
            $table->string('user_tujuan')->nullable();;
            $table->boolean('active')->default(1);
            $table->string('pj_pengelola')->nullable();
            $table->string('tag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan');
    }
}
