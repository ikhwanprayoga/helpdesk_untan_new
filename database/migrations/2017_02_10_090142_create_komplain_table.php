<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomplainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komplain', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_komplain');
            $table->integer('unit_kerja_id')->nullable();
            $table->string('kategori');
            $table->string('email');
            $table->string('telpon');            
            $table->string('nama');
            $table->string('prioritas');
            $table->string('judul');
            $table->text('isi');
            $table->string('pj_pengelola')->nullable();
            $table->string('tag')->nullable();
            $table->string('file_identitas')->nullable();
            $table->string('file_tambahan')->nullable();
            $table->string('status')->nullable();;
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komplain');
    }
}
