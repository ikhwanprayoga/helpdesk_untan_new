<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKomplainSiremunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komplain_siremun', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip');
            $table->string('kode_komplain');
            $table->string('kategori_pengguna')->nullable();
            $table->string('unit_kerja')->nullable();
            $table->string('kategori');
            $table->string('email');
            $table->string('telpon');            
            $table->string('nama');
            $table->string('prioritas');
            $table->string('judul');
            $table->text('isi');
            $table->string('pj_pengelola')->nullable();
            $table->string('tag')->nullable();
            $table->string('file_identitas')->nullable();
            $table->string('file_tambahan')->nullable();
            $table->string('status')->nullable();;
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komplain_siremun');
    }
}
