<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingRuangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_ruang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode_booking');
            $table->string('nama');
            $table->string('email');
            $table->string('telpon');
            $table->string('kegiatan');
            $table->text('instansi');
            $table->string('file_identitas');
            $table->string('file_surat');
            $table->string('data_ruang_id');
            $table->date('dari_tgl');
            $table->date('sampai_tgl');
            $table->time('dari_jam')->nullable();
            $table->time('sampai_jam')->nullable();
            $table->string('status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('booking_ruang');
    }
}
