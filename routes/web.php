<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/email', 'EmailController@kirim_email');
Route::get('/template-email', function(){
	return view('layouts.email.admin.pesan_masuk');
});

########################## AUTH ##########################
Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
########################## AUTH ##########################


########################## USER ##########################
Route::get('/home', 'HomeController@index');

Route::get('/mahasiswa/login', 'LoginMahasiswaController@login')->name('mahasiswa.login');
Route::post('/mahasiswa/login', 'LoginMahasiswaController@verifikasi')->name('mahasiswa.login.verifikasi');

Route::get('/dosen/login', 'LoginDosenController@login')->name('dosen.login');
Route::post('/dosen/login', 'LoginDosenController@verifikasi')->name('dosen.login.verifikasi');

Route::get('/mahasiswa/komplain', 'KomplainController@index_mahasiswa')->name('mahasiswa.komplain');
Route::get('/dosen/komplain', 'KomplainController@index_dosen')->name('dosen.komplain');
Route::get('/unitkerja/komplain', 'KomplainController@index_unitkerja')->name('unitkerja.komplain');
Route::get('/komplain', 'KomplainController@index')->name('komplain.index');
Route::post('/komplain', 'KomplainController@create')->name('komplain.save');

Route::get('/mahasiswa/permintaan', 'PermintaanDataController@index_mahasiswa')->name('mahasiswa.permintaan');
Route::get('/dosen/permintaan', 'PermintaanDataController@index_dosen')->name('dosen.permintaan');
Route::get('/unitkerja/permintaan', 'PermintaanDataController@index_unitkerja')->name('unitkerja.permintaan');
Route::get('/permintaan', 'PermintaanDataController@index')->name('permintaan.index');
Route::post('/permintaan', 'PermintaanDataController@create')->name('permintaan.save');
Route::get('/permintaan-ppid', 'PermintaanDataController@index')->name('permintaan-ppid');

Route::get('/tracking', 'TrackingController@index')->name('tracking.index');
Route::post('/tracking', 'TrackingController@search')->name('tracking.search');
Route::get('/tracking/detail', 'TrackingController@detail')->name('tracking.detail');
Route::get('/tracking/kirim-catatan', 'TrackingController@kirim_catatan')->name('tracking.kirim_catatan');
Route::post('/tracking/kirim-catatan', 'TrackingController@kirim_catatan_save')->name('tracking.kirim_catatan.save');

Route::get('/cekruang', 'BookingController@index');
Route::post('/cekruang', 'BookingController@cekruangan');
Route::post('/bookingruang', 'BookingController@booking');
Route::get('/getruang/{id}', 'BookingController@ambilruang');

Route::get('/download/unitkerjafile}', function($file){
	return response()->download(storage_path('file/komplain/'.$file));
})->name('download.komplain');
Route::get('/download/mintadata/{file}', function($file){
	return response()->download(storage_path('file/minta_data/'.$file));
});
Route::get('/download/booking/{file}', function($file){
	return response()->download(storage_path('file/booking/'.$file));
});


	############# untuk redirect ke halaman auth dengan token #############
	Route::get('/redirect', 'RedirectWithTokenController@index')->name('redirect.link');
	############# ./untuk redirect ke halaman auth dengan token #############

########################## USER ##########################



########################## ADMIN ##########################
Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function(){	
	Route::get('/dashboard', function () {
		return view('admin.index');
	})->name('admin.dashboard');

	############# controller komplain #############
		Route::get('/datakomplain', 'DataKomplainController@index')->name('admin.komplain');
		Route::get('/datakomplain/detail/{id}', 'DataKomplainController@detail')->name('admin.komplain.detail');
		Route::post('/feedback-komplain', 'DataKomplainController@send_feedback');

		Route::get('/datakomplain/forward/{id}', 'DataKomplainController@forward')->name('admin.komplain.forward');
		Route::post('/datakomplain/forward/{id}', 'DataKomplainController@forward_save')->name('admin.komplain.forward.save');

		Route::get('/datakomplain/pesan/masuk', 'PesanKomplainController@pesan_masuk')->name('admin.komplain.pesan.masuk');
		Route::get('/datakomplain/pesan/masuk/list/{id}', 'PesanKomplainController@pesan_masuk_list')->name('admin.komplain.pesan.masuk.list');
		Route::get('/datakomplain/pesan/masuk/list/{komplain_id}/detail/{user_id}', 'PesanKomplainController@pesan_masuk_list_detail')->name('admin.komplain.pesan.masuk.list.detail');
		Route::get('/datakomplain/pesan/masuk/forward/{id}', 'PesanKomplainController@pesan_masuk_forward')->name('admin.komplain.pesan.masuk.forward');
		Route::post('/datakomplain/pesan/masuk/forward/{id}', 'PesanKomplainController@pesan_masuk_forward_save')->name('admin.komplain.pesan.masuk.forward.save');
	############# controller komplain #############

	############# controller permintaan #############
		Route::get('/datapermintaan', 'DataPermintaanController@index')->name('admin.permintaan');
		Route::get('/datapermintaan/detail/{id}', 'DataPermintaanController@detail')->name('admin.permintaan.detail');
		Route::post('/feedback-permintaan', 'DataPermintaanController@send_feedback');

		Route::get('/datapermintaan/forward/{id}', 'DataPermintaanController@forward')->name('admin.permintaan.forward');
		Route::post('/datapermintaan/forward/{id}', 'DataPermintaanController@forward_save')->name('admin.permintaan.forward.save');

		Route::get('/datapermintaan/pesan/masuk', 'PesanPermintaanController@pesan_masuk')->name('admin.permintaan.pesan.masuk');
		Route::get('/datapermintaan/pesan/masuk/list/{id}', 'PesanPermintaanController@pesan_masuk_list')->name('admin.permintaan.pesan.masuk.list');
		Route::get('/datapermintaan/pesan/masuk/list/{permintaan_id}/detail/{user_id}', 'PesanPermintaanController@pesan_masuk_list_detail')->name('admin.permintaan.pesan.masuk.list.detail');
		Route::get('/datapermintaan/pesan/masuk/forward/{id}', 'PesanPermintaanController@pesan_masuk_forward')->name('admin.permintaan.pesan.masuk.forward');
		Route::post('/datapermintaan/pesan/masuk/forward/{id}', 'PesanPermintaanController@pesan_masuk_forward_save')->name('admin.permintaan.pesan.masuk.forward.save');
	############# controller permintaan #############

	############# edit profil #############
		Route::get('/profil/edit/', 'UserController@index')->name('admin.profil.edit');	
		Route::post('/profil/save', 'UserController@save')->name('admin.profil.save');
	############# ./edit profil #############

	Route::get('/databooking', 'DataBookingController@index');
	Route::get('/databooking/detail/{id}', 'DataBookingController@detail');
	Route::post('/feedback-booking', 'DataBookingController@send_feedback');

});
########################## ADMIN ##########################

########################## SIREMUN ##########################
Route::get('/siremun', 'KomplainSiremunController@index')->name('siremun.index');
Route::post('/siremun', 'KomplainSiremunController@create')->name('siremun.komplain.save');

	############# controller komplain siremun #############
	Route::group(['middleware' => 'auth', 'prefix' => 'siremun'], function () {
		
		Route::get('/datakomplain', 'DataKomplainSiremunController@index')->name('admin.siremun.komplain');
		Route::get('/datakomplain/detail/{id}', 'DataKomplainSiremunController@detail')->name('admin.siremun.komplain.detail');
		Route::post('/feedback-komplain', 'DataKomplainSiremunController@send_feedback');
	
		Route::get('/datakomplain/forward/{id}', 'DataKomplainSiremunController@forward')->name('admin.siremun.komplain.forward');
		Route::post('/datakomplain/forward/{id}', 'DataKomplainSiremunController@forward_save')->name('admin.siremun.komplain.forward.save');
	
		Route::get('/datakomplain/pesan/masuk', 'PesanKomplainSiremunController@pesan_masuk')->name('admin.siremun.komplain.pesan.masuk');
		Route::get('/datakomplain/pesan/masuk/list/{id}', 'PesanKomplainSiremunController@pesan_masuk_list')->name('admin.siremun.komplain.pesan.masuk.list');
		Route::get('/datakomplain/pesan/masuk/list/{komplain_id}/detail/{user_id}', 'PesanKomplainSiremunController@pesan_masuk_list_detail')->name('admin.siremun.komplain.pesan.masuk.list.detail');
		Route::get('/datakomplain/pesan/masuk/forward/{id}', 'PesanKomplainSiremunController@pesan_masuk_forward')->name('admin.siremun.komplain.pesan.masuk.forward');
		Route::post('/datakomplain/pesan/masuk/forward/{id}', 'PesanKomplainSiremunController@pesan_masuk_forward_save')->name('admin.siremun.komplain.pesan.masuk.forward.save');
	
	});
	############# controller komplain siremun #############

########################## SIREMUN ##########################

Route::get('/teswa', function () {
	$pesan = 'tes wa lagi ni bos';
	$tujuan = '6289501942413';
	$headers = ['headers' => ['apikey' => 'untan2020']];
	$client = new \GuzzleHttp\Client();
    $request = $client->post('http://103.126.83.7:8089/api/sendText/?id_device=1&message='.$pesan.'&tujuan='.$tujuan.'@s.whatsapp.net', $headers);
    $response = $request->getBody();
	return $response;
});