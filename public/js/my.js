$(document).ready(function(){

	$('#no_hp').keypress(function(){
		a = $(this).val( parseInt( $(this).val(), 10 ) );
		// console.log( a );
	});
	$("#cara_peroleh").change(function(){
		if($(this).val() == 'rekaman'){
			$("#rekaman").show('fast');
		} else {
			$("#rekaman").hide('fast');
			$("#kisi").val("");
		}
	});

	$("#detail_ruang").hide('fast');
	$("#pilih_ruangan").change(function(){
		var id_ruangan = $(this).val();		

		$.ajaxSetup({
        	headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    	});
		$.ajax({
			url: '/getruang/' + id_ruangan,
			dataType: 'json',
			type: 'GET',
			success: function(data) {
				// console.log(data.ruang.nama_ruang);
				// if(data.ruang !== 'null') {
				// 	// $("#nama_ruang").html(data.ruang.nama_ruang);
				// 	// $("#lokasi").html(data.ruang.lokasi);
				// 	// $("#kapasitas").html(data.ruang.kapasitas);
				// 	// $("#foto").html(data.ruang.foto);
				// 	console.log(data.ruang.nama_ruang);
				// } else {
				// 	console.log("null");
				// 	// $("#detail_ruang").hide('fast');
				// }
				if(data.ruang == null){
					$("#detail_ruang").hide('fast');
					console.log("ada");
				} else {
					console.log(data.ruang);
					$("#detail_ruang").show('fast');
					$("#nama_ruang").html(data.ruang.nama_ruang);
					$("#lokasi").html(data.ruang.lokasi);
					$("#kapasitas").html(data.ruang.kapasitas);
					$("#foto").html(data.ruang.foto);
				}
			}
		});
	});

	var table = $('#datatable-fixed-header').DataTable({fixedHeader: true});


	//dihalaman admin.permintaan.detil
	var cara_peroleh 		= $("#peroleh").data("caraperoleh");
	var kisi 		 		= $("#kisi").data("kisi");;
	var tempat		 		= $("#tempat").data("tempat");;
	var waktu		 		= $("#waktu").data("waktu");;
	var tujuan_wawancara	= $("#tujuan_wawancara").data("tujuan_wawancara");
	if(cara_peroleh == 'rekaman'){
		$(".rekaman").show();
		$("textarea#data_kisi").val(kisi);
		$("#data_kisi").val(kisi);
		$("#data_tempat").val(tempat);
		$("#data_waktu").val(waktu);
		$("#data_tujuan_wawancara").val(tujuan_wawancara);
	}

	var pathname = window.location.pathname; 
	if (pathname == '/mintadata-ppid') {
		$('#kategori').val('ppid');
	}
});
